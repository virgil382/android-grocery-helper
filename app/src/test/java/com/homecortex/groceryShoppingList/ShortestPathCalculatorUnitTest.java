package com.homecortex.groceryShoppingList;

import com.homecortex.groceryShoppingList.DataModel.Upgrade.ShortestPathCalculator.Edge;
import com.homecortex.groceryShoppingList.DataModel.Upgrade.ShortestPathCalculator.ShortestPathCalculator;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ShortestPathCalculatorUnitTest {
    private class TestEdge implements Edge {
        String m_source;
        String m_target;

        public TestEdge(String source, String target) {
            m_source = source; m_target = target;
        }

        public String getSourceName() { return(m_source); }
        public String getTargetName() { return(m_target); }

        @Override
        public String toString() {
            return(m_source + "->" + m_target);
        }
    }


    @Test
    public void testShortestPathCalculator1()
    {
        ShortestPathCalculator<TestEdge> sut = new ShortestPathCalculator<>();
        sut.addEdge(new TestEdge("A", "B"));
        sut.addEdge(new TestEdge("B", "C"));
        sut.addEdge(new TestEdge("C", "D"));
        sut.addEdge(new TestEdge("A", "C"));

        List<TestEdge> path = sut.getShortestPath("A", "D");

        TestEdge testEdge;
        int index = 0;

        Assert.assertEquals(2, path.size());

        testEdge = path.get(index++);
        Assert.assertTrue(testEdge.toString().equals("A->C"));
        testEdge = path.get(index++);
        Assert.assertTrue(testEdge.toString().equals("C->D"));
    }

    @Test
    public void testShortestPathCalculator2()
    {
        ShortestPathCalculator<TestEdge> sut = new ShortestPathCalculator<>();
        sut.addEdge(new TestEdge("A", "B"));
        sut.addEdge(new TestEdge("C", "B"));

        List<TestEdge> path = sut.getShortestPath("C", "B");

        TestEdge testEdge;
        int index = 0;

        Assert.assertEquals(1, path.size());

        testEdge = path.get(index++);
        Assert.assertTrue(testEdge.toString().equals("C->B"));
    }
}
