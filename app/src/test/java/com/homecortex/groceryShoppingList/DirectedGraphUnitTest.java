package com.homecortex.groceryShoppingList;

import com.homecortex.groceryShoppingList.TopologicalSort.DirectedGraph;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class DirectedGraphUnitTest {
    /**
     * Verify that a topological sort removes the vertex with no inbound edges first.
     * The graph looks like this:
     *   A-------------->B
     * So the topological sort should remove A first and then B.
     */
    @Test
    public void testTopologicalSort1() throws Exception {
        DirectedGraph<String> dgraph = new DirectedGraph<>();
        dgraph.addDirectedEdge("A", "B");
        List<String> sortedVertices = dgraph.topologicalSort();

        String actualVertex;
        int index = 0;

        actualVertex = sortedVertices.get(index++);
        Assert.assertTrue(actualVertex.compareTo("A") == 0);

        actualVertex = sortedVertices.get(index++);
        Assert.assertTrue(actualVertex.compareTo("B") == 0);
    }

    /**
     * Verify that a topological sort removes A, B, C.
     * The graph looks like this:
     *   +-------A-------+
     *   |               |
     *   |               |
     *   v               V
     *   B-------------->C
     */
    @Test
    public void testTopologicalSort2()
    {
        DirectedGraph<String> dgraph = new DirectedGraph<>();
        dgraph.addDirectedEdge("A", "B");
        dgraph.addDirectedEdge("B", "C");
        dgraph.addDirectedEdge("A", "C");
        List<String> sortedVertices = dgraph.topologicalSort();

        String actualVertex;
        int index = 0;

        actualVertex = sortedVertices.get(index++);
        Assert.assertTrue(actualVertex.compareTo("A") == 0);

        actualVertex = sortedVertices.get(index++);
        Assert.assertTrue(actualVertex.compareTo("B") == 0);

        actualVertex = sortedVertices.get(index++);
        Assert.assertTrue(actualVertex.compareTo("C") == 0);
    }

    /**
     * Verify that a topological sort correctly sorts 5 vertices.  The graph looks like this:
     * A----->B----->C----->D----->E
     * |             ^
     * |             |
     * +-------------+
     */
    @Test
    public void testTopologicalSort3()
    {
        DirectedGraph<String> dgraph = new DirectedGraph<>();
        dgraph.addDirectedEdge("A", "B");
        dgraph.addDirectedEdge("A", "C");
        dgraph.addDirectedEdge("B", "C");
        dgraph.addDirectedEdge("C", "D");
        dgraph.addDirectedEdge("D", "E");
        List<String> sortedVertices = dgraph.topologicalSort();

        String actualVertex;
        int index = 0;

        actualVertex = sortedVertices.get(index++);
        Assert.assertTrue(actualVertex.compareTo("A") == 0);

        actualVertex = sortedVertices.get(index++);
        Assert.assertTrue(actualVertex.compareTo("B") == 0);

        actualVertex = sortedVertices.get(index++);
        Assert.assertTrue(actualVertex.compareTo("C") == 0);

        actualVertex = sortedVertices.get(index++);
        Assert.assertTrue(actualVertex.compareTo("D") == 0);

        actualVertex = sortedVertices.get(index++);
        Assert.assertTrue(actualVertex.compareTo("E") == 0);
    }

}
