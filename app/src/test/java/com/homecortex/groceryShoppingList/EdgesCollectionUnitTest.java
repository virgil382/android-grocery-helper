package com.homecortex.groceryShoppingList;

import com.homecortex.groceryShoppingList.TopologicalSort.EdgesCollection;

import org.junit.Assert;
import org.junit.Test;

import java.util.AbstractCollection;


public class EdgesCollectionUnitTest {

    private boolean isContained(AbstractCollection<Integer> collection, Integer n) {
        for(Integer i : collection) {
            if(n.equals(i)) return(true);
        }
        return(false);
    }

    @Test
    public void getCenterVertex_isCorrect() throws Exception {
        EdgesCollection<Integer> sut = new EdgesCollection<>(1);
        Assert.assertEquals(Integer.valueOf(1), sut.getCenterVertex());
    }

    @Test
    public void addRemoveEdge_isCorrect() throws Exception {
        EdgesCollection<Integer> sut = new EdgesCollection<>(1);
        sut.addEdge(2);
        sut.addEdge(3);

        AbstractCollection<Integer> peripheralVertices = sut.getPeripheralVertices();

        Assert.assertFalse(isContained(peripheralVertices, 1));
        Assert.assertTrue(isContained(peripheralVertices, 2));
        Assert.assertTrue(isContained(peripheralVertices, 3));

        sut.removeEdge(2);
        peripheralVertices = sut.getPeripheralVertices();
        Assert.assertFalse(isContained(peripheralVertices, 1));
        Assert.assertFalse(isContained(peripheralVertices, 2));
        Assert.assertTrue(isContained(peripheralVertices, 3));
    }

    @Test
    public void getEdgeCount_isCorrect() throws Exception {
        EdgesCollection<Integer> sut = new EdgesCollection<>(1);
        sut.addEdge(2);
        sut.addEdge(3);
        Assert.assertEquals(Integer.valueOf(2), sut.getEdgeCount());
    }
}
