// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Location;

import android.location.Location;

/**
 * An ILocationListener is notified when the Location of the device is available.
 */
public interface ILocationListener {
	/**
	 * Handle an event announcing that the device's Location is available.
	 * @param location The Location of the device.
	 */
	void onLocationAvailable(Location location);
}
