// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Location;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;

/**
 * A LocationRequestor is a control class that delivers "best" Location updates to an 
 * ILocationListener.  A LocationRequestor tries to obtain locations from the GPS provider, from
 * the Network provider, and from the passive provider (which listens for updates from any
 * provider).  LocationRequestor has a start() method that starts the updates and a stop() method
 * that stops the updates after an optional delay which helps to "debounce" frequent calls to
 * start() and stop() in order to allow more reliable calculation and delivery of Locations in
 * scenarios in which start() and stop() are called frequently but the overall intent is for
 * Locations updates to be delivered.
 */
public class LocationRequestor {
	private static final String TAG = "LocationRequestor";

	private LocationManager mLocationManager;
	private ILocationListener mLocationListener;
	private final long MAX_AGE_OF_LOCATION_MILLIS = 120000;
	private final long LOCATION_UPDATE_FREQUENCY_MILLIS = 10000;

	class State {
		public static final int STOPPED = 1;
		public static final int STARTED = 2;
		public static final int STOPPING = 3;
	}
	private int mState;
	
	public LocationRequestor(LocationManager locationManager) {
		mLocationManager = locationManager;
		mState = State.STOPPED;
	}
	
	/**
	 * Determine if the device supports any appropriate location providers from which a LocationRequestor
	 * may receive Location updates.
	 * 
	 * @return true if there are, otherwise false.
	 */
	public boolean areLocationProvidersAvailable()
	{
		if (null != mLocationManager.getProvider(LocationManager.GPS_PROVIDER)) return true;
		if (null != mLocationManager.getProvider(LocationManager.NETWORK_PROVIDER)) return true;
		return false;
	}

	/**
	 * Determine if the GPS location provider is available (i.e. the device supports it) and disabled.  If it is,
	 * then the caller may subsequently call takeUserToGpsSettings().
	 *  
	 * @return true if the GPS is available and disabled, otherwise false.
	 */
	public boolean isGpsAvailableAndDisabled() {
		if (null != mLocationManager.getProvider(LocationManager.GPS_PROVIDER)) {
			if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) return true;
		}

		return false;
	}

	/**
	 * Take the user to the GPS settings activity so that the user may enable/disable the GPS.
	 * 
	 * @param context The current application Context.
	 */
	public void takeUserToGpsSettings(Context context) {
		final LocationManager manager = (LocationManager) context.getSystemService( Context.LOCATION_SERVICE );
		if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
		     Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		     context.startActivity(intent);
		}		
	}
	
	/**
	 * Start delivering Location updates to the specified ILocationListener.
	 * 
	 * @param locationListener The ILocationListener to receive the Location updates.
	 * @return True if an appropriate LocationProvider is available, otherwise false.  If false, then
	 * the caller may need to function without any Location updates.
	 */
	public boolean start(ILocationListener locationListener) {
		if (!areLocationProvidersAvailable()) return false;

		// If already STARTED, then do nothing.
		if (mState == State.STARTED) return true;
		
		// If STOPPING, then cancel the Timer so that it won't execute the TimerTask that stops the updates. 
		if (mState == State.STOPPING) {
			if (timer != null) {
				timer.cancel();
				timer = null;
		        Log.d(TAG, "start(): Stopped timer that stops updates");
			}
			mState = State.STARTED;
			return true;
		}
		
		// If STOPPED, then start the updates.
		if (mState == State.STOPPED) {
			mLocationListener = locationListener;
	
			// Start receiving updates from the GPS.
			if (null != mLocationManager.getProvider(LocationManager.GPS_PROVIDER)) {
				mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_UPDATE_FREQUENCY_MILLIS, 0, locationProviderListener, Looper.getMainLooper());
		        Log.d(TAG, "start(): GPS location updates started");
		        // Note that if the GPS is disabled, then this will immediately result in a
		        // call to locationProviderListener.OnProviderDisabled().
			}

			// Start receiving (less accurate) updates from the network.
			if (null != mLocationManager.getProvider(LocationManager.NETWORK_PROVIDER)) {
				mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_UPDATE_FREQUENCY_MILLIS, 0, locationProviderListener, Looper.getMainLooper());
		        Log.d(TAG, "start(): Network location updates started");
			}

			// Send an update right away if a Location is available.
			Location bestResult = getBestLocation();
			if (bestResult != null) mLocationListener.onLocationAvailable(bestResult);
			
			mState = State.STARTED;
			return true;
		}
		
        Log.e(TAG, "start(): Unknown state");
		return true;
	}

	private Timer timer;

	/**
	 * Stop delivering Location updates to the ILocationListener.
	 * 
	 * @param delayMillis Optional delay (in milliseconds) to wait before actually stopping.  If start()
	 * is called again before this delay expires, then delivery of Locations will continue (i.e. will not
	 * be stopped).  Thus, if the caller calls start() and stop() whenever the user logs on and off, then
	 * the delay may be used by the caller as a buffer to allow the device's GPS sufficient time to still
	 * lock on to GPS satellites and actually obtain a Location even in scenarios in which the user logs
	 * on and off frequently. 
	 */
	public void stop(long delayMillis) {
		if (mState == State.STOPPED) return;
		
		if (delayMillis <= 0) {
			stop();
			return;
		}
		
		if (mState == State.STOPPING) return;

		// If necessary, cancel the previous Timer (and terminate its background Thread).
		if (timer != null) timer.cancel();

		// Create a task that stops the updates.
		TimerTask doRequestLocation = new TimerTask() {
        	public void run() { stop(); }
        }; 
        
        // Schedule the task to execute on the thread of a new Timer after the specified time delay.
		timer = new Timer();
		timer.schedule(doRequestLocation, delayMillis);
		mState = State.STOPPING;

		Log.d(TAG, "stop(): Will occur in " + delayMillis + " milliseconds");
	}

	/**
	 * Stop delivery of Locations and cancel the Timer.
	 */
	private void stop() {
        Log.d(TAG, "stop(): entering STOPPED state");
        mState = State.STOPPED;
		mLocationManager.removeUpdates(locationProviderListener);

		// Stop the timer.
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}
	
	// The LocationListener used to receive Location updates.
	private LocationListener locationProviderListener = new LocationListener() {
		@Override
		public void onLocationChanged(Location location) {
	        Log.d(TAG, "onLocationChanged()");
			Location bestResult = getBestLocation();
			if (bestResult != null) mLocationListener.onLocationAvailable(bestResult);
		}

		@Override public void onStatusChanged(String provider, int status, Bundle extras) { 
//			switch(status) {
//			case LocationProvider.OUT_OF_SERVICE:
//		        Log.d(TAG, "onStatusChanged(): OUT_OF_SERVICE");
//		        break;
//			case LocationProvider.TEMPORARILY_UNAVAILABLE :
//		        Log.d(TAG, "onStatusChanged(): TEMPORARILY_UNAVAILABLE ");
//		        break;
//			case LocationProvider.AVAILABLE:
//		        Log.d(TAG, "onStatusChanged(): AVAILABLE");
//		        break;
//		    default:
//		        Log.d(TAG, "onStatusChanged(): " + status);
//		        break;
//			}
		}

		@Override public void onProviderEnabled(String provider) {
//	        Log.d(TAG, "onProviderEnabled()");
//			mLocationListener.onLocationServicesAvailable();
		}

		@Override public void onProviderDisabled(String provider) {
//	        Log.d(TAG, "onProviderDisabled()");
//			mLocationListener.onLocationServicesUnavailable();
		}
	};

	private Location getBestLocation() {
		Location bestResult = null;
		float bestAccuracy = Float.MAX_VALUE;
		long bestTime = Long.MIN_VALUE;
		long minTime = System.currentTimeMillis() - MAX_AGE_OF_LOCATION_MILLIS;

		// Find the most accurate Location newer than minTime, otherwise find the
		// newest Location older than minTime.
	    List<String> matchingProviders = mLocationManager.getAllProviders();
	    for (String provider: matchingProviders) {
		Location location = mLocationManager.getLastKnownLocation(provider);
			if (location != null) {
				float accuracy = location.getAccuracy();
				long time = location.getTime();
	   
				if (time > minTime) {
					// Pick the most accurate Location that is newer than minTime.
					if (accuracy < bestAccuracy) {
						bestResult = location;
						bestAccuracy = accuracy;
						bestTime = time;
					}
				} else {
					// Pick the newest Location that is older than minTime.
					if (bestAccuracy == Float.MAX_VALUE && time > bestTime) {
						bestResult = location;
						bestTime = time;
					}
				}
			}
	    }

	    return bestResult;
	}
}
