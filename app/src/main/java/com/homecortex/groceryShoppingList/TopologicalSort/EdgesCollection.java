package com.homecortex.groceryShoppingList.TopologicalSort;

import java.util.AbstractCollection;
import java.util.TreeSet;

/**
 * An EdgeCollection represents a set of edges between (i.e. to or from) a center vertex and a
 * set of peripheral vertices.
 */
public class EdgesCollection<T> {
    private T m_centerVertex;
    private TreeSet<T> m_peripheralVertices = new TreeSet<>();

    /**
     * Construct an EdgesCollection that holds edges to/from specified center vertex.
     * @param centerVertex The centerVertex vertex.
     */
    public EdgesCollection(T centerVertex)
    {
        m_centerVertex = centerVertex;
    }

    /**
     * Get the center vertex.
     * @return The target vertex.
     */
    public T getCenterVertex() {
        return(m_centerVertex);
    }

    /**
     * Get the collection of peripheral vertices.
     * @return An AbstractCollection containing the peripheral vertices.
     */
    public AbstractCollection<T> getPeripheralVertices() {
        return(m_peripheralVertices);
    }

    /**
     * Add an edge between the center vertex and the specified peripheral vertex.
     * @param peripheralVertex The peripheral vertex.
     */
    public void addEdge(T peripheralVertex)
    {
        m_peripheralVertices.add(peripheralVertex);
    }

    /**
     * Drop an edge between the center vertex and the specified peripheral vertex.
     * @param peripheralVertex The peripheral vertex.
     */
    public void removeEdge(T peripheralVertex)
    {
        m_peripheralVertices.remove(peripheralVertex);
    }

    public Integer getEdgeCount() {
        return(m_peripheralVertices.size());
    }
}
