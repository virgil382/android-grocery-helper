package com.homecortex.groceryShoppingList.TopologicalSort;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeMap;

/**
 *
 */
public  class DirectedGraph<T> {
    // This TreeMap is used to map a node T to a collection of inbound edges into it.  It
    // quickly answers the question "Find all inbound edges to vertex V".
    private TreeMap<T, EdgesCollection<T>> m_allInboundEdges = new TreeMap<>();
    /**
     * Get an EdgesCollection that holds all vertices with an edge leading into the
     * specified targetVertex.
     *
     * @param targetVertex The targetVertex into which the edges must lead.
     * @return An EdgesCollection if successful, otherwise null.
     */
    private EdgesCollection<T> getInboundEdgesToVertex(T targetVertex) {
        EdgesCollection<T> edgesToVertex = m_allInboundEdges.get(targetVertex);
        if (edgesToVertex == null) {
            edgesToVertex = new EdgesCollection<>(targetVertex);
            m_allInboundEdges.put(targetVertex, edgesToVertex);
        }
        return edgesToVertex;
    }

    // This TreeMap is used to map a node T to a collection of outbound edges.  It
    // quickly answers the question "Find all outbound edges from vertex V".
    private TreeMap<T, EdgesCollection<T>> m_allOutboundEdges = new TreeMap<>();
    private EdgesCollection<T> getOutboundEdgesFromVertex(T sourceVertex) {
        EdgesCollection<T> edgesFromVertex = m_allOutboundEdges.get(sourceVertex);
        if (edgesFromVertex == null) {
            edgesFromVertex = new EdgesCollection<>(sourceVertex);
            m_allOutboundEdges.put(sourceVertex, edgesFromVertex);
        }
        return edgesFromVertex;
    }

    // This PriorityQueue holds EdgesCollections containing edges inbound to vertices.  It keeps
    // them sorted by their size so that the EdgesCollection with the smallest size (i.e. the
    // vertex with the fewest inbound edges) is at the top of the PriorityQueue.
    private PriorityQueue<EdgesCollection<T>> m_prioritizedCollectionsOfInboundEdges =
            new PriorityQueue<>(11, new SortByEdgeCount());
    private class SortByEdgeCount implements Comparator<EdgesCollection<T>>
    {
        // Compare two EdgesCollections by using their sizes.  This allows the
        // priority queue to organize it contents such that the vertices with the fewest
        // number of inbound edges are at the top.
        @Override
        public int compare(EdgesCollection<T> lhs, EdgesCollection<T> rhs) {
            return (lhs.getEdgeCount().compareTo(rhs.getEdgeCount()));
        }
    }

    /**
     * Add an edge from a tail vertex to a head vertex.
     * @param tail The tail vertex.
     * @param head The head vertex.
     */
    public void addDirectedEdge(T tail, T head) {
        assert(tail != null);
        assert(head != null);

        // Add the edge to the map of inbound edges.
        EdgesCollection<T> edgesToHead = getInboundEdgesToVertex(head);
        edgesToHead.addEdge(tail);
        getInboundEdgesToVertex(tail);  // This simply adds an empty EdgesCollection if one does not already exist.

        // Add the edge to the map of outbound edges.
        EdgesCollection<T> edgesFromTail = getOutboundEdgesFromVertex(tail);
        edgesFromTail.addEdge(head);
        getOutboundEdgesFromVertex(head);  // This simply adds an empty EdgesCollection if one does not already exist.


        // Note that we don;t yet touch m_prioritizedCollectionsOfInboundEdges.  We will populate
        // it when we do the topological sort.
    }

    /**
     * Perform a topological sort on the graph.  The algorithm always succeeds (even if
     * there are cycles).  It repeatedly drops the vertex with the smallest total number of
     * inbound edges (by locating it in the priority queue) until there are no more vertices left.
     * In the case of a DAG, the top element in the priority queue should have 0 inbound edges.
     *
     * Each time, it adds the dropped vertex to the list of sorted vertices.  Upon return, the
     * DirectedGraph will be empty (i.e. allInboundEdges and m_prioritizedCollectionsOfInboundEdges
     * will be empty).
     *
     * @return A list of vertices sorted in topological order.
     */
    public List<T> topologicalSort()
    {
        // Copy all the EdgesCollections from m_allInboundEdges to the PriorityQueue
        for(Map.Entry<T, EdgesCollection<T>> entry : m_allInboundEdges.entrySet()) {
            EdgesCollection<T> vertexInboundEdges = entry.getValue();
            m_prioritizedCollectionsOfInboundEdges.add(vertexInboundEdges);
        }
        // At this point, the top element in the PriorityQueue will be the EdgesCollection that
        // has no inbound edges.

        LinkedList<T> sortedVertices = new LinkedList<>();
        while(m_prioritizedCollectionsOfInboundEdges.size() > 0)
        {
            // Get the top vertex and its edges.
            EdgesCollection<T> edgesToTopVertex = m_prioritizedCollectionsOfInboundEdges.poll();
            sortedVertices.add(edgesToTopVertex.getCenterVertex());

            // Remove from the graph edges to/from the top vertex.
            dropVertex(edgesToTopVertex.getCenterVertex());
        }

        return (sortedVertices);
    }

    /**
     * Drop a vertex from m_allInboundEdges and from m_allOutboundEdges.  Also adjust the contents
     * of m_prioritizedCollectionsOfInboundEdges.  Note
     * that dropping a vertex may affect the number of edges into other vertices.  This affects the
     * ordering of the priority queue.  Consequently, this method
     *
     * @param doomedVertex The vertex to be dropped.
     */
    private void dropVertex(T doomedVertex) {
        // Remove the collection of edges to the doomed vertex.
        EdgesCollection<T> edgesToDoomedVertex = m_allInboundEdges.remove(doomedVertex);
        EdgesCollection<T> edgesFromDoomedVertex = m_allOutboundEdges.remove(doomedVertex);
        m_prioritizedCollectionsOfInboundEdges.remove(edgesToDoomedVertex);  // This does nothing when called from topologicalSort().

        // For each vertex peripheralDoomed peripheral to doomedVertex, for which there is an edge
        // (doomedVertex -> peripheralDoomed):
        //   Locate the EdgesCollection EC that contains edges into peripheralDoomed.
        //   Add EC to a list L.
        //
        // Note that L contains EdgeCollections that are members of the PriorityQueue.  When we
        // drop doomedVertex from each EdgeCollection, we must adjust the PriorityQueue.  This is
        // done as follows:
        //   1) remove each EdgeCollection from the PriorityQueue
        //   2) drop doomedVertex from the EdgeCollection (which reduces its size)
        //   3) add the EdgeCollection back to the PriorityQueue
        LinkedList<EdgesCollection<T>> affectedInboundEdgeCollections = new LinkedList<>();
        for(T peripheralDoomed : edgesFromDoomedVertex.getPeripheralVertices()) {
            EdgesCollection<T> ec = m_allInboundEdges.get(peripheralDoomed);
            if(ec != null) {
                affectedInboundEdgeCollections.add(ec);
                m_prioritizedCollectionsOfInboundEdges.remove(ec);
            }
        }

        // Remove doomedVertex from each ec.  This reduces their size by 1.
        for(EdgesCollection<T> ec : affectedInboundEdgeCollections) {
            ec.removeEdge(doomedVertex);
        }

        // Add each ec back to the PriorityQueue.
        for(EdgesCollection<T> ec : affectedInboundEdgeCollections) {
            m_prioritizedCollectionsOfInboundEdges.add(ec);
        }
    }

}
