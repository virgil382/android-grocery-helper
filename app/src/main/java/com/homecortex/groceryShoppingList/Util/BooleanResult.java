package com.homecortex.groceryShoppingList.Util;

public class BooleanResult {
	private boolean m_value = false;
	
	public void set(boolean value) {
		m_value = value;
	}
	
	public boolean get() {
		return m_value;
	}
}
