package com.homecortex.groceryShoppingList.Util;

import java.util.List;

import com.homecortex.groceryShoppingList.Util.IabHelper.QueryInventoryFinishedListener;
import com.homecortex.groceryShoppingList.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

public class LicenseManager {
	private static final String TAG = "LicenseManager";

	private static final String PREFERENCE_FIRST_RUN_MILLIS = "FirstRunMillis";
	private static final String PREFERENCE_ITEM_ORDER_LEARNING_COUNT = "ItemOrderLearningCount";
	private static final String PREFERENCE_GRACE_USAGES_COUNT = "GraceUsagesCount";
	private static final int PREFERENCE_INITIAL_GRACE_USAGES_COUNT = 3;
	private static final long TRIAL_PERIOD_MILLIS = (long) 90 * (long) 24 * (long) 60 * (long) 60 * (long) 1000;  // 90 days
	private static final String PREFERENCE_IS_LICENSE_PURCHASED = "IsLicensePurchased";
	public static final int PURCHASE_ACTIVITY_REQUEST_CODE = 5921001;

	public static final String LICENSE_SKU = "save_items_order_license";
	//public static final String LICENSE_SKU = "android.test.purchase";

	private SharedPreferences m_sharedPreferences;

	// Singleton operations.
	private static LicenseManager m_instance;
	public static void initialize(Activity activity) {
		m_instance = new LicenseManager(activity);
	}
	public static LicenseManager instance() {
		return(m_instance);
	}
	private LicenseManager(Activity activity) {
		m_sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
	}
	
	/**
	 * Determine if the application is either licensed or is in the grace period.
	 * @return True if the application is either licensed or is in the grace period.
	 */
	public boolean isLicensed() {
		if (getMillisSinceFirstRun() < TRIAL_PERIOD_MILLIS) return (true);
		if (isLicensePurchased()) return (true);
		return (false);
	}

	public boolean isInGracePeriod() {
		if (getGraceUsagesCount() > 0) return (true);
		return (false);
	}

	/**
	 * Determine of a license was purchased by reading the value from preferences.
	 * @return true if the license was purchased, otherwise false.
	 */
	private boolean isLicensePurchased() {
		boolean isPurchased = m_sharedPreferences.getBoolean(PREFERENCE_IS_LICENSE_PURCHASED, false);
		return (isPurchased);
	}

	/**
	 * Cache the assertion about whether or not a license was purchased.
	 * @param value True if it was, otherwise false.
	 */
	private void setIsLicensePurchased(boolean value) {
		m_sharedPreferences.edit().putBoolean(PREFERENCE_IS_LICENSE_PURCHASED, value).commit();
	}
	
	public long getMillisSinceFirstRun() {
		long currentTime = System.currentTimeMillis();
		long millisOfFirstRun = m_sharedPreferences.getLong(PREFERENCE_FIRST_RUN_MILLIS, 0);
		if (millisOfFirstRun == 0) {
			// This is the first run, so store the current date.
			m_sharedPreferences.edit().putLong(PREFERENCE_FIRST_RUN_MILLIS, currentTime).commit();
			return (0);
		}
		
		return (currentTime - millisOfFirstRun);
	}
	
	public long incrementItemOrderLearningCount() {
		long count = getItemOrderLearningCount();
		count++;
		m_sharedPreferences.edit().putLong(PREFERENCE_ITEM_ORDER_LEARNING_COUNT, count).commit();
		
		if (!isLicensed()) decrementGraceUsagesCount();
		
		return (count);
	}
	
	private long getItemOrderLearningCount() {
		long count = m_sharedPreferences.getLong(PREFERENCE_ITEM_ORDER_LEARNING_COUNT, 0);
		if (count == 0) {
			m_sharedPreferences.edit().putLong(PREFERENCE_ITEM_ORDER_LEARNING_COUNT, count).commit();
			return (0);
		}
		
		return (count);
	}
	
	public int getGraceUsagesCount() {
		int count = m_sharedPreferences.getInt(PREFERENCE_GRACE_USAGES_COUNT, PREFERENCE_INITIAL_GRACE_USAGES_COUNT);
		return (count);
	}

	private void decrementGraceUsagesCount() {
		int count = getGraceUsagesCount();
		m_sharedPreferences.edit().putInt(PREFERENCE_GRACE_USAGES_COUNT, count).commit();
	}
	
	private Activity m_activity;
	IabHelper m_iabHelper;
	
	/**
	 * Begin the flow of checking if a license is purchased.  This method relies on a IabHelper to connect to the Google Play
	 * service.  It also handles the result of the connection attempt.
	 * 
	 * @param activity The Activity whose onActivityResult() method will be called with the outcome of the
	 *        purchase use case (e.g. a MainScreenUi).
	 */
	public void initiateCheckLicensePurchase(Activity activity) {
		m_activity = activity;
		m_iabHelper = new IabHelper(activity, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv3aykbfNFWBG4np/swunPo5qJT1Qic+qSG7OcKTQuy7qzt1IkYLfaT6I7IbWRzq1RZVGwM9/4R0j/Vq9Mrewvjc27AHxgDeqov/7njaTJWmnR9sBNWBPMh4O4CJif/zzHBrR4pOBzbPyAFWyG+SKIfE6u/wMjlR3pnPM3hcCO6WKQrxj9QGL/mpZunB+PQC+DDnqxkoW0xMDfdS4I6xaZWnbn3clqMoH6pWbP/p/jJ5bkXFIRi+amdCKPs9CH7zOOovJBQ3JcJU4xXrmyKZEapc8QSi27roMtofQqnw+Rx6GH+J3hZ8fkK4B8dufU08PQrFDKfscCYyMwXF/INFdeQIDAQAB");
		
		// Determine if Google Play is installed.  If not, then return to avoid crashing.
		Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
		serviceIntent.setPackage("com.android.vending");
		PackageManager pm = activity.getPackageManager();
		List<ResolveInfo> is = pm.queryIntentServices(serviceIntent, 0);
		if (is == null) return;
		
		m_iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			@Override
			public void onIabSetupFinished(IabResult result) {
				if (result.isFailure()) {
					// Handle failure connecting to the Google Play service.
					Toast.makeText(m_activity, "License check FAILED: No Google Play", Toast.LENGTH_LONG).show();
					//Log.d(TAG, "onIabSetupFinished: failure: result = " + result);
				} else {
					// Success!  Get the list of items purchased.
					m_iabHelper.flagEndAsync();
					m_iabHelper.queryInventoryAsync(m_queryInventoryFinishedListener);
				}
			}
		});
	}
	
	// This object handles the result of an items request that we make to the Google Service.
	private IabHelper.QueryInventoryFinishedListener m_queryInventoryFinishedListener = new QueryInventoryFinishedListener() {
		@Override
		public void onQueryInventoryFinished(IabResult result, Inventory inv) {
			if (result.isFailure()) {
				// handle error here
				Toast.makeText(m_activity, "License check FAILED: Inventory failure", Toast.LENGTH_LONG).show();
				Log.d(TAG, "onQueryInventoryFinished: failure: result = " + result);
			} else {
				// If the license is already purchased, then provision it.
				if (DebugSettings.m_showLicenseCheckToasts) {
					if (inv.hasPurchase(LICENSE_SKU)) {
						Toast.makeText(m_activity, "License is EXTANT", Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(m_activity, "License is NOT EXTANT", Toast.LENGTH_LONG).show();
					}
				}

				setIsLicensePurchased(inv.hasPurchase(LICENSE_SKU));
			}

			// Disconnect from the Google Play service.
			m_iabHelper.dispose();
			m_iabHelper = null;
		}
	};
	
	/**
	 * Begin the flow of purchasing a license.  This method relies on a IabHelper to connect to the Google Play
	 * service.  It also handles the result of the connection attempt.
	 * 
	 * @param activity The Activity whose onActivityResult() method will be called with the outcome of the
	 *        purchase use case (e.g. an EditShoppingListUi).
	 */
	public void initiateBuyLicense(Activity activity) {
		m_activity = activity;
		m_iabHelper = new IabHelper(activity, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvzMdkkEw22BlbTaV31FLSXfnkia0qXhrWMcOS0vOAxrQIvR/FsgWWA3JSIhJ/l7Cil1NDyrcq8NrZ8A30JgIByUK/YHZMLIOnNk/hdRS9qZIZoQc5tle3UVRMhBNz5fZyGwsNWphRFJcx/abwRS1JMsf8gARcCaLTCZocL30kKNgL4OwB2V6PS+UJQF0dMpNC68J9TexAxrzQos/yhYDr6/5NuF36Nt3RAG1Lb0HpetESKCAxtZWl2Fp+zB2tzB7DLK79O5C8ZhW6x13t1k0kmNBNTKFfIIrGoJYEyf7RpG3avSWmpDUG2a63u2NslKfSat30xN9bm/QWJTX9bRJjQIDAQAB");
		m_iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			@Override
			public void onIabSetupFinished(IabResult result) {
				if (result.isFailure()) {
					// Handle failure connecting to the Google Play service.
					CharSequence text = m_activity.getString(R.string.no_billing_service);
				    Toast.makeText(m_activity, text, Toast.LENGTH_LONG).show();
				} else {
					// Success!  Send the purchase request.
					m_iabHelper.launchPurchaseFlow(m_activity, LICENSE_SKU, PURCHASE_ACTIVITY_REQUEST_CODE, m_purchaseFinishedListener, generatePurchaseNonce());
				}
			}
		});
	}
	
	private String m_purchaseNonce = "";
	private String generatePurchaseNonce() {
		// TODO: Generate a random value here.
		m_purchaseNonce = "";
		return (m_purchaseNonce);
	}

	/**
	 * Handle the onActivityResult() dispatched to the Activity from which the purchase flow was started.
	 * @param resultCode The integer result code returned by the child activity through its setResult().
	 * @param data An {@link Intent} containing purchase order data as described in In-app Billing Reference (IAB Version 3).
	 * @see <a href="http://developer.android.com/google/play/billing/billing_reference.html#getBuyIntent">In-app Billing Reference (IAB Version 3)</a>
	 */
	public void onLicensePurchaseResult(int resultCode, Intent data) {
		m_iabHelper.handleActivityResult(PURCHASE_ACTIVITY_REQUEST_CODE, resultCode, data);
	}
	
	// This object handles the result of a purchase request that we make to the Google Service.
	private IabHelper.OnIabPurchaseFinishedListener m_purchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) 
		{
			if (result.isFailure()) {
				Log.d(TAG, "onIabPurchaseFinished(): failure: result = " + result);
				return;
			}      
			else if (purchase.getSku().equals(LICENSE_SKU)) {
				// TODO: Verify that m_purchaseNonce == purchase.getDeveloperPayload()
	
				// All is well.  Cache the fact that we purchased a license.  Subsequent calls
				// to isLicensePurchased() will return true.
				setIsLicensePurchased(true);
			}
	
			// Disconnect from the Google Play service.
			m_iabHelper.dispose();
			m_iabHelper = null;
		}
	};
}
