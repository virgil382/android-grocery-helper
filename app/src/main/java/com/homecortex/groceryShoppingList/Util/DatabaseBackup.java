package com.homecortex.groceryShoppingList.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import android.os.Environment;

public class DatabaseBackup {
	File m_currentDir;
	File m_currentDB;
	File m_backupDir;
	File m_backupDB;

	public DatabaseBackup(String packageName, String dbName) {
		String currentDirPath = "/data/" + packageName + "/databases";
		String currentDBPath = currentDirPath + "/" + dbName;
		String backupDirPath = "com.vmocanu.groceryHelper";
		String backupDBPath = backupDirPath + "/" + dbName;

		File sd = Environment.getExternalStorageDirectory();
		File data = Environment.getDataDirectory();
		m_currentDir = new File(data, currentDirPath);
		m_currentDB = new File(data, currentDBPath);
		m_backupDir = new File(sd, backupDirPath);
		m_backupDB = new File(sd, backupDBPath);
	}
	
	public boolean isBackupEnabled() {
		return(m_backupDir.exists());
	}
	
	public void createBackupDir() {
		m_backupDir.mkdirs();
	}
	
    public boolean backup() {
		try {
			m_backupDir.mkdirs();
			copyData(m_currentDB, m_backupDB);
            return (true);
        } catch(IOException e) {
        	e.printStackTrace();
            return(false);
        }
    }

    public boolean restore() {
		try {
			m_currentDir.mkdirs();
			copyData(m_backupDB, m_currentDB);
            return (true);
        } catch(IOException e) {
        	e.printStackTrace();
            return(false);
        }
    }
    
    private void copyData(File sourceFile, File destinationFile) throws IOException {
		FileChannel source;
		FileChannel destination;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destinationFile).getChannel();
        destination.transferFrom(source, 0, source.size());
        source.close();
        destination.close();
    }
}
