package com.homecortex.groceryShoppingList.Util;

public class DebugSettings {
	public static boolean m_showDbBackupRestoreToasts = true;
	public static boolean m_showAppVersionToasts = true;
	public static boolean m_showLicenseCheckToasts = false;
}
