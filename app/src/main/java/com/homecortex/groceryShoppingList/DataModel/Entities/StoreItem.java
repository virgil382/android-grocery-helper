// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Entities;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import android.support.annotation.NonNull;
import android.util.Log;

import com.homecortex.groceryShoppingList.DataModel.ChildEntityCollectionManager;
import com.homecortex.groceryShoppingList.DataModel.EntityAutoLoader;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.NaturalShoppingOrderLearningExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.StoreItemEdgeExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.StoreItemExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IChildEntity;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.homecortex.groceryShoppingList.TopologicalSort.DirectedGraph;
import com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList.SaveItemsOrderTask;
import com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList.SaveItemsOrderTask.CancelledException;
import com.homecortex.groceryShoppingList.Util.ObjectUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * A StoreItem associates an Item with a Store to represent the fact that the Item is sold in that
 * Store.  An Item may be sold in many different Stores, and a Store may sell many different Items.
 * StoreItem is used to represent this many-to-many relationship between Items and Stores.
 * 
 * StoreItems may be ordered within the Store with respect to each other.  StoreItemEdges are used
 * to represent the relative order.  A StoreItem has a Collection of inbound StoreItemEdges and a
 * Collection of outbound StoreItemEdges.
 */
@DatabaseTable(tableName = "storeItem")
public class StoreItem implements Comparable<StoreItem>, IChildEntity {
	private static final String TAG = "StoreItem";

	/// ORMLite needs a parameterless constructor.
	public StoreItem() { }

	public StoreItem(UUID storeId, UUID itemId) { 
		this.storeId = storeId;
		this.itemId = itemId;
		StoreItemExtent.instance().create(this);
	}
	
	private boolean m_isOpen = false;
	@Override
	public void open() {
		if (m_isOpen) return;
		m_isOpen = true;

//		m_itemAutoLoader.open();   // TODO: Remove after debug

		//m_inboundEdgesManager.open();
		//m_outboundEdgesManager.open();
	}
	@Override
	public void close() {
		if (!m_isOpen) return;
		m_isOpen = false;

//		m_itemAutoLoader.close();   // TODO: Remove after debug

		m_inboundEdgesManager.close();
		m_outboundEdgesManager.close();
	}

	@Override
	public void delete() {
		StoreItemExtent.instance().delete(this);
	}
	
	@DatabaseField(generatedId = true)
	public UUID storeItemId;
	public UUID getId() { return storeItemId; }
	public void setId(UUID newValue) {
		if(ObjectUtils.equals(newValue, storeItemId)) return;
		storeItemId = newValue;
		StoreItemExtent.instance().update(this, new UpdateSet("storeItemId"));
	}

	/// The Item being sold in the Store
	@DatabaseField(canBeNull = false, index = true)
	UUID itemId;
	public UUID getItemId() { return(itemId); }
//	public void setItemId(UUID newValue) {
//		if(ObjectUtils.equals(newValue, itemId)) return;
//
//		itemId = newValue;
//		StoreItemExtent.instance().update(this, new UpdateSet("itemId"));
//	}
	
	// The Store in which the Item is sold.
	@DatabaseField(canBeNull = false, index = true)
	UUID storeId;
	public UUID getStoreId() { return(storeId); }
//	public void setStoreId(UUID newValue) {
//		if(ObjectUtils.equals(newValue, storeId)) return;
//
//		storeId = newValue;
//		StoreItemExtent.instance().update(this, new UpdateSet("storeId"));
//	}

	@Override
	public void setParent(String foreignKeyName, IEntity parent) { }  // no need to reference parents

	@DatabaseField(canBeNull = true)
	Integer precalculatedSortIndex;
	public void setPrecalculatedSortIndex(Integer newValue) throws SQLException {
		if(ObjectUtils.equals(newValue, precalculatedSortIndex)) return;

		precalculatedSortIndex = newValue;
		StoreItemExtent.instance().update(this, new UpdateSet("precalculatedSortIndex"));
	}
	public Integer getPrecalculatedSortIndex() {
		return(precalculatedSortIndex);
	}
		
	///////////////// Inbound StoreItemEdges.
	private ChildEntityCollectionManager<StoreItemEdge> m_inboundEdgesManager =
			new ChildEntityCollectionManager<>(this, "headStoreItemId", StoreItemEdgeExtent.instance());
	public Collection<StoreItemEdge> getInboundEdges() {
		m_inboundEdgesManager.open();
		return(m_inboundEdgesManager.getCollection());
	}

	///////////////// Outbound StoreItemEdges.
	private ChildEntityCollectionManager<StoreItemEdge> m_outboundEdgesManager =
			new ChildEntityCollectionManager<>(this, "tailStoreItemId", StoreItemEdgeExtent.instance());
	public Collection<StoreItemEdge> getOutboundEdges() {
        m_outboundEdgesManager.open();
        return(m_outboundEdgesManager.getCollection());
    }
	
	// Price per each 1 item.  Note that although it would be possible to support price per volume
	// or price per weight, that would be too complicated.
	@DatabaseField(canBeNull = true)
	Double unitPrice;
	public Double getUnitPrice() { return unitPrice; }
	public void setUnitPrice(Double newValue) {
		if(ObjectUtils.equals(newValue, unitPrice)) return;

		unitPrice = newValue;
		StoreItemExtent.instance().update(this, new UpdateSet("unitPrice"));
	}

	// Satisfy the requirements of Comparable<StoreItem>.
	@Override
	public int compareTo(@NonNull StoreItem another) {
		return getId().compareTo(another.getId());
	}

    // TODO: Remove after debug
    private EntityAutoLoader<StoreItem, Item> m_itemAutoLoader =
            new EntityAutoLoader<>(this, "itemId", StoreItemExtent.instance(), ItemExtent.instance());
    public Item getItem() {
        return(m_itemAutoLoader.getEntity());
    }

	@Override
	public String toString() {
		if (getItem() == null) return("[unavailable]");
		return getItem().toString();
	}

	/**
	 * Update this StoreItem's inbound and outbound StoreItemEdges to ensure that there are no
	 * paths to it from its successors, and that it has outbound edges to its successors.
	 * <p>
	 * First, remove the last segment of all paths from its successors to this StoreItem.  This
	 * ensures that, if any successor used to be a predecessor of this StoreItem's, then it no
	 * longer is.  This also preserves StoreItemEdges from successors to intermediate StoreItems
	 * on paths leading to this StoreItem so that their ordering is preserved.
	 * <p>
	 * Then, ensure that outbound StoreItemEdges exist from this StoreItem to all successors.
	 * This ensures that this node becomes their explicit predecessor. 
	 * <p>
	 * Note that inbound StoreItemEdges from its "surviving" predecessors (i.e. predecessors that
	 * have not become its successors) are unaffected.
	 * 
	 * @param successors A HashSet containing StoreItem that are or will become this StoreItem's
	 * successors.  Note that some of them may be its predecessors but will become its successors.
	 *  
	 * @throws SQLException 
	 * @throws CancelledException 
	 */
	public void updateStoreItemEdges(HashSet<StoreItem> successors, SaveItemsOrderTask task) throws SQLException, CancelledException
	{
		if (successors.size() == 0) return;
		
		Log.d(TAG, "updateStoreItemEdges: " + toString());
		List<StoreItemEdge> doomedStoreItemEdges = new ArrayList<>();

		StoreItemReachabilityOracle.instance().reset();

        // Break circular paths in the DAG.
        {
            // For each StoreItem P with a StoreItemEdge P->this,
            // determine if there is a path from any successors to P.  If there is, then doom the
            // StoreItemEdge P->this.
            for (StoreItemEdge inboundEdge : getInboundEdges()) {
                UUID predecessorId = inboundEdge.getTailStoreItemId();
                StoreItem predecessor = NaturalShoppingOrderLearningExtent.instance().getById(predecessorId);
                if (predecessor == null)
                    continue;  // should not happen

                boolean isPathExtant = predecessor.pathExists(successors, task);
                task.checkCancelledAndThrow("updateStoreItemEdges");
                if (isPathExtant)
                    doomedStoreItemEdges.add(inboundEdge);
            }

            // Now delete all the doomed StoreItemEdges.
            for (StoreItemEdge edge : doomedStoreItemEdges) {
                Log.d(TAG, "updateStoreItemEdges: Deleting edge " + edge.toString());
                edge.delete();
            }
        }

	    // Ensure that edges to successors exist (i.e. add them if they don't).
		UUID myID = getId();
	    for(StoreItem successor : successors) {
	    	StoreItemEdge edge = NaturalShoppingOrderLearningExtent.instance().find(myID, successor.getId());
	    	if (edge == null)
                edge = new StoreItemEdge(myID, successor.getId());
			Log.d(TAG, "updateStoreItemEdges: Ensuring edge " + edge.toString());
	    }
	}

	// Used by the DFS to mark nodes already visited (in the unlikely case of cycles).
	Boolean isVisited = false;
	
	/**
	 * Perform a DFS to determine if a path exists from any of the specified source StoreItems
	 * to this StoreItem.  If this node is among the sources, then a path exists.  A path also
	 * exists from this StoreItem to itself.
	 * 
	 * @param sources A HashSet of StoreItems containing the source StoreItems.
	 * @return True if a path exists from any of the StoreItems specified by sources to this
	 * StoreItems.
	 * @throws SQLException 
	 * @throws CancelledException 
	 */
	private boolean pathExists(HashSet<StoreItem> sources, SaveItemsOrderTask task) throws SQLException, CancelledException
	{
		//StringBuilder sourcesString = new StringBuilder();
		//for(StoreItem storeItem : sources) sourcesString.append(storeItem.toString()).append(" ");
		//Log.d(TAG, "pathExists: to: " + toString() + " from: " + sourcesString);
		task.checkCancelledAndThrow("pathExists 1");

		if (sources.contains(this)) return true;
		
		if (getInboundEdges().size() == 0) return false;

		isVisited = true;
	
		boolean result = false;

		// Iterate through all the inbound StoreItemEdges and recursively follow them to their source.
		for(StoreItemEdge inboundEdge : getInboundEdges()) {
			task.checkCancelledAndThrow("pathExists 2");
			UUID predecessorId = inboundEdge.getTailStoreItemId();
			StoreItem predecessor = NaturalShoppingOrderLearningExtent.instance().getById(predecessorId);
			if (predecessor == null) continue;  // should not happen
			
			// Ask the oracle.
			Boolean isPathExtant = StoreItemReachabilityOracle.instance().getIsNodeReachable(getId(), predecessorId);
			if (isPathExtant != null) {
				result = isPathExtant;
				break;
			}
			
			if (!predecessor.isVisited) {
				isPathExtant = predecessor.pathExists(sources, task);
				task.checkCancelledAndThrow("pathExists 3");
				if (isPathExtant) {
					result = true;
					StoreItemReachabilityOracle.instance().setIsNodeReachable(getId(), predecessorId, true);
					break;
				} else {
					StoreItemReachabilityOracle.instance().setIsNodeReachable(getId(), predecessorId, false);
				}
			}
		}
		
		isVisited = false;
		return result;
	}

	/**
	 * Build a DirectedGraph out of all the StoreItemEdges within the StoreItem.
	 * 
	 * @param dgraph The DirectedGraph<StoreItem> to build.
	 * @throws SQLException
	 */
	public void populateDirectedGraph(DirectedGraph<UUID> dgraph) throws SQLException
	{
		for(StoreItemEdge storeItemEdge : getOutboundEdges()) {
			UUID headId = storeItemEdge.getHeadStoreItemId();
			UUID tailId = storeItemEdge.getTailStoreItemId();
			if (headId == null && tailId == null) {
				// This should not happen.  If it does, then delete the edge from the DB.
				storeItemEdge.delete();
				return;
			}
			
			dgraph.addDirectedEdge(tailId, headId);
		}
	}
}
