// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Interfaces;

import java.util.Collection;

public interface IEntityCollectionQueryResultHandler<E> {
	void onResult(Collection<E> objects);

}
