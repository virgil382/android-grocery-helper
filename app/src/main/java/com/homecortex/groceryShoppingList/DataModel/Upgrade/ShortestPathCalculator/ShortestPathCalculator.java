package com.homecortex.groceryShoppingList.DataModel.Upgrade.ShortestPathCalculator;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Vector;

/**
 * A ShortestPathCalculator is used to build a directed acyclical graph (DAG) and to find the
 * shortest path between two vertices.
 *
 * @param <ConcreteEdge> A class implementing Edge.
 */
public class ShortestPathCalculator<ConcreteEdge extends Edge> {
    public class Vertex implements Comparable<Vertex> {
        private String m_name;
        public Vector<ConcreteEdge> m_adjacencies = new Vector<>();
        public Integer m_minDistance = Integer.MAX_VALUE;
        public Vertex m_previous;

        public Vertex(String name) {
            m_name = name;
        }

        public String toString() { return m_name; }

        public void addAdjacency(ConcreteEdge adjacency) {
            m_adjacencies.add(adjacency);
        }

        public int compareTo(@NonNull Vertex other) {
            return(m_minDistance.compareTo(other.m_minDistance));
        }
    }

    private HashMap<String, Vertex> m_vertices = new HashMap<>();
    private HashMap<String, ConcreteEdge> m_edges = new HashMap<>();

    /**
     * Add a ConcreteEdge between two vertices.
     * @param e The ConcreteEdge to be added.
     */
    public void addEdge(ConcreteEdge e) {
        Vertex sourceVertex = getVertex(e.getSourceName());
        sourceVertex.addAdjacency(e);
        m_edges.put(e.getSourceName() + "#" + e.getTargetName(), e);
    }

    /**
     * Ensure that a Vertex with the specified exists in the hashMap and return it.
     * @param name The name of the Vertex.
     * @return The Vertex with the specified name.
     */
    private Vertex getVertex(String name) {
        Vertex e = m_vertices.get(name);
        if(e == null) {
            e = new Vertex(name);
            m_vertices.put(name, e);
        }
        return(e);
    }

    /**
     * Get the shortest path between two vertices.
     * @param sourceVertexName The name of the source vertex.
     * @param targetVertexName The name of the target vertex.
     * @return A List of ConcreteEdges describing the path if successful, otherwise null.
     */
    public List<ConcreteEdge> getShortestPath(int sourceVertexName, int targetVertexName) {
        return(getShortestPath(String.valueOf(sourceVertexName), String.valueOf(targetVertexName)));
    }

    /**
     * Get the shortest path between two vertices.
     * @param sourceVertexName The name of the source vertex.
     * @param targetVertexName The name of the target vertex.
     * @return A List of ConcreteEdges describing the path if successful, otherwise null.
     */
    public List<ConcreteEdge> getShortestPath(String sourceVertexName, String targetVertexName) {
        computePaths(sourceVertexName);

        Vertex targetVertex = getVertex(targetVertexName);

        List<Vertex> path = new ArrayList<>();
        for(Vertex vertex = targetVertex; vertex != null; vertex = vertex.m_previous) {
            path.add(0, vertex);
        }

        if(path.size() < 2) return(null);

        List<ConcreteEdge> edges = new ArrayList<>();
        for(int i = 1; i < path.size(); i++) {
            Vertex v1 = path.get(i-1);
            Vertex v2 = path.get(i);
            ConcreteEdge e = m_edges.get(v1.m_name + "#" + v2.m_name);
            if (e == null) return(null);
            edges.add(e);
        }

        return(edges);
    }

    private void computePaths(String sourceVertexName) {
        Vertex sourceVertex = getVertex(sourceVertexName);

        sourceVertex.m_minDistance = 0;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<>();
        vertexQueue.add(sourceVertex);

        while (!vertexQueue.isEmpty()) {
            Vertex u = vertexQueue.poll();

            // Visit each edge exiting u
            for (Edge e : u.m_adjacencies)
            {
                Vertex v = getVertex(e.getTargetName());
                Integer distanceThroughU = u.m_minDistance + 1;
                if(distanceThroughU < v.m_minDistance) {
                    vertexQueue.remove(v);
                    v.m_minDistance = distanceThroughU;
                    v.m_previous = u;
                    vertexQueue.add(v);
                }
            }
        }
    }
}
