// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Extents;

import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.WeightUnit;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.j256.ormlite.dao.Dao;

public class WeightUnitExtent extends EntityService<WeightUnit> {
	private static WeightUnitExtent instance;
	public static WeightUnitExtent instance() {
		if (instance == null) instance = new WeightUnitExtent();
		return instance;
	}
	
	// The DB connection.
	Dao<WeightUnit, UUID> classDao;
	
	@Override
	public Dao<WeightUnit, UUID> classDao() {
		if (classDao != null) return classDao;
		classDao = ApplicationPm.getDatabaseHelper().getWeightUnitDao();
		return classDao;
	}

	@Override
	public void delete(WeightUnit doomedObject) { }
}
