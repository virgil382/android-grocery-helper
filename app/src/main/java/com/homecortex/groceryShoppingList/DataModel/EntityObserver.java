// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel;

import com.homecortex.groceryShoppingList.DataModel.Entities.UpdateSet;

public class EntityObserver<E> {
	public void open() {};
	public void close() {};
	public void onCreate(E object) {};
	public void onUpdate(E object, UpdateSet updates) {};
	
	// Only the ID will be valid.
	public void onDelete(E object) {};
}
