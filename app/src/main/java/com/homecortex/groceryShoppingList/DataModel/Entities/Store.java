// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Entities;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import android.util.Log;

import com.homecortex.groceryShoppingList.DataModel.ChildEntityCollectionManager;
import com.homecortex.groceryShoppingList.DataModel.Extents.EntityService;
import com.homecortex.groceryShoppingList.DataModel.Extents.NaturalShoppingOrderLearningExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.StoreExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.StoreItemExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.homecortex.groceryShoppingList.TopologicalSort.DirectedGraph;
import com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList.SaveItemsOrderTask;
import com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList.SaveItemsOrderTask.CancelledException;
import com.homecortex.groceryShoppingList.Util.ObjectUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * A Store contains a Collection of StoreItems that indicate which Items are sold in the Store.  These
 * Items can be sorted to obtain the user's "natural shopping order" at that Store.  This order is
 * calculated with the help of a directed graph whose StoreItems vertexes are connected via StoreItemEdges,
 * each of which represents the order in which the user shopped (i.e. picked up) a StoreItem relative to
 * another StoreItem.
 */
@DatabaseTable(tableName = "store")
public class Store implements IEntity
{
	private static final String TAG = "Store";

	/// ORMLite needs a parameterless constructor.
	public Store() {}

	public Store(String name) {
		this.name = name;
		StoreExtent.instance().create(this);
	}

	private boolean m_isOpen = false;
	@Override
	public void open() {
		if (m_isOpen) return;
		m_isOpen = true;
		m_childStoreItemsManager.open();
	}
	@Override
	public void close() {
		if (!m_isOpen) return;
		m_isOpen = false;
		m_childStoreItemsManager.close();
	}

	@Override
	public void delete() {
		StoreExtent.instance().delete(this);
	}

	@DatabaseField(generatedId = true)
	private UUID storeId;
	public void setId(UUID newValue) {
		if(ObjectUtils.equals(newValue, storeId)) return;

		storeId = newValue;
		StoreExtent.instance().update(this, new UpdateSet("storeId"));
	}

	@Override
	public UUID getId() { return storeId; }

	@DatabaseField
	private String name;
	public void setName(String newValue) {
		if(ObjectUtils.equals(newValue, name)) return;

		name = newValue;
		StoreExtent.instance().update(this, new UpdateSet("name"));
	}
	public String getName() { return name; }

	// TODO: Remove after debugging.
	@Override
	public String toString() {
		return name;
	}

	@DatabaseField
	private Double latitude;
	public Double getLatitude() { return latitude; }
	
	@DatabaseField
	private Double radius;
	public Double getRadius() { return radius; }
	
	@DatabaseField
	private Double longitude;
	public Double getLongitude() { return longitude; }
	
	public void setCoordinates(double lat, double lng, double r) {
		latitude = lat;
		longitude = lng;
		radius = r;
		StoreExtent.instance().update(this, new UpdateSet("latitude", "longitude", "radius"));
	}
	
	@DatabaseField
	private Integer sortOracleVersion;
	public int getSortOracleVersion() {
		if (sortOracleVersion == null) return(0);
		return(sortOracleVersion);
	}
	public void setSortOracleVersion(Integer newValue) {
		if(ObjectUtils.equals(newValue, sortOracleVersion)) return;

		sortOracleVersion = newValue;
		StoreExtent.instance().update(this, new UpdateSet("sortOracleVersion"));
	}
	public void incrementSortOracleVersion() {
		int previousVersion = getSortOracleVersion();
		setSortOracleVersion(previousVersion + 1);
	}
	
	/**
	 * StoreItemsManager maintains the child StoreItems of this store and a hash map of precalculated
	 * sort indexes (i.e. the sort oracle) that maps UUIDs -> integer indexes. 
	 */
	private class StoreItemsManager extends ChildEntityCollectionManager<StoreItem> {
		private HashMap<UUID,Integer> m_sortOracle = new HashMap<>();
		
		public StoreItemsManager(IEntity parent, String foreignKeyName,
				EntityService<StoreItem> entityService) {
			super(parent, foreignKeyName, entityService);
		}
		
		public HashMap<UUID,Integer> getSortOracle() {
			return(m_sortOracle);
		}

		@Override
		public void onResult(Collection<StoreItem> objects) {
			super.onResult(objects);
			
			m_sortOracle.clear();
			
			for(StoreItem object : objects) {
				if (!isManagedChild(object)) continue;
				m_sortOracle.put(object.getItemId(), object.getPrecalculatedSortIndex());
			}
		}
		
		@Override
		public void onCreate(StoreItem object) {
			super.onCreate(object);
			if (!isManagedChild(object)) return;
			m_sortOracle.put(object.getItemId(), object.getPrecalculatedSortIndex());
		}

		@Override
		public void onUpdate(StoreItem object, UpdateSet updates) { 
			super.onUpdate(object, updates);
			if (!isManagedChild(object)) return;
			m_sortOracle.put(object.getItemId(), object.getPrecalculatedSortIndex());
		}

		@Override
		public void onDelete(StoreItem object) {
			super.onDelete(object);
			if (!isManagedChild(object)) return;
			m_sortOracle.remove(object.getId());
		}
	}
	private StoreItemsManager m_childStoreItemsManager = new StoreItemsManager(this, "storeId", StoreItemExtent.instance());
	public HashMap<UUID,Integer> getSortOracle() { return(m_childStoreItemsManager.getSortOracle()); }
	public Collection<StoreItem> getStoreItems() { return(m_childStoreItemsManager.getCollection()); }

	/**
	 * Update the StoreItemEdges for this Store's StoreItems so that the resulting graph represents
	 * the user's natural shopping oder at this Store.  The update is performed based on information
	 * gleaned from a subset of StoreItems that is ordered according to the user's natural shopping
	 * order.
	 * 
	 * @param orderedStoreItems The ordered subset of StoreItems.
	 * @throws SQLException 
	 * @throws CancelledException 
	 */
	public void updateNaturalShoppingOrder(List<StoreItem> orderedStoreItems, SaveItemsOrderTask task) throws SQLException, CancelledException {
		//Log.d(TAG, "updateNaturalShoppingOrder: Items being saved are:");
		//for(StoreItem storeItem : orderedStoreItems) Log.d(TAG, storeItem.toString());
		
		// Fill a HashSet with all the StoreItems.  It will be used to keep track of a StoreItem's
		// successors.
		HashSet<StoreItem> successors = new HashSet<>();
		for(StoreItem storeItem : orderedStoreItems) successors.add(storeItem);
		
		// Total number of steps includes:
		// - iterating through store items to remove loops: O(orderedStoreItems)
		// - build directed graph: O(storeItems)
		// - updating store items sort index: O(storeItems)
		int nSteps = orderedStoreItems.size() + getStoreItems().size() * 2;
		task.setTotalSteps(nSteps);

		// For each StoreItem in the ordered set, drop the StoreItem from the HashSet (to leave its
		// successors) and update the StoreItem's edges to its successors.
		for(StoreItem storeItem : orderedStoreItems) {
			Log.d(TAG, "updateNaturalShoppingOrder(): updating " + storeItem.toString());
			successors.remove(storeItem);
			storeItem.updateStoreItemEdges(successors, task);
			task.checkCancelledAndThrow("updateNaturalShoppingOrder");
			task.incrementStep();
		}
		
		// Recalculate this Store's StoreItems pre-calculated sort indexes based on new information.
		updateStoreItemsOrder(task);

		// Reset the oracle to free some memory.
		StoreItemReachabilityOracle.instance().reset();
	}

	/**
	 * Recalculate this Store's StoreItems pre-calculated sort indexes based on available StoreItemEdges,
	 * and update them in the database.
	 * @throws SQLException
	 * @throws CancelledException 
	 */
	public void updateStoreItemsOrder(SaveItemsOrderTask task) throws SQLException, CancelledException
	{
		// Recalculate the List of Items in "natural shopping order".
		DirectedGraph<UUID> dgraph = constructDirectedGraph(task);
		task.checkCancelledAndThrow("updateNaturalShoppingOrder");

		List<UUID> sortedStoreItemIds = dgraph.topologicalSort();

		int index = 0;
		for(UUID sortedStoreItemId : sortedStoreItemIds) {
			StoreItem storeItem = NaturalShoppingOrderLearningExtent.instance().getById(sortedStoreItemId);
			
			Log.d(TAG, "updateStoreItemsOrder(): index=" + index + ": " + storeItem.toString());

			storeItem.setPrecalculatedSortIndex(index++);

			task.incrementStep();
			task.checkCancelledAndThrow("updateNaturalShoppingOrder");
		}
		
		incrementSortOracleVersion();
	}

	/**
	 * Build and returns a DirectedGraph out of all the StoreItems and StoreItemEdges within the Store.
	 * 
	 * @param task The SaveItemsOrderTask in the context of which this operation is being carried out.
	 * @return A The DirectedGraph<StoreItem> built out of all the StoreItems and StoreItemEdges
	 *         within the Store.
	 * @throws SQLException
	 * @throws CancelledException 
	 */
	protected DirectedGraph<UUID> constructDirectedGraph(SaveItemsOrderTask task) throws SQLException, CancelledException
	{
		DirectedGraph<UUID> dgraph = new DirectedGraph<>();
		for(StoreItem storeItem : getStoreItems()) {
			storeItem.populateDirectedGraph(dgraph);
			task.incrementStep();
			task.checkCancelledAndThrow("populateDirectedGraph");
		}
		return dgraph;
	}
}
