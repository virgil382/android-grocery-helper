package com.homecortex.groceryShoppingList.DataModel.Entities.Helpers;

import com.homecortex.groceryShoppingList.DataModel.Entities.ItemPurchaseEvent;
import com.homecortex.groceryShoppingList.DataModel.EntityObserver;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemPurchaseEventExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.SettingsExtent;

import java.util.Date;
import java.util.UUID;

/**
 * An ItemPurchaseFrequencyCalculator determines how frequently an Item (i.e. the subject Item)
 * has been purchased during a span of time up to the current time.  To do so, it needs the
 * following data:
 * - the subject Item ID
 * - the start Date of the span of time
 * - the current Date
 * - the Item purchase event data (i.e. ItemPurchaseEvents)
 *
 * For efficiency, an ItemPurchaseFrequencyCalculator recalculates the frequency only when
 * necessary.  It does so if any of the following events occur:
 *   - a new ItemPurchaseEvent is create (pertaining to the subject Item)
 *   - the start date of the span of time has changed
 */
public class ItemPurchaseFrequencyCalculator extends EntityObserver<ItemPurchaseEvent> {
    private UUID m_itemId;
    private Integer m_frequency = 0;
    private boolean m_isOpen = false;

    // Used to optimize the performance of the calculation.
    private static long RECALCULATION_FREQUENCY_IN_MILLIS = 2 * 60 * 60 * 1000;  // every 2 hours
    private long m_lastRecalculationTime = 0;

    /**
     * Get the start date of the span of time that SHOULD be used for the frequency calculation.
     * The returned value may be different than the m_effectiveStartOfSpan, in which case, the
     * frequency should be recalculated.
     * @return The start date of the span of time that SHOULD be used for the frequency
     * calculation.
     */
    private Date getStartOfSpan() {
        return(SettingsExtent.instance().getStartOfItemPurchaseFrequencyTimeSpan());
    }

    public ItemPurchaseFrequencyCalculator(UUID itemId) {
        m_itemId = itemId;
    }

    @Override
    public void open() {
        if(m_isOpen) return;
        m_isOpen = true;
        ItemPurchaseEventExtent.instance().addObserver(this);
    }

    @Override
    public void close() {
        if(!m_isOpen) return;
        m_isOpen = false;
        ItemPurchaseEventExtent.instance().dropObserver(this);
    }

    public void onCreate(ItemPurchaseEvent object) {
        if(!m_itemId.equals(object.getItemId())) return;
        m_frequency++;
    }

    public Integer getPurchaseFrequency() {
        // Determine if we should recalculate the frequency.  We do this as an optimization.
        long currentTimeInMillis = System.currentTimeMillis();
        if(currentTimeInMillis - RECALCULATION_FREQUENCY_IN_MILLIS > m_lastRecalculationTime) {
            Date startOfSpanDate = getStartOfSpan();
            m_frequency = ItemPurchaseEventExtent.instance().getCount(startOfSpanDate, m_itemId);
            m_lastRecalculationTime = currentTimeInMillis;
        }

        return(m_frequency);
    }

    public String toString() {
        return(String.valueOf(m_frequency));
    }
}
