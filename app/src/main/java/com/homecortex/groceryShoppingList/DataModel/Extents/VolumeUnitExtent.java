// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Extents;

import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.VolumeUnit;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.j256.ormlite.dao.Dao;

public class VolumeUnitExtent extends EntityService<VolumeUnit> {
	private static VolumeUnitExtent instance;
	public static VolumeUnitExtent instance() {
		if (instance == null) instance = new VolumeUnitExtent();
		return instance;
	}
	
	// The DB connection.
	Dao<VolumeUnit, UUID> classDao;
	
	@Override
	public Dao<VolumeUnit, UUID> classDao() {
		if (classDao != null) return classDao;
		classDao = ApplicationPm.getDatabaseHelper().getVolumeUnitDao();
		return classDao;
	}

	@Override
	public void delete(VolumeUnit doomedObject) { }
}
