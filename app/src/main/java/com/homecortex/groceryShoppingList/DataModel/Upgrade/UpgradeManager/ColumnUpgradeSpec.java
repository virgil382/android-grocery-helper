// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Upgrade.UpgradeManager;

import java.sql.SQLException;

import android.util.Log;

import com.j256.ormlite.dao.Dao;

public class ColumnUpgradeSpec {
	private static final String TAG = "DatabaseUpgrade";

	private String m_parentTableName;

	private String m_name;
	private String m_newName;

	private String m_type;
	private String m_newType;
	private TypeMapper m_typeMapper;

	private boolean m_isIndex;
	private Boolean m_newIsIndex;

	private boolean m_isKey;
	private Boolean m_newIsKey;

	// for foreign key
	private String m_targetTableName;
	private String m_targetKeyName;

	private boolean m_isObsolete;
	private boolean m_isNew;

	public ColumnUpgradeSpec parentTableName(String parentTableName) {
		m_parentTableName = parentTableName;
		return(this);
	}
	public String parentTableName() { return(m_parentTableName); }
	
	public ColumnUpgradeSpec(String name, String type) {
		m_name = name;
		m_type = type;
	}
	public String name() { return(m_name); }
	public String type() { return(m_type); }
	public String temporaryName() { return(m_name + "Tmp"); }

	public ColumnUpgradeSpec newName(String value) {
		m_newName = value;
		return(this);
	}
	public String newName() {
		if (m_newName != null) return(m_newName);
		return(m_name);
	}

	public ColumnUpgradeSpec newType(String value) {
		m_newType = value;
		return(this);
	}
	public String newType() {
		return(m_newType);
	}

	public ColumnUpgradeSpec typeMapper(TypeMapper value) {
		m_typeMapper = value;
		return(this);
	}
	public TypeMapper typeMapper() {
		return(m_typeMapper);
	}

	public ColumnUpgradeSpec isIndex(boolean value) {
		m_isIndex = value;
		return(this);
	}
	public boolean isIndex() { return(m_isIndex); }

	public ColumnUpgradeSpec newIsIndex(boolean value) {
		m_newIsIndex = value;
		return(this);
	}
	public boolean newIsIndex() {
		if(m_newIsIndex != null) return(m_newIsIndex);
		return(m_isIndex);
	}
	
	public ColumnUpgradeSpec isKey(boolean value) {
		m_isKey = value;
		return(this);
	}
	public boolean isKey() { return(m_isKey); }

	public ColumnUpgradeSpec newIsKey(boolean value) {
		m_newIsKey = value;
		return(this);
	}
	public boolean newIsKey() {
		if(m_newIsKey != null) return(m_newIsKey);
		return(m_isKey);
	}
	
	public ColumnUpgradeSpec foreignKey(String targetTableName, String targetKeyName) {
		m_targetTableName = targetTableName;
		m_targetKeyName = targetKeyName;
		return(this);
	}
	public boolean isForeignKeyFor(String targetTableName, String targetKeyName) {
		if(targetTableName == null || targetKeyName == null) return(false);
		if(m_targetTableName == null || m_targetKeyName == null) return(false);
		
		if(!m_targetTableName.equals(targetTableName)) return(false);
		if(!m_targetKeyName.equals(targetKeyName)) return(false);
		return (true);
	}

	public ColumnUpgradeSpec isObsolete(boolean value) {
		m_isObsolete = value;
		return(this);
	}
	public boolean isObsolete() {
		return(m_isObsolete);
	}
	
	public ColumnUpgradeSpec isNew(boolean value) {
		m_isNew = value;
		return(this);
	}
	public boolean isNew() {
		return(m_isNew);
	}
	
	public void addTemporaryColumn(String tableName, Dao<Bogus, Void> dao) throws SQLException {
		String s = "ALTER TABLE " + tableName + " ADD COLUMN " + temporaryName() + " " + m_newType + ";";
		Log.d(TAG, s);
		dao.executeRaw(s);
	}
	
	public void dropIndexIfExtant(String tableName, Dao<Bogus, Void> dao) throws SQLException {
		String s = "DROP INDEX IF EXISTS " + tableName + "_" + name() + "_idx;";
		Log.d(TAG, s);
		dao.executeRaw(s);
	}

	public void createIndex(String tableName, Dao<Bogus, Void> dao) throws SQLException {
		String s = "CREATE INDEX " + tableName + "_" + newName() + "_idx ON " + tableName + " (" + newName() + ");";
		Log.d(TAG, s);
		dao.executeRaw(s);
	}
}
