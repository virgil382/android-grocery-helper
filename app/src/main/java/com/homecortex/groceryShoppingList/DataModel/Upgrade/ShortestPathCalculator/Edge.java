package com.homecortex.groceryShoppingList.DataModel.Upgrade.ShortestPathCalculator;

/**
 * An Edge is used to represent a path between two named vertices.
 */
public interface Edge {
    String getSourceName();
    String getTargetName();
}
