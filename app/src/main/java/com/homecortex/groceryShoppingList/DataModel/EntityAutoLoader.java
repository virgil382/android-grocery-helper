// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel;

import com.homecortex.groceryShoppingList.DataModel.Entities.UpdateSet;
import com.homecortex.groceryShoppingList.DataModel.Extents.EntityService;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntityQueryResultHandler;
import java.lang.reflect.Field;
import java.util.UUID;

/**
 * An EntityAutoLoader automatically loads an Entity A (the auto-loaded entity) whenever the value
 * of the field D.foreignKeyFieldName of a dependent entity D changes.  I.e. D.foreignKeyFieldName
 * specifies the ID of the entity A that should be (re)loaded.
 * 
 * An EntityAutoLoader subscribes to update events of D.  If D is updated, and if the UpdateSet
 * contains D.foreignKeyFieldName, then the EntityAutoLoader reloads A from the database.
 *
 * @param <D> The type of D.
 * @param <A> The type of A.
 */
public class EntityAutoLoader<D extends IEntity, A extends IEntity> extends EntityObserver<D> {
    private boolean m_isOpen = false;
	private final D m_dependentEntity;
	private final String m_foreignKeyFieldName;
    private final Field m_foreignKeyField;

	private final EntityService<D> m_dependentEntityService;
	private final EntityService<A> m_autoLoadedEntityService;
	private A m_autoLoadedEntity;

	public EntityAutoLoader(D dependentEntity, String foreignKeyFieldName, EntityService<D> dependentEntityService, EntityService<A> autoLoadedEntityService) {
		m_dependentEntity = dependentEntity;
		m_foreignKeyFieldName = foreignKeyFieldName;
		m_dependentEntityService = dependentEntityService;
		m_autoLoadedEntityService = autoLoadedEntityService;

        Field foreignKeyField = null;
        try {
            foreignKeyField = dependentEntity.getClass().getDeclaredField(foreignKeyFieldName);
            foreignKeyField.setAccessible(true);
        } catch (NoSuchFieldException e) {
            // Ignore.  fail later.
            e.printStackTrace();
        }
        m_foreignKeyField = foreignKeyField;
	}

	@Override
	public void open() {
        if(m_isOpen) return;
        m_isOpen = true;
		m_dependentEntityService.addObserver(this); refreshAutoLoadedEntity();
	}
	
	@Override
	public void close() {
        if(!m_isOpen) return;
        m_isOpen = false;
        m_dependentEntityService.dropObserver(this);
    }

	@Override
	public void onCreate(D object) { }

	@Override
	public void onUpdate(D object, UpdateSet updates) {
		if (object != m_dependentEntity) return;
		if(!updates.contains(m_foreignKeyFieldName)) return;
		m_autoLoadedEntity = null;
		refreshAutoLoadedEntity();
	}

	private void refreshAutoLoadedEntity() {
        // From D, get the value of the field named m_foreignKeyFieldName.
        try {
			UUID foreignKeyFieldValue = (UUID) m_foreignKeyField.get(m_dependentEntity);
			m_autoLoadedEntityService.getById(foreignKeyFieldValue, new IEntityQueryResultHandler<A>() {
				@Override
				public void onResult(A object) {
                    m_autoLoadedEntity = object;
				}
			});
		} catch(IllegalAccessException e) {
			// Eat it.  Detect it below.
		}
	}

	@Override
	public void onDelete(D object) { }
	
	public A getEntity() { return(m_autoLoadedEntity); }
}
