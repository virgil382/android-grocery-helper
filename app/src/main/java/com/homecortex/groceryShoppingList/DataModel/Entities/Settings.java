// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Entities;

import java.sql.SQLException;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Extents.SettingsExtent;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "settings")
public class Settings {
	@DatabaseField(generatedId = true)

	public UUID settingsId;
	public UUID getId() { return settingsId; }

	/// ORMLite needs a parameterless constructor.
	public Settings() { }
	
	/**
	 * Create a database representation of this object.
	 * 
	 * @throws SQLException
	 */
	public void create() throws SQLException
	{
		if (getId() == null) {
			Dao<Settings, UUID> classDao = ApplicationPm.getDatabaseHelper().getSettingsDao();
			classDao.create(this);
		}
	}
	
	public void update() throws SQLException
	{
		SettingsExtent.instance().update(this);
	}
}
