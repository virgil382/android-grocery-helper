// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Upgrade.UpgradeManager;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.field.DataType;

public class IntegerToUuidTypeMapper implements TypeMapper {
	private static final String TAG = "DatabaseUpgrade";

	static IntegerToUuidTypeMapper m_instance = new IntegerToUuidTypeMapper();
	
	public static TypeMapper instance() {
		return(m_instance);
	}
	
	@Override
	public String newType() {
		return("TEXT");
	}

	@Override
	public void mapColumnValues(Dao<Bogus, Void> dao, ColumnUpgradeSpec keyCs, List<ColumnUpgradeSpec> foreignKeys) throws SQLException {
		// Query for unique values in keyCs
		// Iterate through each value.
		// Map value to new type.
		// Set the temporary column value of keyCs to the mapped value.  
		// Do the same for each foreign key value.
		String sqlStatement = "SELECT DISTINCT " + keyCs.name() + " FROM " + keyCs.parentTableName() + ";";
		//Log.d(TAG, sqlStatement);
		GenericRawResults<Object[]> rawResults = dao.queryRaw(sqlStatement, new DataType[] { DataType.INTEGER_OBJ });
		for (Object[] resultArray : rawResults) { 
			Integer oldId = (Integer) resultArray[0];
			UUID newId = java.util.UUID.randomUUID();   // Generate UUID to replace the integer
			
			// Update the row in the root table.
			sqlStatement = "UPDATE " + keyCs.parentTableName() + " SET " + keyCs.temporaryName() + "='" + newId + "' " +
					"WHERE " + keyCs.name() + "=" + oldId + " ;";
			Log.d(TAG, sqlStatement);
			dao.executeRaw(sqlStatement);

			// Update the rows in the child tables.
			for(ColumnUpgradeSpec fkCs : foreignKeys) {
				if(fkCs.isNew()) continue;  // Nothing to upgrade if the column is new.
				sqlStatement = "UPDATE " + fkCs.parentTableName() + " SET " + fkCs.temporaryName() + "='" + newId + "' " +
						"WHERE " + fkCs.name() + "=" + oldId + " ;";
				Log.d(TAG, sqlStatement);
				dao.executeRaw(sqlStatement);
			}
		}
	}
}
