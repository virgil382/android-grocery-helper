package com.homecortex.groceryShoppingList.DataModel.Extents;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.StoreItem;
import com.homecortex.groceryShoppingList.DataModel.Entities.StoreItemEdge;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.j256.ormlite.dao.Dao;

public class NaturalShoppingOrderLearningExtent {
	private static NaturalShoppingOrderLearningExtent m_instance;
	public static NaturalShoppingOrderLearningExtent instance() {
		if (m_instance == null) m_instance = new NaturalShoppingOrderLearningExtent();
		return m_instance;
	}

	// The DB connection.
	Dao<StoreItem, UUID> m_classDao;

	public Dao<StoreItem, UUID> classDao() {
		if (m_classDao != null) return m_classDao;
		m_classDao = ApplicationPm.getDatabaseHelper().getStoreItemDao();
		return m_classDao;
	}

	HashMap<UUID, StoreItem> m_cache;

	public void reloadCache() {
		m_cache = new HashMap<>();
        /*
		List<StoreItem> results;
		try {
			results = classDao().queryForAll();
			for(StoreItem object : results) m_cache.put(object.getId(), object);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	}

	public StoreItem getById(UUID id) {
		StoreItem storeItem = m_cache.get(id);
		if(storeItem != null) return(storeItem);
        try {
            storeItem = classDao().queryForId(id);
            if(storeItem != null)
                m_cache.put(id, storeItem);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

		return(storeItem);
	}

	public void clearCache() {
		m_cache.clear();
	}
	
	/**
	 * Determine if a StoreItemEdge with the specified head and tail StoreItems exists.
	 * @param tailStoreItemId The ID of the tail StoreItem.
	 * @param headStoreItemId The ID of the head StoreItem.
	 * @return True if it exists, otherwise false.
	 * @throws SQLException
	 */
	public StoreItemEdge find(UUID tailStoreItemId, UUID headStoreItemId) throws SQLException {
		Dao<StoreItemEdge, UUID> classDao = ApplicationPm.getDatabaseHelper().getStoreItemEdgeDao();
		Map<String, Object> map = new HashMap<>();
		map.put("tailStoreItemId", tailStoreItemId);
		map.put("headStoreItemId", headStoreItemId);
		
		List<StoreItemEdge> list = classDao.queryForFieldValues(map);
		if (list.size() == 0) return null;
		assert(list.size() == 1);
		
		return list.get(0);
	}

	// TODO: Add method for constructing directed graph.
	
	// TODO: Add logic for pruning malformed StoreItemEdges (e.g. with a single node).
	
}
