// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Entities;

import java.sql.SQLException;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Extents.WeightUnitExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.homecortex.groceryShoppingList.Util.ObjectUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "weightUnit")
public class WeightUnit implements IEntity {
	/// ORMLite needs a parameterless constructor.
	public WeightUnit() { }
	
	public WeightUnit(String name, Double grams) throws SQLException {
		this.name = name;
		this.grams = grams;
		
		WeightUnitExtent.instance().create(this);
	}

	@Override
	public void open() { }
	@Override
	public void close() { }
	@Override
	public void delete() { }

	@DatabaseField(generatedId = true)
	private UUID weightUnitId;
	public UUID getId() { return weightUnitId; }

	@DatabaseField
	private String name;
	public String getName() { return this.name; }
	public void setName(String newValue) {
		if(ObjectUtils.equals(newValue, name)) return;
		name = newValue;
		WeightUnitExtent.instance().update(this, new UpdateSet("name"));
	}

    @Override
    public String toString() { return(name); }

	@DatabaseField
	private Double grams;
	public void setGrams(Double value) { this.grams = value; }
	public Double getGrams() { return this.grams; }
}
