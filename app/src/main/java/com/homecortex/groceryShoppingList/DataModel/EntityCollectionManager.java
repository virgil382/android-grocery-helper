// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel;

import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.UpdateSet;
import com.homecortex.groceryShoppingList.DataModel.Extents.EntityService;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntityCollectionQueryResultHandler;

public class EntityCollectionManager <E extends IEntity> extends EntityObserver<E> implements IEntityCollectionQueryResultHandler<E>
{
	private EntityService<E> m_entityService;
	private boolean m_isOpen = false;
	private HashMap<UUID, E> m_childrenById = new HashMap<>();

	public EntityCollectionManager(EntityService<E> entityService) {
		m_entityService = entityService;
	}

	@Override
	public void open() {
		if(m_isOpen) return;
		m_isOpen = true;
		
		m_entityService.addObserver(this);
		m_entityService.getAll(this);
	}

	@Override
	public void close() {
		if(!m_isOpen) return;
		m_isOpen = false;
		
		m_childrenById.clear();
		m_entityService.dropObserver(this);
	}

	@Override
	public void onResult(Collection<E> objects) {
		for(E object : objects) {
			m_childrenById.put(object.getId(), object);
		}
	}

	public Collection<E> getCollection() {
		return(m_childrenById.values());
	}

	@Override
	public void onCreate(E object) {
		m_childrenById.put(object.getId(), object);
	}

	@Override
	public void onUpdate(E object, UpdateSet updates) { }

	@Override
	public void onDelete(E object) {
		m_childrenById.remove(object.getId());
	}
}
