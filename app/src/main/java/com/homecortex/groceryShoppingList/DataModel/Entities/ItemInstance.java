// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Entities;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.ChildEntityCollectionManager;
import com.homecortex.groceryShoppingList.DataModel.EntityAutoLoader;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemInstanceAttributeExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemInstanceExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.ShoppingListExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.StoreExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.VolumeUnitExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.WeightUnitExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IChildEntity;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.homecortex.groceryShoppingList.Util.BooleanResult;
import com.homecortex.groceryShoppingList.Util.ObjectUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * An <code>ItemInstance</code> represents an {@link Item} with specific attributes 
 * (e.g. volume, weight, color, flavor, count) that the user intends to purchase.
 */
@DatabaseTable(tableName = "itemInstance")
public class ItemInstance implements IChildEntity
{
	/// ORMLite needs a parameterless constructor.
	public ItemInstance() { }
	
	public ItemInstance(UUID shoppingListId, Item item) {
		this.shoppingListId = shoppingListId;
		this.itemId = item.getId();

		pickupIndex = null;

		ItemInstanceExtent.instance().create(this);
	}

	@Override
	public void open() {
		//m_childItemInstanceAttributesManager.open();
		//m_itemAutoLoader.open();
		//m_shoppingListAutoLoader.open();
		//m_volumeUnitAutoLoader.open();
		//m_weightUnitAutoLoader.open();
		//m_pickupStoreAutoLoader.open();
	}
	
	@Override
	public void close() {
		m_childItemInstanceAttributesManager.close();
		m_itemAutoLoader.close();
		m_shoppingListAutoLoader.close();
		m_volumeUnitAutoLoader.close();
		m_weightUnitAutoLoader.close();
		m_pickupStoreAutoLoader.close();
	}

	public void setDefaults(Item item) {
		setHasCount(item.getDefaultHasCountValue());
		setCount(item.getDefaultCountValue());
		setHasVolume(item.getDefaultHasVolumeValue());
		setVolumeUnitId(item.getDefaultVolumeUnitValueId());
		setVolumeMagnitude(item.getDefaultVolumeMagnitudeValue());
		setHasWeight(item.getDefaultHasWeightValue());
		setWeightUnitId(item.getDefaultWeightUnitValueId());
		setWeightMagnitude(item.getDefaultWeightMagnitudeValue());

		for (ItemAttribute itemAttribute : item.getItemAttributes()) {
			if (itemAttribute.isDefaultValue) {
				new ItemInstanceAttribute(itemAttribute.getId(), getId());
			}
		}
	}

	@Override
	public void delete() {
		ItemInstanceExtent.instance().delete(this);
	}

	@Override
	public void setParent(String foreignKeyName, IEntity parent) {
		//if(foreignKeyName.equals("itemId")) {
		//	item = (Item) parent;
		//	return;
		//}
		//setShoppingList((ShoppingList) parent);
	}

	@DatabaseField(generatedId = true)
	public UUID itemInstanceId;
	@Override
	public UUID getId() { return itemInstanceId; }
	public void setId(UUID newValue) {
		if(ObjectUtils.equals(newValue, itemInstanceId)) return;
		itemInstanceId = newValue;
		ItemInstanceExtent.instance().update(this, new UpdateSet("itemInstanceId"));
	}

	@DatabaseField(canBeNull = false)
	private UUID itemId;
	public UUID getItemId() { return(itemId); }
	private EntityAutoLoader<ItemInstance, Item> m_itemAutoLoader =
			new EntityAutoLoader<>(this, "itemId", ItemInstanceExtent.instance(), ItemExtent.instance());
	public Item getItem() {
		m_itemAutoLoader.open();
		return(m_itemAutoLoader.getEntity());
	}

	// The ShoppingList to which this ItemInstance belongs.
	@DatabaseField(canBeNull = false, index = true)
	private UUID shoppingListId;
	private EntityAutoLoader<ItemInstance, ShoppingList> m_shoppingListAutoLoader =
			new EntityAutoLoader<>(this, "shoppingListId", ItemInstanceExtent.instance(), ShoppingListExtent.instance());
	public ShoppingList getShoppingList() {
        m_shoppingListAutoLoader.open();
        return(m_shoppingListAutoLoader.getEntity());
    }
	
	@DatabaseField
	public boolean hasCount;
	public boolean getHasCount() { return hasCount; }
	public void setHasCount(boolean newValue) {
		if(newValue == hasCount) return;
		hasCount = newValue;
		ItemInstanceExtent.instance().update(this, new UpdateSet("hasCount"));
	}

	@DatabaseField
	public int count;
	public int getCount() { return count; }
	public void setCount(int newValue) { 
		if(newValue == count) return;
		count = newValue;
		ItemInstanceExtent.instance().update(this, new UpdateSet("count"));
	}

	@DatabaseField
	public boolean hasVolume;
	public boolean getHasVolume() { return hasVolume; }
	public void setHasVolume(boolean newValue) {
		if(newValue == hasVolume) return;
		hasVolume = newValue;
		ItemInstanceExtent.instance().update(this, new UpdateSet("hasVolume"));
	}

	@DatabaseField(canBeNull = true)
	private UUID volumeUnitId;
	public UUID getVolumeUnitId() { return(volumeUnitId); }
	public void setVolumeUnitId(UUID newValue) {
		if(ObjectUtils.equals(newValue, volumeUnitId)) return;
		volumeUnitId = newValue;
		ItemInstanceExtent.instance().update(this, new UpdateSet("volumeUnitId"));
	}
	private EntityAutoLoader<ItemInstance, VolumeUnit> m_volumeUnitAutoLoader =
			new EntityAutoLoader<>(this, "volumeUnitId", ItemInstanceExtent.instance(), VolumeUnitExtent.instance());
	public VolumeUnit getVolumeUnit() {
        m_volumeUnitAutoLoader.open();
        return(m_volumeUnitAutoLoader.getEntity());
    }
	
	@DatabaseField
	public float volumeMagnitude;
	public float getVolumeMagnitude() { return volumeMagnitude; }
	public void setVolumeMagnitude(float newValue) {
		if(newValue == volumeMagnitude) return;
		volumeMagnitude = newValue;
		ItemInstanceExtent.instance().update(this, new UpdateSet("volumeMagnitude"));
	}
	
	@DatabaseField
	public boolean hasWeight;
	public boolean getHasWeight() { return hasWeight; }
	public void setHasWeight(boolean newValue) {
		if(newValue == hasWeight) return;
		hasWeight = newValue;
		ItemInstanceExtent.instance().update(this, new UpdateSet("hasWeight"));
	}

	@DatabaseField(canBeNull = true)
	UUID weightUnitId;
	public UUID getWeightUnitId() { return(weightUnitId); }
	public void setWeightUnitId(UUID newValue) {
		if(ObjectUtils.equals(newValue, weightUnitId)) return;
		weightUnitId = newValue;
		ItemInstanceExtent.instance().update(this, new UpdateSet("weightUnitId"));
	}
	private EntityAutoLoader<ItemInstance, WeightUnit> m_weightUnitAutoLoader =
			new EntityAutoLoader<>(this, "weightUnitId", ItemInstanceExtent.instance(), WeightUnitExtent.instance());
	public WeightUnit getWeightUnit() {
        m_weightUnitAutoLoader.open();
        return(m_weightUnitAutoLoader.getEntity());
    }

	@DatabaseField
	public float weightMagnitude;
	public float getWeightMagnitude() { return weightMagnitude; }
	public void setWeightMagnitude(float newValue) {
		if(newValue == weightMagnitude) return;
		weightMagnitude = newValue;
		ItemInstanceExtent.instance().update(this, new UpdateSet("weightMagnitude"));
	}

	/**
	 * Propose default values in the Item associated with this ItemInstance.
	 * @throws SQLException
	 */
	public void proposeDefaultValuesInCorrespondingItem() throws SQLException {
		if (getItem() == null) return;
		getItem().proposeDefaultValues(this);
	}
	
	///////////////// ItemInstanceAttributes
	private ChildEntityCollectionManager<ItemInstanceAttribute> m_childItemInstanceAttributesManager =
			new ChildEntityCollectionManager<>(this, "itemInstanceId", ItemInstanceAttributeExtent.instance());
	public Collection<ItemInstanceAttribute> getItemInstanceAttributes() {
        m_childItemInstanceAttributesManager.open();
        return(m_childItemInstanceAttributesManager.getCollection());
    }
//	public ItemInstanceAttribute getItemInstanceAttributeFor(ItemAttribute itemAttribute) {
//		for (ItemInstanceAttribute itemInstanceAttribute : getItemInstanceAttributes()) {
//			if (itemInstanceAttribute.getItemAttribute() == itemAttribute) return itemInstanceAttribute;
//		}
//		return null;
//	}
//	public boolean hasItemAttribute(ItemAttribute itemAttribute) throws SQLException {
//		return getItemInstanceAttributeFor(itemAttribute) != null;
//	}

	// If set, specifies the index of the order in which this ItemInstance was picked up relative to other
	// ItemInstances within a ShoppingList.
	@DatabaseField(canBeNull = true)
	public Integer pickupIndex;
	public Integer getPickupIndex() { return(pickupIndex); }
	public void setPickupIndex(Integer newValue) {
		if(ObjectUtils.equals(newValue, pickupIndex)) return;
		pickupIndex = newValue;
		ItemInstanceExtent.instance().update(this, new UpdateSet("pickupIndex"));
	}
	public boolean isPickedUp() { return pickupIndex != null; }
	
	/**
	 * Set the picked-up state of the ItemInstance.
	 * 
	 * @param newPickedUpState The new picked-up state.
	 * @return True if the new state could be set, otherwise false.  The state cannot be set to "picked-up"
	 * if the ShoppingList that owns this ItemInstance does not have a Store set.
	 */
	public boolean isPickedUp(boolean newPickedUpState) {
		if (newPickedUpState == isPickedUp()) return true;

		if(getShoppingList() == null) return(false); // ShoppingList not loaded yet
		
		if (newPickedUpState) {
			if (getShoppingList().getStore() == null) return false;
			getShoppingList().pickUpItemInstance(this);
		} else {
			getShoppingList().dropItemInstance(this);
		}
		return true;
	}
	
	// If set, specifies the Store at which this ItemInstance was picked up.
	@DatabaseField(canBeNull = true)
	UUID pickupStoreId;
	public UUID getPickupStoreId() { return(pickupStoreId); }
	public void setPickupStoreId(UUID newValue) {
		if(ObjectUtils.equals(newValue, pickupStoreId)) return;
		pickupStoreId = newValue;
		ItemInstanceExtent.instance().update(this, new UpdateSet("pickupStoreId"));
	}
	private EntityAutoLoader<ItemInstance, Store> m_pickupStoreAutoLoader =
			new EntityAutoLoader<>(this, "pickupStoreId", ItemInstanceExtent.instance(), StoreExtent.instance());
	public Store getPickupStore() {
        m_pickupStoreAutoLoader.open();
        return(m_pickupStoreAutoLoader.getEntity());
    }
	
	/**
	 * Represent an <code>ItemInstance</code> as a human-readable String.
	 * @return A human-readable representation of the <code>ItemInstance</code>.
	 */
	@Override
	public String toString() {
		String result;
		if (getItem() == null) return("[unavailable]");
		result = getItem().getName();

		// Append the count if present.
		if (getHasCount()) {
			result += " x" + getCount();
		}

		String otherAttributes = "";
		
		// Append the volume if present.
		if (getHasVolume()) {
			if (getVolumeUnit() != null) {
				if (otherAttributes.length() > 0) otherAttributes += ", ";
				otherAttributes += new DecimalFormat("#.##").format(getVolumeMagnitude());
				otherAttributes += getVolumeUnit().getName();
			}
		}
		
		// Append the weight if present.
		if (getHasWeight()) {
			if (getWeightUnit() != null) {
				if (otherAttributes.length() > 0) otherAttributes += ", ";
				otherAttributes += new DecimalFormat("#.##").format(getWeightMagnitude());
				otherAttributes += getWeightUnit().getName();
			}
		}
		
		if (otherAttributes.length() > 0) {
			result += " (" + otherAttributes + ")";
		}

		return result;
	}
	
	public String getAttributesString() {
		String otherAttributes = "";
		
		// Append the names of the ItemInstanceAttributes.
		Collection<ItemInstanceAttribute> itemInstanceAttributes;
		itemInstanceAttributes = getItemInstanceAttributes();
		for(ItemInstanceAttribute itemInstanceAttribute : itemInstanceAttributes) {
            assert(itemInstanceAttribute.getItemAttribute() != null);
			if (otherAttributes.length() > 0) otherAttributes += ", ";
			otherAttributes += itemInstanceAttribute.getItemAttribute().getName();
		}
		
		if (otherAttributes.length() > 0)
			return otherAttributes;
		
		return "";
	}
	
	public Double getStoreItemPrice(BooleanResult isKnownPrice) {
		isKnownPrice.set(true);

		ShoppingList shoppingList = getShoppingList();
		if(shoppingList == null) return(null);

		// Get the current store.  If not set, then no price is available.
		UUID storeId = shoppingList.getStoreId();
		if (storeId == null) return null;

		// Get the store item.  If none is found, then return the lowest price
		// at any store.
		if(getItem() == null) return(null);
		StoreItem storeItem = getItem().getStoreItem(storeId);
		if (storeItem == null) {
			isKnownPrice.set(false);
			return getItem().getLowestPrice();
		}

		// Get the store item price.  If not set, then return the lowest price
		// at any store. 
		Double storeItemPrice = storeItem.getUnitPrice();
		if (storeItemPrice == null) {
			isKnownPrice.set(false);
			return (getItem().getLowestPrice());
		}

		// Else return the store item price.
		return storeItemPrice;
	}
	
	public void setStoreItemPrice(Double value) {
		// Get the current store.  If not set, then we can't set the price.
		UUID storeId = getShoppingList().getStoreId();
		if (storeId == null) return;

		// Get the store item.  If none is found, then we can't set the price.
		StoreItem storeItem = getItem().getStoreItem(storeId);
		if (storeItem == null) storeItem = new StoreItem(storeId, itemId);

		storeItem.setUnitPrice(value);
	}
}
