// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Entities;

import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.EntityAutoLoader;
import com.homecortex.groceryShoppingList.DataModel.Extents.StoreItemExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.StoreItemEdgeExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IChildEntity;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.homecortex.groceryShoppingList.Util.ObjectUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * A StoreItemEdge represents a weighted directed edge in a directed graph whose vertices are StoreItems.
 * The graph may be used to represent the relative order between StoreItems.  The weight represents the
 * age of the information - smaller weights represent older and less certain order information.
 */
@DatabaseTable(tableName = "storeItemEdge")
public class StoreItemEdge implements IChildEntity {
	
	/// ORMLite needs a parameterless constructor.
	public StoreItemEdge() { }

	public StoreItemEdge(UUID tailStoreItemId, UUID headStoreItemId) {
		this.tailStoreItemId = tailStoreItemId;
		this.headStoreItemId = headStoreItemId;
		StoreItemEdgeExtent.instance().create(this);
	}

	private boolean m_isOpen = false;
	@Override
	public void open() {
		if (m_isOpen) return;
		m_isOpen = true;
//		m_tailStoreItemAutoLoader.open();  // TODO: Remove after debugging
//		m_headStoreItemAutoLoader.open();  // TODO: Remove after debugging
	}
	@Override
	public void close() {
		if (!m_isOpen) return;
		m_isOpen = false;
//		m_tailStoreItemAutoLoader.close();  // TODO: Remove after debugging
//		m_headStoreItemAutoLoader.close();  // TODO: Remove after debugging
	}
	
	public void delete() { StoreItemEdgeExtent.instance().delete(this); }
	
	
	@DatabaseField(generatedId = true)
	private UUID storeItemEdgeId;
	public UUID getId() { return storeItemEdgeId; }
	public void setId(UUID newValue) {
		if(ObjectUtils.equals(newValue, storeItemEdgeId)) return;

		storeItemEdgeId = newValue;
		StoreItemEdgeExtent.instance().update(this, new UpdateSet("storeItemEdgeId"));
	}

	/// The StoreItem at the tail of the directed edge (i.e. the source StoreItem).
	@DatabaseField(canBeNull = false, index = true)
	private UUID tailStoreItemId;
	public UUID getTailStoreItemId() { return(tailStoreItemId); }

	// TODO: Remove after debugging.
	private EntityAutoLoader<StoreItemEdge, StoreItem> m_tailStoreItemAutoLoader =
			new EntityAutoLoader<>(this, "tailStoreItemId", StoreItemEdgeExtent.instance(), StoreItemExtent.instance());
	public StoreItem getTailStoreItem() { return(m_tailStoreItemAutoLoader.getEntity()); }

	/// The StoreItem at the head of the directed edge (i.e. the destination StoreItem).
	@DatabaseField(canBeNull = false, index = true)
	UUID headStoreItemId;
	public UUID getHeadStoreItemId() { return(headStoreItemId); }

	// TODO: Remove after debugging.
	private EntityAutoLoader<StoreItemEdge, StoreItem> m_headStoreItemAutoLoader =
			new EntityAutoLoader<>(this, "headStoreItemId", StoreItemEdgeExtent.instance(), StoreItemExtent.instance());
	public StoreItem getHeadStoreItem() { return(m_headStoreItemAutoLoader.getEntity()); }

	@Override
	public void setParent(String foreignKeyName, IEntity parent) { }

	@Override
	public String toString() {
		String tailName;
		if (getTailStoreItem() == null)
			tailName = "[null]"; 
		else
			tailName = getTailStoreItem().toString();
		
		String headName;
		if (getHeadStoreItem() == null)
			headName = "[null]"; 
		else
			headName = getHeadStoreItem().toString();

		return tailName + " -> " + headName;
	}
}
