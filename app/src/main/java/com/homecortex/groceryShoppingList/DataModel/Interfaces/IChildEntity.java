package com.homecortex.groceryShoppingList.DataModel.Interfaces;

public interface IChildEntity extends IEntity {
	void setParent(String foreignKeyName, IEntity parent);
}
