// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Extents;

import java.sql.SQLException;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.StoreItemEdge;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.j256.ormlite.dao.Dao;

public class StoreItemEdgeExtent extends EntityService<StoreItemEdge> {
	private static StoreItemEdgeExtent m_instance;
	public static StoreItemEdgeExtent instance() {
		if (m_instance == null) m_instance = new StoreItemEdgeExtent();
		return m_instance;
	}
	
	// The DB connection.
	Dao<StoreItemEdge, UUID> m_classDao;
	@Override
	public Dao<StoreItemEdge, UUID> classDao() {
		if (m_classDao != null) return m_classDao;
		m_classDao = ApplicationPm.getDatabaseHelper().getStoreItemEdgeDao();
		return m_classDao;
	}

	/*
	public List<StoreItemEdge> getByTailStoreItem(StoreItem storeItem) throws SQLException {
		List<StoreItemEdge> results = classDao().queryForEq("tailStoreItemId", storeItem.getId());
		return results;
	}

	public List<StoreItemEdge> getByHeadStoreItem(StoreItem storeItem) throws SQLException {
		List<StoreItemEdge> results = classDao().queryForEq("headStoreItemId", storeItem.getId());
		return results;
	}
	
	public void deleteByStoreItemId(StoreItem storeItem) {
		Map<String, Object> map1 = new HashMap<String, Object>();
		Map<String, Object> map2 = new HashMap<String, Object>();
		map1.put("tailStoreItemId", storeItem.getId());
		map2.put("headStoreItemId", storeItem.getId());
		List<StoreItemEdge> storeItemEdges1;
		List<StoreItemEdge> storeItemEdges2;
		try {
			storeItemEdges1 = classDao().queryForFieldValues(map1);
			storeItemEdges2 = classDao().queryForFieldValues(map2);
			for(StoreItemEdge doomedStoreItemEdge : storeItemEdges1) {
				delete(doomedStoreItemEdge);
			}
			for(StoreItemEdge doomedStoreItemEdge : storeItemEdges2) {
				delete(doomedStoreItemEdge);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}
	*/

	@Override
	public void delete(StoreItemEdge doomedObject) {
		doomedObject.close();
		notifyDelete(doomedObject);
		try {
			classDao().delete(doomedObject);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
