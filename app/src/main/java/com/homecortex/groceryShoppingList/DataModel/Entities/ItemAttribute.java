// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Entities;

import java.sql.SQLException;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.DefaultValueTracker;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemAttributeExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IChildEntity;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.homecortex.groceryShoppingList.Util.ObjectUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * An <code>ItemAttribute</code> represents a named attribute of an {@link Item}.  An {@link Item}
 * may contain any number of <code>ItemAttribute</code>s.  An <code>ItemAttribute</code> contains
 * a <code>Boolean</code> that specifies if a new instance of ItemInstances should add
 * corresponding {@link ItemInstanceAttribute}s to itself by default.  (The user may explicitly
 * add/remove other corresponding {@link ItemInstanceAttribute}s to/from the ItemInstances).
 */
@DatabaseTable(tableName = "itemAttribute")
public class ItemAttribute implements IChildEntity
{
	/// ORMLite needs a parameterless constructor.
	public ItemAttribute() { }

	public ItemAttribute(String name, UUID itemId, boolean isDefaultValue, int isDefaultPopularity) {
		this.name = name;
		this.itemId = itemId;
		this.isDefaultValue = isDefaultValue;
		this.isDefaultPopularity = isDefaultPopularity;
		
		ItemAttributeExtent.instance().create(this);
	}

	private boolean m_isOpen = false;
	@Override
	public void open() {
		if (m_isOpen) return;
		m_isOpen = true;
		//m_childItemInstanceAttributesManager.open();
	}

	@Override
	public void close() {
		if (!m_isOpen) return;
		m_isOpen = false;
		//m_childItemInstanceAttributesManager.close();
	}

	@Override
	public void delete() {
		ItemAttributeExtent.instance().delete(this);
	}
	
	@Override
	public void setParent(String foreignKeyName, IEntity parent) { }
	
	@DatabaseField(generatedId = true)
	private UUID itemAttributeId;
	public UUID getId() { return itemAttributeId; }
	public void setId(UUID newvalue) { itemId = newvalue; }

	@DatabaseField(canBeNull = false, index = true)
	private UUID itemId;
	public UUID getItemId() { return(itemId); }

	@DatabaseField
	private String name;
	public String getName() { return name; }
	public void setName(String newValue) {
		if(ObjectUtils.equals(newValue, name)) return;

		name = newValue;
		ItemAttributeExtent.instance().update(this, new UpdateSet("name"));
	}

	// TODO: Remove after debugging.
	@Override
	public String toString() {
		return name;
	}

	// Fields used to track the value of isDefault which determines if Item.constructItemInstance()
	// adds this ItemAttribute to new ItemInstances that it constructs.
	@DatabaseField
	public Boolean isDefaultValue;

	@DatabaseField
	public Integer isDefaultPopularity;
	protected DefaultValueTracker<Boolean> m_isDefaultTracker = new DefaultValueTracker<>(this, "isDefaultValue", "isDefaultPopularity", 3);
	public void proposeIsDefaultValue(boolean isDefault) throws SQLException {
		Boolean previousValue = isDefaultValue;
		Integer previousPopularity = isDefaultPopularity;
		
		m_isDefaultTracker.proposeDefaultValue(isDefault);

		if(!ObjectUtils.equals(isDefaultValue, previousValue)) {
			ItemAttributeExtent.instance().update(this, new UpdateSet("isDefaultValue"));
		}
		if(!ObjectUtils.equals(isDefaultPopularity, previousPopularity)) {
			ItemAttributeExtent.instance().update(this, new UpdateSet("isDefaultPopularity"));
		}
	}
}
