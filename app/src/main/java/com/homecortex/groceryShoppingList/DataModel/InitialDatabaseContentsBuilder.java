// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel;

import java.sql.SQLException;

import com.homecortex.groceryShoppingList.DataModel.Entities.ShoppingList;
import com.homecortex.groceryShoppingList.DataModel.Entities.Store;
import com.homecortex.groceryShoppingList.DataModel.Entities.VolumeUnit;
import com.homecortex.groceryShoppingList.DataModel.Entities.WeightUnit;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;

public class InitialDatabaseContentsBuilder {
	public void Build() throws SQLException
	{
		if (ApplicationPm.getDatabaseHelper() == null) return;
		// Create a few units
		WeightUnit lb = new WeightUnit("lb", 453.592);
		new WeightUnit("oz", 28.35);
		
		VolumeUnit floz = new VolumeUnit("fl oz", 29.57353);
		new VolumeUnit("cup", 236.588237);
		new VolumeUnit("quart", 1136.5225);
		new VolumeUnit("pint", 473.176473);
		VolumeUnit gal = new VolumeUnit("gal", 3785.411784);

		// Create a few items
		/*
		Item itemApples = new Item("apples", false, 3, 0, 1, false, 3, null, 1, 0, 1, true, 3, lb, 1, 1, 1);
		new Item("bananas", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("blueberries", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("bread", true, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("butter", true, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("canned peas", true, 3, 3, 1, false, 3, floz, 1, 14, 1, false, 3, null, 1, 0, 1);
		new Item("cereal", true, 3, 1, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("cheerios", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("cheese", false, 3, 0, 1, false, 3, null, 1, 0, 1, true, 3, lb, 3, 2, 3);
		new Item("chicken", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		Item itemCoffee = new Item("coffee", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("corn flakes", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("ear pluggs", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("eggs", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("flour", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("flowers", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("hummus", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("ice cream", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		Item itemOil = new Item("oil", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("olives", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("oranges", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("pate", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		Item itemPie = new Item("pie", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("raisins", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("raspberries", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("sponge", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("stamps", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("strawberries", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("toilet paper", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("tomatoes", false, 3, 0, 1, false, 3, null, 1, 0, 1, true, 3, null, 1, 0, 1);
		new Item("vinegar", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		Item itemWater = new Item("water", true, 3, 4, 1, false, 3, gal, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("waffles", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		new Item("wine", false, 3, 0, 1, false, 3, null, 1, 0, 1, false, 3, null, 1, 0, 1);
		*/
		
		// Create ItemAttributes.
		/*
		new ItemAttribute("cherry", itemPie, true, 3);
		new ItemAttribute("canola", itemOil, true, 3);
		*/
		
		// Create a shopping list
		ShoppingList shoppingList = new ShoppingList("My Grocery List");

		// Add a few ItemInstances to the shopping list.
		/*
		new ItemInstance(shoppingList, itemApples);
		new ItemInstance(shoppingList, itemPie);
		new ItemInstance(shoppingList, itemOil);
		new ItemInstance(shoppingList, itemCoffee);
		new ItemInstance(shoppingList, itemWater);
		*/

		new Store("[My Store]");
	}

	//public static void Destroy() throws SQLException
	//{
	//	// Delete all the test ShoppingLists.
	//	Dao<ShoppingList, Integer> shoppingListDao = ApplicationPm.getDatabaseHelper().getShoppingListDao();
	//	DeleteBuilder<ShoppingList, Integer> shoppingListDeleteBuilder = shoppingListDao.deleteBuilder();
	//	shoppingListDeleteBuilder.where().eq("isTest", true);
	//	shoppingListDeleteBuilder.delete();
	//}
}
