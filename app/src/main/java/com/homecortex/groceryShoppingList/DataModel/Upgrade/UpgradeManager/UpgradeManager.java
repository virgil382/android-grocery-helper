// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Upgrade.UpgradeManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.dao.Dao;

public class UpgradeManager {
	private Dao<Bogus, Void> m_dao;
	private ArrayList<TableUpgradeSpec> m_tableUpgradeSpecs = new ArrayList<>();
	
	public UpgradeManager(Dao<Bogus, Void> dao) {
		m_dao = dao;
	}
	
	public UpgradeManager add(TableUpgradeSpec ts) {
		m_tableUpgradeSpecs.add(ts);
		return(this);
	}

	public void upgrade() throws SQLException {
		fixNewTypes();
		
		for (TableUpgradeSpec ts : m_tableUpgradeSpecs) {
			ts.createTemporaryColumns(m_dao);
		}
		
		for (TableUpgradeSpec ts : m_tableUpgradeSpecs) {
			ts.upgradeData(this, m_dao);
		}

		for (TableUpgradeSpec ts : m_tableUpgradeSpecs) {
			ts.createTemporaryTable(m_dao);
			ts.copyDataToTemporaryTable(m_dao);
			ts.dropOriginalTable(m_dao);
			ts.renameTableAndCreateIndexes(m_dao);
		}
	}
	
	private void fixNewTypes() {
		for (TableUpgradeSpec ts : m_tableUpgradeSpecs) {
			ColumnUpgradeSpec primaryKey = ts.findPrimaryKey();
			if (primaryKey == null) continue;
			if (primaryKey.typeMapper() == null) continue;

			// Get the new type of the primary key and fix it.
			String newType = primaryKey.typeMapper().newType();
			primaryKey.newType(newType);

			// Fix the new type of each foreign key.
			ArrayList<ColumnUpgradeSpec> foreignKeys = new ArrayList<>();
			findForeignKeys(primaryKey, foreignKeys);
			for(ColumnUpgradeSpec foreignKey : foreignKeys) {
				foreignKey.newType(newType);
			}
		}
	}
	
	/**
	 * Find the ColumnUpgradeSpecs that represent a foreign keys for the specified ColumnUpgradeSpec.
	 * @param keyCs The ColumnUpgradeSpec that specifies the key.
	 * @param foreignKeysCs A List of ColumnUpgradeSpecs that specify the foreign keys.
	 */
	public void findForeignKeys(ColumnUpgradeSpec keyCs, List<ColumnUpgradeSpec> foreignKeysCs) {
		for(TableUpgradeSpec ts : m_tableUpgradeSpecs) {
			ts.findForeignKeys(keyCs, foreignKeysCs);
		}
	}
}
