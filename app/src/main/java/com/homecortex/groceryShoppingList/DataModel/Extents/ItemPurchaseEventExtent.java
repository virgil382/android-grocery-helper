package com.homecortex.groceryShoppingList.DataModel.Extents;

import com.homecortex.groceryShoppingList.DataModel.Entities.ItemPurchaseEvent;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.field.DataType;

import java.sql.SQLException;
import java.util.Date;
import java.util.UUID;

public class ItemPurchaseEventExtent extends EntityService<ItemPurchaseEvent> {
    private static ItemPurchaseEventExtent m_instance;
    public static ItemPurchaseEventExtent instance() {
        if (m_instance == null) m_instance = new ItemPurchaseEventExtent();
        return m_instance;
    }

    // The DB connection.
    Dao<ItemPurchaseEvent, UUID> m_classDao;

    @Override
    public Dao<ItemPurchaseEvent, UUID> classDao() {
        if (m_classDao != null) return m_classDao;
        m_classDao = ApplicationPm.getDatabaseHelper().getItemPurchaseEventDao();
        return m_classDao;
    }

    @Override
    public void delete(ItemPurchaseEvent doomedObject) {
        try {
            classDao().delete(doomedObject);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return;
        }
        notifyDelete(doomedObject);
        doomedObject.close();
    }

    public int getCount(Date afterDate, UUID itemId) {
        int count = 0;
        try {
            String sqlStatement = "SELECT COUNT(itemId) FROM itemPurchaseEvent WHERE "
                + "itemId='" + itemId + "' AND purchaseTime > " + afterDate.getTime() + ";";
            GenericRawResults<Object[]> rawResults = classDao().queryRaw(sqlStatement, new DataType[]{DataType.INTEGER});
            for (Object[] resultArray : rawResults) {
                count = (int) resultArray[0];
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO: This could be optimized by calculating the count for all Items and caching the
        // results with an SQL statement like:
        // SELECT itemId, COUNT(*) FROM itemPurchaseEvent WHERE itemPurchaseEventId >= afterDate

        return(count);
    }
}
