// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Entities;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import android.os.Looper;

import com.homecortex.groceryShoppingList.DataModel.ChildEntityCollectionManager;
import com.homecortex.groceryShoppingList.DataModel.EntityAutoLoader;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemInstanceExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.ShoppingListExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.StoreExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.StoreItemExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.homecortex.groceryShoppingList.DataModel.EntityObserver;
import com.homecortex.groceryShoppingList.PresentationModel.INarrowCustomArrayAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList.SaveItemsOrderTask;
import com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList.SaveItemsOrderTask.CancelledException;
import com.homecortex.groceryShoppingList.Util.BooleanResult;
import com.homecortex.groceryShoppingList.Util.ObjectUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * A ShoppingList contains an ordered set of ItemInstances.  It has a method for reordering the
 * ItemInstances based on the "natural shopping order" of the corresponding Items at a Store.
 */
@DatabaseTable(tableName = "shoppingList")
public class ShoppingList implements IEntity
{
	/// ORMLite needs a parameterless constructor.
	public ShoppingList() { 
	}

	public ShoppingList(String name) {
		this.name = name;
		ShoppingListExtent.instance().create(this);
	}

	private boolean m_isOpen = false;
	@Override
	public void open() {
		if (m_isOpen) return;
		m_isOpen = true;
		//m_childItemInstancesManager.open();
		m_storeOrderingOracleTracker.open();
		//m_currentStoreAutoLoader.open();
	}
	@Override
	public void close() {
		if (!m_isOpen) return;
		m_isOpen = false;
		m_childItemInstancesManager.close();
		m_storeOrderingOracleTracker.close();
		m_currentStoreAutoLoader.close();
	}
	
	public void delete() {
		ShoppingListExtent.instance().delete(this);
	}
	
	@DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
	private UUID shoppingListId;
	public UUID getId() { return shoppingListId; }
	public void setId(UUID newValue) {
		if(ObjectUtils.equals(newValue, shoppingListId)) return;

		shoppingListId = newValue;
		ShoppingListExtent.instance().update(this, new UpdateSet("shoppingListId"));
	}

	@DatabaseField
	private String name;
	public String getName() { return name; }
	public void setName(String newValue) {
		if(ObjectUtils.equals(newValue, name)) return;

		name = newValue;
		ShoppingListExtent.instance().update(this, new UpdateSet("name"));
	}
	@Override
	public String toString() {
		return getName();
	}

	@DatabaseField
	private boolean isAutoSelectStore;
	public boolean getIsAutoSelectStore() {
		return isAutoSelectStore;
	}
	public void setIsAutoSelectStore(boolean newValue) throws SQLException {
		if(isAutoSelectStore == newValue) return;
		
		isAutoSelectStore = newValue;
		ShoppingListExtent.instance().update(this, new UpdateSet("isAutoSelectStore"));
	}

	@DatabaseField(canBeNull = true)
	private UUID storeId;
	
	private EntityAutoLoader<ShoppingList, Store> m_currentStoreAutoLoader =
			new EntityAutoLoader<>(this, "storeId", ShoppingListExtent.instance(), StoreExtent.instance());
	public UUID getStoreId() { return(storeId); }
	public void setStoreId(UUID newValue) {
		if(ObjectUtils.equals(storeId, newValue)) return;
		storeId = newValue;
		ShoppingListExtent.instance().update(ShoppingList.this, new UpdateSet("storeId"));
        m_isItemInstanceSortingNeeded = true;
	}
	public Store getStore() {
        m_currentStoreAutoLoader.open();
        return m_currentStoreAutoLoader.getEntity();
    }

	// State variable used to determine if a (re)sorting of ItemInstances is needed. 
	private boolean m_isItemInstanceSortingNeeded = false;

	///////////////// ItemInstances
	private ChildEntityCollectionManager<ItemInstance> m_childItemInstancesManager =
			new ChildEntityCollectionManager<ItemInstance>(this, "shoppingListId", ItemInstanceExtent.instance())
			{
				@Override
				public void onResult(Collection<ItemInstance> objects) {
					super.onResult(objects);

					if (m_orderedItemInstances != null) {
						for(ItemInstance object : objects) {
							m_orderedItemInstances.addFromAnyThread(object, false);
						}
					}
				}

				@Override
				public void onCreate(ItemInstance object) {
					super.onCreate(object);

					if (m_orderedItemInstances != null) {
						m_orderedItemInstances.addFromAnyThread(object, false);
					}
					if (storeId != null) m_isItemInstanceSortingNeeded = true;
				}

				// If the pickupIndex changed, then a (re)sorting is needed.
				@Override
				public void onUpdate(ItemInstance object, UpdateSet updates) {
					super.onUpdate(object, updates);

					if(!isManagedChild(object)) return;
					if(updates.contains("pickupIndex") && storeId != null) m_isItemInstanceSortingNeeded = true;
				}

				@Override
				public void onDelete(ItemInstance object) {
					super.onDelete(object);
					if (m_orderedItemInstances != null) {
						m_orderedItemInstances.removeFromAnyThread(object);
					}
				}
			};
	public Collection<ItemInstance> getItemInstances() {
		m_childItemInstancesManager.open();
		return m_childItemInstancesManager.getCollection();
	}

	// Tracks the Store.  If its sortOracleVersion changed, then a (re)sorting is needed.
	int m_lastSortOracleVersionUsed = -1;
	private EntityObserver<Store> m_storeOrderingOracleTracker = new EntityObserver<Store>() {
		@Override
		public void open() { StoreExtent.instance().addObserver(this); }

		@Override
		public void close() { StoreExtent.instance().dropObserver(m_storeOrderingOracleTracker); }
		
		@Override
		public void onUpdate(Store object, UpdateSet updates) {
			if(!object.getId().equals(storeId)) return;
			if(object.getSortOracleVersion() == m_lastSortOracleVersionUsed) return;
			m_lastSortOracleVersionUsed = object.getSortOracleVersion();
			m_isItemInstanceSortingNeeded = true;
		}
	};
	
	/**
	 * Comparator used to sort ItemInstances.  It places picked up items first based on their pickup index,
	 * and places items not picked up (i.e. with null pickup indexes) last but sorts them by consulting a
	 * HashMap that maps Items to their respective sort indexes (according to the Store's "natural
	 * shopping order"), if the Store is available.
	 */
	class ItemInstanceComparator implements Comparator<ItemInstance>
	{
		// HashMap that maps Items to sort indexes.  The Items were sorted by the user's "natural shopping
		// order" at a Store.
		HashMap<UUID,Integer> m_naturalShoppingOrderIndexes;

		/**
		 * Construct an ItemInstanceComparator.
		 * @param store The Store to use to get the HashMap.
		 */
		public ItemInstanceComparator(Store store) {
			if (store != null)
				m_naturalShoppingOrderIndexes = store.getSortOracle();
		}
		
		@Override
		public int compare(ItemInstance object1, ItemInstance object2) 
		{
			Integer object1PickupIndex = object1.getPickupIndex();
			Integer object2PickupIndex = object2.getPickupIndex();

			// Sort them by comparing the pickup indexes if they are available.
			if (object1PickupIndex != null && object2PickupIndex != null)
				return object1PickupIndex.compareTo(object2PickupIndex);

			// If object1 was picked up, but object2 was not, then object1 comes first. 
			if (object1PickupIndex != null)
				return -1;
			
			// If object1 was not picked up, but object2 was, then object2 comes first. 
			if (object2PickupIndex != null)
				return 1;
			
			// If no HashMap is available, then use their names to compare them.  This should order
			// them alphabetically.
			if (m_naturalShoppingOrderIndexes == null)
				return object1.toString().compareTo(object2.toString());

			// Determine their order with the help of the HashMap that maps Items to their respective
			// sort index.
			Integer index1 = m_naturalShoppingOrderIndexes.get(object1.getItem().getId());
			Integer index2 = m_naturalShoppingOrderIndexes.get(object2.getItem().getId());
			
			// If the Items are not in the HashMap (because the Store does not know about them),
			// then use their IDs to compare them.
			if (index1 == null && index2 == null)
				return object1.getId().compareTo(object2.getId());

			// If the Store does not know about object1, then it comes last.
			if (index1 == null)
				return 1;
			
			// If the Store does not know about object2, then it comes last.
			if (index2 == null)
				return -1;

			// If the index of object1 is smaller than the index of object2, then object1 comes first. 
			if (index1 < index2)
				return -1;
			
			// If the index of object2 is smaller than the index of object1, then object2 comes first. 
			if (index2 < index1)
				return 1;

			// In the unlikely case that both their indexes are the same, then use their IDs to compare them.
			return object1.getId().compareTo(object2.getId());
		}
	}

	// Collection of ItemInstances that updateOrderedItemInstances() synchronizes with the current set of
	// ItemInstances in the DB and sorts appropriately (see updateOrderedItemInstances).
	private INarrowCustomArrayAdapter<ItemInstance> m_orderedItemInstances = null;
	public void setOrderedItemInstances(INarrowCustomArrayAdapter<ItemInstance> orderedItemInstances) throws SQLException
	{
		m_orderedItemInstances = orderedItemInstances;

        // Reopen the m_childItemInstancesManager.  The open() method will add ItemInstances to m_orderedItemInstances.
        m_childItemInstancesManager.close();
        m_childItemInstancesManager.open();

		sortOrderedItemInstances();
	}
	
	public ItemInstance getItemInstance(int location) {
		return m_orderedItemInstances.get(location);
	}



	public void sortOrderedItemInstancesIfNecessary() {
		if(!m_isItemInstanceSortingNeeded) return;
		sortOrderedItemInstances();
	}

	/**
	 * (Re)sort m_orderedItemInstances first by the pickup index (so that the picked up ItemInstances appear
	 * first in the order in which the user picked them up), and then by the user's "natural shopping order" at
	 * the current Store (so the ItemInstances not yet picked up appear in the user's "natural shopping order").
	 */
	public void sortOrderedItemInstances()
	{
		if (m_orderedItemInstances == null) return;

		// Sort orderedItemInstances using an ItemInstanceComparator.
		ItemInstanceComparator comparator = new ItemInstanceComparator(getStore());
		m_orderedItemInstances.sortFromAnyThread(comparator);
		m_isItemInstanceSortingNeeded = false;
		this.notifyOrderedInstancesChanged();
	}

	/**
	 * Notify the ordered ItemInstances that some ItemInstance has changed.
	 */
	public void notifyOrderedInstancesChanged() {
		if (Looper.myLooper() != Looper.getMainLooper()) return;
		if (m_orderedItemInstances != null) m_orderedItemInstances.notifyDataSetChanged();
	}

	/**
	 * Pick up the specified ItemInstance.  This assigns it the current Store and a pickup index.
	 * @param pickedUpItemInstance The ItemInstance to be transitioned to the "picked up" state.
	 */
	public void pickUpItemInstance(ItemInstance pickedUpItemInstance)
	{
		int maxPickupIndex = getMaxPickupIndex();
		pickedUpItemInstance.setPickupIndex(maxPickupIndex + 1);
		pickedUpItemInstance.setPickupStoreId(getStore().getId());
		sortOrderedItemInstances();
	}
	
	/**
	 * Drop (i.e. the opposite of pick up) the specified ItemInstance.  This assigns it a null pickup index
	 * and a null pickup Store.
	 * @param droppedItemInstance The ItemInstance to be transitioned to the "not picked up" state.
	 */
	public void dropItemInstance(ItemInstance droppedItemInstance)
	{
		droppedItemInstance.setPickupIndex(null);
		droppedItemInstance.setPickupStoreId(null);
		sortOrderedItemInstances();
	}

	/**
	 * Move a picked up ItemInstance from the specified position to another specified position.
	 * @param from The 0-based index of the ItemInstance to be moved.
	 * @param to The 0-based index of the intended destination of the ItemIndex.
	 * @return True if successful, otherwise false.
	 * @throws SQLException
	 */
	public boolean moveItemInstance(int from, int to) throws SQLException {
		if (m_orderedItemInstances == null) return false;
		
		List<ItemInstance> itemInstances = m_orderedItemInstances.getObjects();

		ItemInstance fromItemInstance = itemInstances.get(from);
		ItemInstance toItemInstance = itemInstances.get(to);
		if (!fromItemInstance.isPickedUp()) return false;
		if (!toItemInstance.isPickedUp()) return false;
		int toPickupIndex = toItemInstance.getPickupIndex();
		
		int increment = -1;
		if (from < to) increment = 1;
		for (int i = to; i != from; i -= increment) {
			ItemInstance prevItem = itemInstances.get(i-increment);
			ItemInstance curItem = itemInstances.get(i);
			Integer prevItemIndex = prevItem.getPickupIndex();
			curItem.setPickupIndex(prevItemIndex);
		}
		fromItemInstance.setPickupIndex(toPickupIndex);

		sortOrderedItemInstances();
		return true;
	}

	public Item deleteItemInstance(int position) throws SQLException
	{
		if (m_orderedItemInstances == null) return null;
		
		List<ItemInstance> itemInstances = m_orderedItemInstances.getObjects();
		ItemInstance doomedItemInstance = itemInstances.get(position);

		Item item = doomedItemInstance.getItem();
		ItemInstanceExtent.instance().delete(doomedItemInstance);
		return item;
	}

	public Item getItem(int position) throws SQLException
	{
		if (m_orderedItemInstances == null) return null;
		
		List<ItemInstance> itemInstances = m_orderedItemInstances.getObjects();
		ItemInstance soughtItemInstance = itemInstances.get(position);

		return soughtItemInstance.getItem();
	}

	public void deleteAllItemInstances(Item item) throws SQLException
	{
		if (item == null) return;
		if (m_orderedItemInstances == null) return;
		
		List<ItemInstance> itemInstances = m_orderedItemInstances.getObjects();
		LinkedList<ItemInstance> doomedItemInstances = new LinkedList<>();
		for(ItemInstance itemInstance : itemInstances)
			if (itemInstance.getItem() == item) doomedItemInstances.add(itemInstance);

		for(ItemInstance doomedItemInstance : doomedItemInstances) {
			doomedItemInstance.delete();
		}
	}
	
	/**
	 * Find the ItemInstance with the largest pickup index and return its pickup index.  If no ItemInstance is
	 * picked up, then return 0.
	 * @return The largest pickup index of all the ItemInstances that have been picked up, or 0 if no
	 * ItemInstances have been picked up.  
	 */
	private int getMaxPickupIndex()
	{
		Integer maxPickupIndex = 0;
		for(ItemInstance itemInstance : getItemInstances()) {
			if (itemInstance.isPickedUp()) {
				Integer pickupIndex = itemInstance.getPickupIndex();
				if (pickupIndex > maxPickupIndex) maxPickupIndex = pickupIndex;
			}
		}
		return maxPickupIndex;
	}
	
	
	// TODO: Maybe this should move to an extent
	/**
	 * Save the natural shopping order of the StoreItems corresponding to the picked up ItemInstances in
	 * the ShoppingList.
	 * @param keepPickedUpItems False if the ItemInstances should be deleted from the ShoppingList afterwards,
	 *        otherwise true. 
	 * @param task The SaveItemsOrderTask whose worker thread animates the execution of this method.
	 * @throws SQLException
	 * @throws CancelledException 
	 */
	public void saveNaturalShoppingOrder(boolean keepPickedUpItems, SaveItemsOrderTask task) throws SQLException, CancelledException
	{
		if (m_orderedItemInstances == null) return;

		List<ItemInstance> doomedItemInstances = new ArrayList<>();
        HashSet<Store> distinctStores = getDistinctStoresFromOrderedItemInstances();
        if(distinctStores == null) return;
		
		for(Store store : distinctStores) {
            // TODO: Maybe this should be an array of StoreItem IDs
            List<StoreItem> orderedStoreItems = new ArrayList<>();

			for(ItemInstance itemInstance : m_orderedItemInstances.getObjects()) {
				if(store.getId().equals(itemInstance.getPickupStoreId())) {
					doomedItemInstances.add(itemInstance);
					StoreItem storeItem = StoreItemExtent.instance().find(store, itemInstance.getItem());
                    if (storeItem == null) {
                        storeItem = new StoreItem(store.getId(), itemInstance.getItemId());
                    }
					orderedStoreItems.add(storeItem);
				}
			}
			store.updateNaturalShoppingOrder(orderedStoreItems, task);
			task.checkCancelledAndThrow("saveNaturalShoppingOrder");
		}

		// Create ItemPurchaseEvents for the ItemInstances for which we just updated the shopping order.
		for (ItemInstance doomedItemInstance : doomedItemInstances) {
			new ItemPurchaseEvent(doomedItemInstance.getItemId());
		}

        // Delete the picked-up ItemInstances for which we just updated the shopping order.
		if (!keepPickedUpItems) {
			for (ItemInstance doomedItemInstance : doomedItemInstances) {
				m_orderedItemInstances.removeFromAnyThread(doomedItemInstance);
				doomedItemInstance.delete();
			}
		}

		sortOrderedItemInstances();
	}
	
	/**
	 * Get distinct Stores at which the ordered ItemInstances have been picked up.
	 * 
	 * @return A hashSet containing Stores.
	 * @throws SQLException 
	 */
	private HashSet<Store> getDistinctStoresFromOrderedItemInstances() throws SQLException
	{
		if (m_orderedItemInstances == null) return null;
		
		HashSet<Store> distinctStores = new HashSet<>();
		for(ItemInstance itemInstance : m_orderedItemInstances.getObjects())
			if(itemInstance.getPickupStore() != null) 
				distinctStores.add(itemInstance.getPickupStore());
		
		return distinctStores;
	}
	
	public Double getTotalPickedUp() {
		BooleanResult isKnownPrice = new BooleanResult();
		Double knownTotal = 0.00;
		Double unknownTotal = 0.00;
		for(ItemInstance itemInstance : getItemInstances()) {
			if (!itemInstance.isPickedUp()) continue;
			Double price = itemInstance.getStoreItemPrice(isKnownPrice);
			if (price != null) {
				int count;
				if (itemInstance.getHasCount()) { 
					count = itemInstance.getCount();
				} else {
					count = 1;
				}
				if (isKnownPrice.get())
					knownTotal += price * count;
				else
					unknownTotal += price * count;
			}
		}
		
		return knownTotal + unknownTotal;
	}

	public Double getTotal() {
		BooleanResult isKnownPrice = new BooleanResult();
		Double knownTotal = 0.00;
		Double unknownTotal = 0.00;
		for(ItemInstance itemInstance : getItemInstances()) {
			Double price = itemInstance.getStoreItemPrice(isKnownPrice);
			if (price != null) {
				int count;
				if (itemInstance.getHasCount()) { 
					count = itemInstance.getCount();
				} else {
					count = 1;
				}
				if (isKnownPrice.get())
					knownTotal += price * count;
				else
					unknownTotal += price * count;
			}
		}
		
		return knownTotal + unknownTotal;
	}
}
