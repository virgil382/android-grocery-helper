// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Upgrade.UpgradeManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.j256.ormlite.dao.Dao;

public class TableUpgradeSpec {
	private static final String TAG = "DatabaseUpgrade";

	private String m_name;
	private String m_newName;
	
	public TableUpgradeSpec(String name) {
		m_name = name;
	}
	public String name() {
		return (m_name);
	}

	public TableUpgradeSpec newName(String value) {
		m_newName = value;
		return(this);
	}
	public String newName() {
		if (m_newName != null) return(m_newName);
		return (m_name);
	}

	public String temporaryName() { return(m_name + "Tmp"); }
	
	private ArrayList<ColumnUpgradeSpec> m_columnSpecs = new ArrayList<>();
	public TableUpgradeSpec addColumnSpec(ColumnUpgradeSpec cs) {
		cs.parentTableName(name());
		m_columnSpecs.add(cs);
		return(this);
	}
	
	public ColumnUpgradeSpec findPrimaryKey() {
		for(ColumnUpgradeSpec cs : m_columnSpecs) {
			if (cs.isKey()) return(cs);
		}
		return(null);
	}	
	
	/**
	 * Find the ColumnUpgradeSpecs that represent a foreign keys for the specified ColumnUpgradeSpec.
	 * @param keyCs The ColumnUpgradeSpec that specifies the key.
	 * @param foreignKeysCs A List of ColumnUpgradeSpecs that specify the foreign keys.
	 */
	public void findForeignKeys(ColumnUpgradeSpec keyCs, List<ColumnUpgradeSpec> foreignKeysCs) {
		for(ColumnUpgradeSpec cs : m_columnSpecs) {
			if (cs.isForeignKeyFor(keyCs.parentTableName(), keyCs.name())) {
				foreignKeysCs.add(cs);
			}
		}
	}
	
	/**
	 * In the original table, create temporary columns for the columns whose types are being changed.
	 * 
	 * @param dao The Dao used to execute SQL statements.
	 * @throws SQLException 
	 */
	public void createTemporaryColumns(Dao<Bogus, Void> dao) throws SQLException {
		// In the original table, create temporary columns for the columns whose types are being changed.
		for(ColumnUpgradeSpec cs : m_columnSpecs) {
			if(cs.newType() != null) {
				cs.addTemporaryColumn(name(), dao);
			}
		}
	}

	/**
	 * Upgrade data in the original table and in other tables (e.g. tables with foreign keys).  This might include:
	 * - Generate data in replacement columns
	 * - Generate data in new columns
	 * - Change data in the rest of the columns
	 * 
	 * @param upgradeManager The UpgradeManager.
     * @param dao The Dao used to execute SQL statements.
	 * @throws SQLException 
	 */
	public void upgradeData(UpgradeManager upgradeManager, Dao<Bogus, Void> dao) throws SQLException {
		for(ColumnUpgradeSpec cs : m_columnSpecs) {
			TypeMapper mapper = cs.typeMapper();
			if (mapper == null) continue;
			
			ArrayList<ColumnUpgradeSpec> foreignKeys = new ArrayList<>();
			upgradeManager.findForeignKeys(cs, foreignKeys);
			mapper.mapColumnValues(dao, cs, foreignKeys);
		}
	}
	
	public void createTemporaryTable(Dao<Bogus, Void> dao) throws SQLException {
		String s = "CREATE TABLE " + temporaryName() + " (";
		
		String separator = "";
		
		for(ColumnUpgradeSpec cs : m_columnSpecs) {
			if (cs.isObsolete()) continue;  // skip obsolete columns
			
			s += separator;
			s += cs.newName();

			if (cs.newType() != null) s += " " + cs.newType();
			else s += " " + cs.type();
			
			if (cs.newIsKey()) s += " PRIMARY KEY NOT NULL"; 
					
			separator = ", ";
		}
		
		s += ");";
		
		Log.d(TAG, s);
		dao.executeRaw(s);
	}
	
	public void copyDataToTemporaryTable(Dao<Bogus, Void> dao) throws SQLException {
		// Skip obsolete columns.
		// Copy data for type change columns (from temporary column name to replacement column name).
		// Copy data for remaining columns.
		String s = "INSERT INTO " + temporaryName() + " (";
		
		String separator = "";
		for(ColumnUpgradeSpec cs : m_columnSpecs) {
			if (cs.isObsolete()) continue;  // skip obsolete columns
            if (cs.isNew()) continue;  // skip new columns (nothing to copy from)

			s += separator;
			s += cs.newName();  // copy to new column name

			separator = ", ";
		}
		
		s += ") SELECT ";
		
		separator = "";
		for(ColumnUpgradeSpec cs : m_columnSpecs) {
			if (cs.isObsolete()) continue;  // skip obsolete columns
            if (cs.isNew()) continue;  // skip new columns (nothing to copy from)

			s += separator;

			if (cs.newType() != null) s += cs.temporaryName();  // copy from temporary column name 
			else s += cs.name();                                // or from old column name 

			separator = ", ";
		}
		
		s += " FROM " + name() + ";";

		Log.d(TAG, s);
		dao.executeRaw(s);
	}

	public void dropOriginalTable(Dao<Bogus, Void> dao) throws SQLException {
		// Drop the table.
		String s = "DROP TABLE " + name() + ";";
		Log.d(TAG, s);
		dao.executeRaw(s);

		// Drop the indexes.
		for(ColumnUpgradeSpec cs : m_columnSpecs) {
			cs.dropIndexIfExtant(name(), dao);
		}
	}

	public void renameTableAndCreateIndexes(Dao<Bogus, Void> dao) throws SQLException {
		// Rename the temporary table.
		String s = "ALTER TABLE " + temporaryName() + " RENAME TO " + newName() + ";";
		Log.d(TAG, s);
		dao.executeRaw(s);
		
		// Create the indexes on the new table.
		for(ColumnUpgradeSpec cs : m_columnSpecs) {
			if (cs.isObsolete()) continue;
			if (!cs.newIsIndex()) continue;
			cs.createIndex(newName(), dao);
		}
	}
}
