// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel;

import com.homecortex.groceryShoppingList.Util.ObjectUtils;

import java.lang.reflect.Field;

/**
 * A <code>DefaultValueTracker</code> tracks the default value of a type and its popularity
 * by counting the number of times that callers propose a default value via calls to 
 * <code>proposeDefaultValue()</code>.  A default value's popularity count goes up when 
 * callers propose a default value that equals the current default value, and it goes down 
 * when callers propose a different default value.  The highest popularity count that the 
 * current default value may reach is specified on construction, and the lowest is 1.  If
 * the popularity count drops to 0, then the newly proposed default value becomes the current
 * default value and its popularity count is set to 1.
 * 
 * <p>A <code>DefaultValueTracker</code> updates the current default value and the popularity
 * count by referencing fields in a container object via reflection.  The container 
 * object must contain a field of type <code>T</code> to hold the current default value,
 * and a field of type <code>Integer</code> to hold its popularity count.
 *
 * @param <T> The type of value whose popularity is tracked by the DefaultValueTracker. 
 */
public class DefaultValueTracker<T>
{
	private Object parentInstance;
	
	private T defaultValue;
	private Field defaultValueField;
	
	private Integer popularityCount;
	private Field popularityCountField;
	
	private int maxPopularityCount;
	
	/**
	 * Construct a DefaultValueTracker.
	 * 
	 * @param containerInstance The container object. 
	 * @param defaultValueFieldName The name of the field of type <code>T</code> (within the
	 * container object) that holds the current default value.  
	 * @param popularityCountFieldName The name of the field of type <code>Integer</code> (within
	 * the container object) that the popularity count.
	 */
	@SuppressWarnings("unchecked")
	public DefaultValueTracker(Object containerInstance, String defaultValueFieldName, String popularityCountFieldName, int maxPopularityCount) {
		try {
			defaultValueField = containerInstance.getClass().getDeclaredField(defaultValueFieldName);
			defaultValueField.setAccessible(true);
			popularityCountField = containerInstance.getClass().getDeclaredField(popularityCountFieldName);
			popularityCountField.setAccessible(true);
		} catch (SecurityException | NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.parentInstance = containerInstance;
		
		try {
			this.defaultValue = (T) defaultValueField.get(containerInstance);
			this.popularityCount = (Integer) popularityCountField.get(containerInstance);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.maxPopularityCount = maxPopularityCount;
	}
	
	/**
	 * Propose a new default value and (if appropriate) update the container object's fields that
	 * hold the current default value and the popularity count.
	 * @param newProposedDefaultValue The new proposed value.
	 */
	@SuppressWarnings("unchecked")
	public void proposeDefaultValue(T newProposedDefaultValue)
	{
		try {
			defaultValue = (T) defaultValueField.get(parentInstance);
			popularityCount = (Integer) popularityCountField.get(parentInstance);
			if(popularityCount == null) popularityCount = 0;

            if(ObjectUtils.equals(defaultValue, newProposedDefaultValue)) {
                // The new value equals the current default value, so increase the popularity count.
                if (popularityCount < maxPopularityCount) {
                    popularityCount++;
                }
            } else {
                // The new value is different than the default value, so decrease the popularity count.
                popularityCount--;
                if (popularityCount <= 0) {
                    defaultValue = newProposedDefaultValue;
                    popularityCount = 1;
                    defaultValueField.set(parentInstance, defaultValue);
                }
            }

			popularityCountField.set(parentInstance, popularityCount);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
