package com.homecortex.groceryShoppingList.DataModel.Upgrade.UpgradeManager;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "bogus")
public class Bogus {
	@DatabaseField(generatedId = true)
	private Integer id;

	public Bogus() {}
}
