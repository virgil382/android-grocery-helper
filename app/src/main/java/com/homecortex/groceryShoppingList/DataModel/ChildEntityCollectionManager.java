// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import com.homecortex.groceryShoppingList.DataModel.Entities.UpdateSet;
import com.homecortex.groceryShoppingList.DataModel.Extents.EntityService;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IChildEntity;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntityCollectionQueryResultHandler;

public class ChildEntityCollectionManager<E extends IChildEntity> extends EntityObserver<E> implements IEntityCollectionQueryResultHandler<E> {
	private IEntity m_parent;
	private String m_foreignKeyName;
	private EntityService<E> m_entityService;
	private boolean m_isOpen = false;
	private ConcurrentHashMap<UUID, E> m_childrenById = new ConcurrentHashMap<>();

	/**
	 * Create a ChildEntityCollectionManager.
	 * @param parent The parent that owns the children.
	 * @param foreignKeyName The name of the foreign key field in the child (that specifies the parent ID).  It MUST be named
	 *        the same as the DB column.
	 */
	public ChildEntityCollectionManager(IEntity parent, String foreignKeyName, EntityService<E> entityService) {
		m_parent = parent;
		m_foreignKeyName = foreignKeyName;
		m_entityService = entityService;
	}
	
	@Override
	public void open() {
		if(m_isOpen) return;
		m_isOpen = true;
		
		m_entityService.addObserver(this);
		m_entityService.getByFieldValue(m_foreignKeyName, m_parent.getId(), this);
	}
	
	@Override
	public void close() {
		if(!m_isOpen) return;
		m_isOpen = false;
		
		m_childrenById.clear();
		m_entityService.dropObserver(this);
	}

	@Override
	public void onResult(Collection<E> objects) {
		for(E object : objects) {
			if (!isManagedChild(object)) continue;
			m_childrenById.put(object.getId(), object);
			object.setParent(m_foreignKeyName, m_parent);
		}
	}

	/**
	 * Determine if the specified object is a child that should be managed by the ChildEntityCollectionManager.
	 * @param possibleChildObject The (potentially) child object.
	 * @return True if it is, otherwise false.
	 */
	public boolean isManagedChild(E possibleChildObject) {
		UUID parentId = getParentId(possibleChildObject);
		return(parentId != null && m_parent.getId().equals(parentId));
	}

	private UUID getParentId(E possibleChildObject) {
		try {
			//TODO: Optimize this.  Retrieve the Field from the class just once.
			Field field = possibleChildObject.getClass().getDeclaredField(m_foreignKeyName);
			field.setAccessible(true);
			return (UUID) (field.get(possibleChildObject));
		} catch (SecurityException | NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return(null);
	}
	
	public Collection<E> getCollection() {
		return(m_childrenById.values());
	}
	
	@Override
	public void onCreate(E object) {
		if (!isManagedChild(object)) return;
		m_childrenById.put(object.getId(), object);
		object.setParent(m_foreignKeyName, m_parent);
	}

	@Override
	public void onUpdate(E object, UpdateSet updates) { }

	@Override
	public void onDelete(E object) {
		if (!isManagedChild(object)) return;
		m_childrenById.remove(object.getId());
		object.setParent(m_foreignKeyName, null);
	}
}
