// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Extents;

import java.sql.SQLException;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.ItemAttribute;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.j256.ormlite.dao.Dao;

public class ItemAttributeExtent extends EntityService<ItemAttribute> {
	private static ItemAttributeExtent m_instance;
	public static ItemAttributeExtent instance() {
		if (m_instance == null) m_instance = new ItemAttributeExtent();
		return m_instance;
	}
	
	// The DB connection.
	Dao<ItemAttribute, UUID> m_classDao;
	@Override
	public Dao<ItemAttribute, UUID> classDao() {
		if (m_classDao != null) return m_classDao;
		m_classDao = ApplicationPm.getDatabaseHelper().getItemAttributeDao();
		return m_classDao;
	}

	@Override
	public void delete(ItemAttribute doomedObject) {
		// Remove and delete all the ItemInstanceAttributes that reference me.
		ItemInstanceAttributeExtent.instance().deleteByFieldValue("itemAttributeId", doomedObject.getId());

		doomedObject.close();
		notifyDelete(doomedObject);
		try {
			classDao().delete(doomedObject);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}
}
