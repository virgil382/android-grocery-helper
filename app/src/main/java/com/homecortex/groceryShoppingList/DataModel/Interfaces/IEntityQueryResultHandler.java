// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Interfaces;

public interface IEntityQueryResultHandler<E> {
	public void onResult(E object);
}
