// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Extents;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.Item;
import com.homecortex.groceryShoppingList.DataModel.Entities.Store;
import com.homecortex.groceryShoppingList.DataModel.Entities.StoreItem;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.j256.ormlite.dao.Dao;

public class StoreItemExtent extends EntityService<StoreItem> {
	private static StoreItemExtent m_instance;
	public static StoreItemExtent instance() {
		if (m_instance == null) m_instance = new StoreItemExtent();
		return m_instance;
	}
	
	// The DB connection.
	Dao<StoreItem, UUID> m_classDao;

	@Override
	public Dao<StoreItem, UUID> classDao() {
		if (m_classDao != null) return m_classDao;
		m_classDao = ApplicationPm.getDatabaseHelper().getStoreItemDao();
		return m_classDao;
	}

	public StoreItem find(Store store, Item item) throws SQLException {
		Map<String, Object> map = new HashMap<>();
		map.put("storeId", store.getId());
		map.put("itemId", item.getId());
		List<StoreItem> storeItems = classDao().queryForFieldValues(map);
		if (storeItems.size() == 0) return null;
		return storeItems.get(0);
	}
	
	@Override
	public void delete(StoreItem doomedObject) {
		// The StoreItem will be removed from Items when their ChildEntityCollectionManager receives the onDelete() event.
		// The StoreItem will be removed from Stores when their ChildEntityCollectionManager receives the onDelete() event.
		
		// Delete all the inbound and outbound StoreItemEdges.
		StoreItemEdgeExtent.instance().deleteByFieldValue("headStoreItemId", doomedObject.getId());
		StoreItemEdgeExtent.instance().deleteByFieldValue("tailStoreItemId", doomedObject.getId());

		doomedObject.close();
		notifyDelete(doomedObject);
		try {
			classDao().delete(doomedObject);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
