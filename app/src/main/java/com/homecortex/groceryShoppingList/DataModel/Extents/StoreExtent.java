// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Extents;

import java.sql.SQLException;
import java.util.Collection;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstance;
import com.homecortex.groceryShoppingList.DataModel.Entities.ShoppingList;
import com.homecortex.groceryShoppingList.DataModel.Entities.Store;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntityCollectionQueryResultHandler;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.j256.ormlite.dao.Dao;

public class StoreExtent extends EntityService<Store> {
	private static StoreExtent m_instance;
	public static StoreExtent instance() {
		if (m_instance == null) {
			m_instance = new StoreExtent();
		}
		return m_instance;
	}
	
	// The DB connection.
	Dao<Store, UUID> classDao;
	
	@Override
	public Dao<Store, UUID> classDao() {
		if (classDao != null) return classDao;
		classDao = ApplicationPm.getDatabaseHelper().getStoreDao();
		return classDao;
	}

	@Override
	public void delete(Store doomedObject)
	{
		// Delete all StoreItems
		StoreItemExtent.instance().deleteByFieldValue("storeId", doomedObject.getId());

		// Unselect the store from each ShoppingList that has it currently selected.
		ShoppingListExtent.instance().getByFieldValue("storeId", doomedObject.getId(), new IEntityCollectionQueryResultHandler<ShoppingList>() {
			@Override
			public void onResult(Collection<ShoppingList> objects) {
				for(ShoppingList object : objects) {
					object.setStoreId(null);
				}
			}
		});

		// Drop each ItemInstance picked up at this store.
		ItemInstanceExtent.instance().getByFieldValue("pickupStoreId", doomedObject.getId(), new IEntityCollectionQueryResultHandler<ItemInstance>() {
			@Override
			public void onResult(Collection<ItemInstance> objects) {
				for(ItemInstance itemInstance : objects) {
					itemInstance.isPickedUp(false);
				}
			}
		});

		doomedObject.close();
		notifyDelete(doomedObject);
		try {
			classDao().delete(doomedObject);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
