// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Extents;

import java.sql.SQLException;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstance;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.j256.ormlite.dao.Dao;

public class ItemInstanceExtent extends EntityService<ItemInstance> {
	private static ItemInstanceExtent m_instance;
	public static ItemInstanceExtent instance() {
		if (m_instance == null) m_instance = new ItemInstanceExtent();
		return m_instance;
	}
	
	// The DB connection.
	Dao<ItemInstance, UUID> m_classDao;
	
	@Override
	public Dao<ItemInstance, UUID> classDao() {
		if (m_classDao != null) return m_classDao;
		m_classDao = ApplicationPm.getDatabaseHelper().getItemInstanceDao();
		return m_classDao;
	}

	@Override
	public void delete(ItemInstance doomedObject) {
		// Remove and delete all the ItemInstanceAttributes that reference doomedObject.
		ItemInstanceAttributeExtent.instance().deleteByFieldValue("itemInstanceId", doomedObject.getId());
		
		try {
			classDao().delete(doomedObject);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		notifyDelete(doomedObject);
		doomedObject.close();
	}
}
