// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Entities;

import java.sql.SQLException;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Extents.VolumeUnitExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.homecortex.groceryShoppingList.Util.ObjectUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "volumeUnit")
public class VolumeUnit implements IEntity {
	/// ORMLite needs a parameterless constructor.
	public VolumeUnit() { }
	
	public VolumeUnit(String name, double ml) throws SQLException {
		this.name = name;
		this.milliliters = ml;
		
		VolumeUnitExtent.instance().create(this);
	}

	@Override
	public void open() { }
	@Override
	public void close() { }
	@Override
	public void delete() { }
	
	@DatabaseField(generatedId = true)
	private UUID volumeUnitId;
	public UUID getId() { return volumeUnitId; }

	@DatabaseField
	private String name;
	public String getName() { return this.name; }
	public void setName(String newValue) {
		if(ObjectUtils.equals(newValue, name)) return;
		name = newValue;
		VolumeUnitExtent.instance().update(this, new UpdateSet("name"));
	}

	@Override
	public String toString() { return(name); }
	
	@DatabaseField
	private Double milliliters;
	public Double getMilliliters() { return this.milliliters; }
}
