// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Entities;

import java.util.HashSet;

public class UpdateSet {
	HashSet<String> m_updatedFieldNames = new HashSet<>();
	
	public UpdateSet(String updateFieldName1) {
		m_updatedFieldNames.add(updateFieldName1);
	}
	
	public UpdateSet(String updateFieldName1, String updateFieldName2) {
		m_updatedFieldNames.add(updateFieldName1);
		m_updatedFieldNames.add(updateFieldName2);
	}

	public UpdateSet(String updateFieldName1, String updateFieldName2, String updateFieldName3) {
		m_updatedFieldNames.add(updateFieldName1);
		m_updatedFieldNames.add(updateFieldName2);
		m_updatedFieldNames.add(updateFieldName3);
	}

	public boolean contains(String fieldName) {
		return(m_updatedFieldNames.contains(fieldName));
	}
}
