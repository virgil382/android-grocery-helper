// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Extents;

import java.sql.SQLException;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.ShoppingList;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.j256.ormlite.dao.Dao;

public class ShoppingListExtent extends EntityService<ShoppingList>{
	private static ShoppingListExtent m_instance;
	public static ShoppingListExtent instance() {
		if (m_instance == null) m_instance = new ShoppingListExtent();
		return m_instance;
	}
	
	// The DB connection.
	Dao<ShoppingList, UUID> m_classDao;
	
	@Override
	public Dao<ShoppingList, UUID> classDao() {
		if (m_classDao != null) return m_classDao;
		m_classDao = ApplicationPm.getDatabaseHelper().getShoppingListDao();
		return m_classDao;
	}

	@Override
	public void delete(ShoppingList doomedObject) {
		ItemInstanceExtent.instance().deleteByFieldValue("shoppingListId", doomedObject.getId());
		
		doomedObject.close();
		notifyDelete(doomedObject);
		try {
			classDao().delete(doomedObject);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
