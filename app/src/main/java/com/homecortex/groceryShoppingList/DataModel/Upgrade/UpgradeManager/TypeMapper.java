// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Upgrade.UpgradeManager;

import java.sql.SQLException;
import java.util.List;

import com.homecortex.groceryShoppingList.DataModel.Upgrade.UpgradeManager.Bogus;
import com.homecortex.groceryShoppingList.DataModel.Upgrade.UpgradeManager.ColumnUpgradeSpec;
import com.j256.ormlite.dao.Dao;

public interface TypeMapper {
	String newType();
	
	/**
	 * Query for unique values in a table column and map these values to values of the new type.  If the
	 * column is a key, then also update corresponding foreign key values.
	 * @param dao The Dao used to execute database queries.
	 * @param keyCs The ColumnUpgradeSpec for the column whose values are to be mapped.
	 * @param foreignKeys List of ColumnUpgradeSpec for foreign key columns.  May be empty.
	 * @throws SQLException 
	 */
	void mapColumnValues(Dao<Bogus, Void> dao, ColumnUpgradeSpec keyCs, List<ColumnUpgradeSpec> foreignKeys) throws SQLException;
}
