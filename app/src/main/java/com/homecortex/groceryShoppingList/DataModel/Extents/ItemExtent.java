// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Extents;

import java.sql.SQLException;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.Item;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.j256.ormlite.dao.Dao;

public class ItemExtent extends EntityService<Item> {
	private static ItemExtent m_instance;
	public static ItemExtent instance() {
		if (m_instance == null) m_instance = new ItemExtent();
		return m_instance;
	}
	
	// The DB connection.
	Dao<Item, UUID> m_classDao;
	
	@Override
	public Dao<Item, UUID> classDao() {
		if (m_classDao != null) return m_classDao;
		m_classDao = ApplicationPm.getDatabaseHelper().getItemDao();
		return m_classDao;
	}

	@Override
	public void delete(Item doomedObject) {
		StoreItemExtent.instance().deleteByFieldValue("itemId", doomedObject.getId());
		ItemInstanceExtent.instance().deleteByFieldValue("itemId", doomedObject.getId());
		ItemAttributeExtent.instance().deleteByFieldValue("itemId", doomedObject.getId());
		ItemPurchaseEventExtent.instance().deleteByFieldValue("itemId", doomedObject.getId());

		try {
			classDao().delete(doomedObject);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		notifyDelete(doomedObject);
		doomedObject.close();
	}
}
