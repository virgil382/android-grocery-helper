// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Interfaces;

import java.util.UUID;

public interface IEntity {
	public UUID getId();
	public void open();
	public void close();
	public void delete();
}
