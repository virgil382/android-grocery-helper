package com.homecortex.groceryShoppingList.DataModel.Upgrade.DbUpgradeStrategies;

import com.homecortex.groceryShoppingList.DataModel.Upgrade.ShortestPathCalculator.Edge;
import com.j256.ormlite.support.ConnectionSource;

public interface IDbUpgradeStrategy extends Edge {
    void doUpgrade(ConnectionSource connectionSource);
}
