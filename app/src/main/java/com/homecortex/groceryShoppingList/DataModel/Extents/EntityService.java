// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Extents;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.homecortex.groceryShoppingList.DataModel.Entities.UpdateSet;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntityCollectionQueryResultHandler;
import com.homecortex.groceryShoppingList.DataModel.EntityObserver;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntityQueryResultHandler;
import com.homecortex.groceryShoppingList.Ui.DataModelUpdateNotifier;
import com.j256.ormlite.dao.Dao;

public abstract class EntityService<E extends IEntity> {
	public abstract Dao<E, UUID> classDao();
	
	public boolean create(E newObject) {
		try {
			classDao().create(newObject);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return(false);
		}
		newObject.open();
		notifyCreate(newObject);
		return true;
	}
	
	public void getById(UUID id, IEntityQueryResultHandler<E> resultHandler) {
        if(id == null) return;
		E object;
		try {
			object = classDao().queryForId(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		if (object != null) object.open();
		resultHandler.onResult(object);
	}
	
	public void getAll(IEntityCollectionQueryResultHandler<E> resultHandler) {
		List<E> results;
		try {
			results = classDao().queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
		
		for(E object : results) object.open();
		
		resultHandler.onResult(results);
	}

	public void getByFieldValue(String fieldName, Object fieldValue, IEntityCollectionQueryResultHandler<E> resultHandler) {
		if(fieldValue == null) return;
		List<E> results;
		try {
			results = classDao().queryForEq(fieldName, fieldValue);
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
		
		for(E object : results) object.open();
		resultHandler.onResult(results);
	}
	
	public void update(E entity, UpdateSet updates) {
		try {
			classDao().update(entity);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		notifyUpdate(entity, updates);
	}
	
	public void deleteByFieldValue(String fieldName, Object fieldValue) {
		Map<String, Object> map = new HashMap<>();
		map.put(fieldName, fieldValue);
		List<E> objects;
		try {
			objects = classDao().queryForFieldValues(map);
			for(E doomedObject : objects) {
				delete(doomedObject);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public abstract void delete(E doomedObject);

	ConcurrentLinkedQueue<EntityObserver<E>> m_observers = new ConcurrentLinkedQueue<>();
	
	public void addObserver(EntityObserver<E> observer) {
		m_observers.add(observer);
	}

	public void dropObserver(EntityObserver<E> observer) {
		m_observers.remove(observer);
	}
	
	public void notifyCreate(E object) {
		for(EntityObserver<E> observer : m_observers) {
			observer.onCreate(object);
		}
		DataModelUpdateNotifier.instance().onDataModelChange();
	}

	public void notifyUpdate(E object, UpdateSet updates) {
		for(EntityObserver<E> observer : m_observers) {
			observer.onUpdate(object, updates);
		}
		DataModelUpdateNotifier.instance().onDataModelChange();
	}

	public void notifyDelete(E object) {
		for(EntityObserver<E> observer : m_observers) {
			observer.onDelete(object);
		}
		DataModelUpdateNotifier.instance().onDataModelChange();
	}
}
