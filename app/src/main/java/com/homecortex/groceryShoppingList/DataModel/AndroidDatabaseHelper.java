// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel;

import java.util.UUID;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.homecortex.groceryShoppingList.DataModel.Entities.Item;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemAttribute;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstance;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstanceAttribute;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemPurchaseEvent;
import com.homecortex.groceryShoppingList.DataModel.Entities.Settings;
import com.homecortex.groceryShoppingList.DataModel.Entities.ShoppingList;
import com.homecortex.groceryShoppingList.DataModel.Entities.Store;
import com.homecortex.groceryShoppingList.DataModel.Entities.StoreItem;
import com.homecortex.groceryShoppingList.DataModel.Entities.StoreItemEdge;
import com.homecortex.groceryShoppingList.DataModel.Entities.VolumeUnit;
import com.homecortex.groceryShoppingList.DataModel.Entities.WeightUnit;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IDatabaseHelper;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;

/**
 * A AndroidDatabaseHelper is a specialized OrmLiteSqliteOpenHelper that implements IDatabaseHelper.  It
 * "wraps" a contained DatabaseHelper to which it delegates all of the IDatabaseHelper operations.  The
 * DatabaseHelper's ConnectionSource is the same as the OrmLiteSqliteOpenHelper's (i.e. an
 * AndroidConnectionSource).
 * 
 * Typically, an AndroidDatabaseHelper is constructed from within an Activity.  The Activity passes itself
 * to the constructor.
 */
public class AndroidDatabaseHelper extends OrmLiteSqliteOpenHelper implements IDatabaseHelper
{
	// The DatabaseHelper implements all the getter methods for DAOs. AndroidDatabaseHelper delegates all
	// the IDatabaseHelper operations to it.  These operations are implemented in DatabaseHelper (not here)
	// so that they may be tested with ConnectionSources other than the OrmLiteSqliteOpenHelper's
	// AndroidConnectionSource.
	private DatabaseHelper databaseHelper;
	
	/**
	 * Create an AndroidDatabaseHelper.
	 * @param context The Activity that ultimately constructs this AndroidDatabaseHelper.
	 */
	public AndroidDatabaseHelper(Context context) {
		super(context, DatabaseHelper.DATABASE_NAME, null, DatabaseHelper.CURRENT_DATABASE_VERSION);
		
		// The superclass initialized connectionSource, so forward it to the DatabaseHelper.
		databaseHelper = new DatabaseHelper(connectionSource);
	}

	@Override
	public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource)
	{
		databaseHelper.create(connectionSource);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) 
	{
		databaseHelper.performUpgrades(connectionSource, oldVersion, newVersion);
	}

	public Dao<ShoppingList, UUID> getShoppingListDao()
	{
		return databaseHelper.getShoppingListDao();
	}

	public Dao<WeightUnit, UUID> getWeightUnitDao()
	{
		return databaseHelper.getWeightUnitDao();
	}

	public Dao<VolumeUnit, UUID> getVolumeUnitDao()
	{
		return databaseHelper.getVolumeUnitDao();
	}

	public Dao<Item, UUID> getItemDao() {
		return databaseHelper.getItemDao();
	}

	public Dao<ItemInstance, UUID> getItemInstanceDao()
	{
		return databaseHelper.getItemInstanceDao();
	}
	
	@Override
	public Dao<ItemAttribute, UUID> getItemAttributeDao() {
		return databaseHelper.getItemAttributeDao();
	}

	@Override
	public Dao<ItemInstanceAttribute, UUID> getItemInstanceAttributeDao() {
		return databaseHelper.getItemInstanceAttributeDao();
	}

	@Override
	public Dao<Store, UUID> getStoreDao() {
		return databaseHelper.getStoreDao();
	}

	@Override
	public Dao<StoreItem, UUID> getStoreItemDao() {
		return databaseHelper.getStoreItemDao();
	}

	@Override
	public Dao<StoreItemEdge, UUID> getStoreItemEdgeDao() {
		return databaseHelper.getStoreItemEdgeDao();
	}

	@Override
	public Dao<Settings, UUID> getSettingsDao() {
		return databaseHelper.getSettingsDao();
	}

	@Override
	public Dao<ItemPurchaseEvent, UUID> getItemPurchaseEventDao() {
		return databaseHelper.getItemPurchaseEventDao();
	}

	@Override
	public void close() {
		super.close();
		databaseHelper.close();
	}
}
