// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Entities;

import java.util.UUID;

public class ItemInstanceAttributeDesireIndicator
{
	private UUID m_itemInstanceId;
	private ItemAttribute m_itemAttribute;
	private ItemInstanceAttribute m_itemInstanceAttribute;
	
	public ItemInstanceAttributeDesireIndicator(UUID itemInstanceId, ItemAttribute itemAttribute) {
		m_itemInstanceId = itemInstanceId;
		m_itemAttribute = itemAttribute;
	}
	
	public UUID getItemAttributeId() { return(m_itemAttribute.getId()); }

	public boolean isDesired() { return (m_itemInstanceAttribute != null); }
	public void isDesired(ItemInstanceAttribute itemInstanceAttribute) {
		m_itemInstanceAttribute = itemInstanceAttribute;
	}

	public void onClickCheckbox() {
		if (isDesired()) {
			m_itemInstanceAttribute.delete();
			m_itemInstanceAttribute = null;
			// the EditItemInstanceDetailsPm will receive onDelete(ItemInstanceAttribute)
		} else {
			m_itemInstanceAttribute = new ItemInstanceAttribute(m_itemAttribute.getId(), m_itemInstanceId);
			// the EditItemInstanceDetailsPm will receive onCreate(ItemInstanceAttribute)
		}
	}
	
	public String getName() {
		return m_itemAttribute.getName();
	}

	// TODO: Remove after debugging.
	@Override
	public String toString() {
		return getName();
	}

	public void setName(String value) {
		m_itemAttribute.setName(value);
	}
	
	public void delete() {
		m_itemAttribute.delete();
	}
}
