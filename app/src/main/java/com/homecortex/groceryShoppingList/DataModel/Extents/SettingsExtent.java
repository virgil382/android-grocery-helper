// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Extents;

import java.sql.SQLException;
import java.util.Date;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.Settings;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.j256.ormlite.dao.Dao;

public class SettingsExtent {
	private static SettingsExtent instance;
	public static SettingsExtent instance() {
		if (instance == null) instance = new SettingsExtent();
		return instance;
	}
	
	// The DB connection.
	Dao<Settings, UUID> classDao;
	Dao<Settings, UUID> classDao() {
		if (classDao != null) return classDao;
		classDao = ApplicationPm.getDatabaseHelper().getSettingsDao();
		return classDao;
	}
	
	public void update(Settings object) throws SQLException {
		classDao().update(object);
	}

	public Date getStartOfItemPurchaseFrequencyTimeSpan() {
		// TODO: This should be anaged by the user and persisted in the DB.
		long lengthOfTimeSpan = 100;

		Date dateNow = new Date();
        return(new Date(dateNow.getTime() - lengthOfTimeSpan*1000*60*60*24));
	}
}
