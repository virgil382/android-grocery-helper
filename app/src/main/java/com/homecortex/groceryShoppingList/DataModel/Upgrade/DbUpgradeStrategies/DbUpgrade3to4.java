package com.homecortex.groceryShoppingList.DataModel.Upgrade.DbUpgradeStrategies;

import android.util.Log;

import com.homecortex.groceryShoppingList.DataModel.Entities.ItemPurchaseEvent;
import com.homecortex.groceryShoppingList.DataModel.Upgrade.UpgradeManager.Bogus;
import com.homecortex.groceryShoppingList.DataModel.Upgrade.UpgradeManager.ColumnUpgradeSpec;
import com.homecortex.groceryShoppingList.DataModel.Upgrade.UpgradeManager.TableUpgradeSpec;
import com.homecortex.groceryShoppingList.DataModel.Upgrade.UpgradeManager.UpgradeManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * A DbUpgrade2to4 can upgrade the DB from version 3 (present in version 1.0(21) of the application)
 * to version 4 of the DB.
 */
public class DbUpgrade3to4 implements IDbUpgradeStrategy {
    private static final String TAG = "DatabaseUpgrade";

    public String getSourceName() { return("3"); }
    public String getTargetName() { return("4"); }

    public void doUpgrade(ConnectionSource connectionSource) {
        // Upgrade the schema.
        try {
            Dao<Bogus, Void> dao = DaoManager.createDao(connectionSource, Bogus.class);
            UpgradeManager ug = new UpgradeManager(dao);
            ug
                    .add(new TableUpgradeSpec("store")
                            .addColumnSpec(new ColumnUpgradeSpec("storeId", "VARCHAR").isKey(true))
                            .addColumnSpec(new ColumnUpgradeSpec("name", "VARCHAR"))
                            .addColumnSpec(new ColumnUpgradeSpec("latitude", "REAL"))
                            .addColumnSpec(new ColumnUpgradeSpec("longitude", "REAL"))
                            .addColumnSpec(new ColumnUpgradeSpec("radius", "REAL"))
                            .addColumnSpec(new ColumnUpgradeSpec("sortOracleVersion", "INTEGER").isNew(true))
                    )
                    .add(new TableUpgradeSpec("shoppingList")
                            .addColumnSpec(new ColumnUpgradeSpec("shoppingListId", "VARCHAR").isKey(true))
                            .addColumnSpec(new ColumnUpgradeSpec("name", "VARCHAR"))
                            .addColumnSpec(new ColumnUpgradeSpec("isAutoSelectStore", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("storeId", "VARCHAR").foreignKey("store", "storeId").isNew(true))
                    )
                    .add(new TableUpgradeSpec("itemInstance")
                            .addColumnSpec(new ColumnUpgradeSpec("itemInstanceId", "VARCHAR").isKey(true))
                            .addColumnSpec(new ColumnUpgradeSpec("itemId", "VARCHAR").foreignKey("item", "itemId")) // LINK
                            .addColumnSpec(new ColumnUpgradeSpec("shoppingListId", "VARCHAR").foreignKey("shoppingList", "shoppingListId").newIsIndex(true))

                            .addColumnSpec(new ColumnUpgradeSpec("hasCount", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("count", "INTEGER"))

                            .addColumnSpec(new ColumnUpgradeSpec("hasVolume", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("volumeUnitId", "VARCHAR").foreignKey("volumeUnit", "volumeUnitId")) // LINK
                            .addColumnSpec(new ColumnUpgradeSpec("volumeMagnitude", "NUMERIC"))

                            .addColumnSpec(new ColumnUpgradeSpec("hasWeight", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("weightUnitId", "VARCHAR").foreignKey("weightUnit", "weightUnitId")) // LINK
                            .addColumnSpec(new ColumnUpgradeSpec("weightMagnitude", "NUMERIC"))

                            .addColumnSpec(new ColumnUpgradeSpec("pickupIndex", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("pickupStoreId", "VARCHAR").foreignKey("store", "storeId")) // LINK
                    )
                    .add(new TableUpgradeSpec("volumeUnit")
                            .addColumnSpec(new ColumnUpgradeSpec("volumeUnitId", "VARCHAR").isKey(true))
                            .addColumnSpec(new ColumnUpgradeSpec("name", "TEXT"))
                            .addColumnSpec(new ColumnUpgradeSpec("milliliters", "REAL"))
                    )
                    .add(new TableUpgradeSpec("weightUnit")
                            .addColumnSpec(new ColumnUpgradeSpec("weightUnitId", "VARCHAR").isKey(true))
                            .addColumnSpec(new ColumnUpgradeSpec("name", "TEXT"))
                            .addColumnSpec(new ColumnUpgradeSpec("grams", "REAL"))
                    )
                    .add(new TableUpgradeSpec("itemInstanceAttribute")
                            .addColumnSpec(new ColumnUpgradeSpec("itemInstanceAttributeId", "VARCHAR").isKey(true))
                            .addColumnSpec(new ColumnUpgradeSpec("itemAttributeId", "VARCHAR").foreignKey("itemAttribute", "itemAttributeId").newIsIndex(true))
                            .addColumnSpec(new ColumnUpgradeSpec("itemInstanceId", "VARCHAR").foreignKey("itemInstance", "itemInstanceId").newIsIndex(true))
                    )
                    .add(new TableUpgradeSpec("itemAttribute")
                            .addColumnSpec(new ColumnUpgradeSpec("itemAttributeId", "VARCHAR").isKey(true))
                            .addColumnSpec(new ColumnUpgradeSpec("itemId", "VARCHAR").foreignKey("item", "itemId"))
                            .addColumnSpec(new ColumnUpgradeSpec("name", "TEXT"))
                            .addColumnSpec(new ColumnUpgradeSpec("isDefaultValue", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("isDefaultPopularity", "INTEGER"))
                    )
                    .add(new TableUpgradeSpec("item")
                            .addColumnSpec(new ColumnUpgradeSpec("itemId", "VARCHAR").isKey(true))
                            .addColumnSpec(new ColumnUpgradeSpec("name", "TEXT"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultHasCountValue", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultHasCountPopularity", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultCountValue", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultCountPopularity", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultHasVolumeValue", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultHasVolumePopularity", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultVolumeUnitValueId", "VARCHAR").foreignKey("volumeUnit", "volumeUnitId"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultVolumeUnitPopularity", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultVolumeMagnitudeValue", "REAL"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultVolumeMagnitudePopularity", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultHasWeightValue", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultHasWeightPopularity", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultWeightUnitValueId", "VARCHAR").foreignKey("weightUnit", "weightUnitId"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultWeightUnitPopularity", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultWeightMagnitudeValue", "REAL"))
                            .addColumnSpec(new ColumnUpgradeSpec("defaultWeightMagnitudePopularity", "INTEGER"))
                    )
                    .add(new TableUpgradeSpec("storeItem")
                            .addColumnSpec(new ColumnUpgradeSpec("storeItemId", "VARCHAR").isKey(true))
                            .addColumnSpec(new ColumnUpgradeSpec("itemId", "VARCHAR").foreignKey("item", "itemId"))
                            .addColumnSpec(new ColumnUpgradeSpec("storeId", "VARCHAR").foreignKey("store", "storeId"))
                            .addColumnSpec(new ColumnUpgradeSpec("precalculatedSortIndex", "INTEGER"))
                            .addColumnSpec(new ColumnUpgradeSpec("unitPrice", "REAL"))
                    )
                    .add(new TableUpgradeSpec("storeItemEdge")
                            .addColumnSpec(new ColumnUpgradeSpec("storeItemEdgeId", "VARCHAR").isKey(true))
                            .addColumnSpec(new ColumnUpgradeSpec("tailStoreItemId", "VARCHAR").foreignKey("storeItem", "storeItemId"))
                            .addColumnSpec(new ColumnUpgradeSpec("headStoreItemId", "VARCHAR").foreignKey("storeItem", "storeItemId"))
                            .addColumnSpec(new ColumnUpgradeSpec("weight", "REAL").isObsolete(true))
                    )
                    .add(new TableUpgradeSpec("settings")
                            .addColumnSpec(new ColumnUpgradeSpec("settingsId", "VARCHAR").isKey(true))
                    )
            ;

            ug.upgrade();

            TableUtils.createTable(connectionSource, ItemPurchaseEvent.class);

            // Fix Item defaults (i.e. that are not set).
            String sqlStatement;
            GenericRawResults<Object[]> rawResults;
            String flozId = "";
            String lbId = "";

            sqlStatement = "UPDATE item SET defaultCountPopularity=1, defaultCountValue=1 WHERE defaultCountPopularity=0;";
            Log.d(TAG, sqlStatement);
            dao.executeRaw(sqlStatement);

            sqlStatement = "UPDATE item SET defaultHasCountPopularity=1, defaultHasCountValue=0 WHERE defaultHasCountPopularity=0;";
            Log.d(TAG, sqlStatement);
            dao.executeRaw(sqlStatement);

            sqlStatement = "UPDATE item SET defaultHasVolumePopularity=1, defaultHasVolumeValue=0 WHERE defaultHasVolumePopularity=0;";
            Log.d(TAG, sqlStatement);
            dao.executeRaw(sqlStatement);

            sqlStatement = "UPDATE item SET defaultHasWeightPopularity=1, defaultHasWeightValue=0, defaultHasVolumeValue=0 WHERE defaultHasWeightPopularity=0;";
            Log.d(TAG, sqlStatement);
            dao.executeRaw(sqlStatement);

            sqlStatement = "UPDATE item SET defaultVolumeMagnitudePopularity=1, defaultVolumeMagnitudeValue=1 WHERE defaultVolumeMagnitudePopularity=0;";
            Log.d(TAG, sqlStatement);
            dao.executeRaw(sqlStatement);

            {
                sqlStatement = "SELECT DISTINCT volumeUnitId FROM volumeUnit WHERE name='fl oz';";
                //Log.d(TAG, sqlStatement);
                rawResults = dao.queryRaw(sqlStatement, new DataType[]{DataType.STRING});
                for (Object[] resultArray : rawResults) {
                    flozId = (String) resultArray[0];
                    sqlStatement = "UPDATE item SET defaultVolumeUnitPopularity=1, defaultVolumeUnitValueId='" + flozId + "' WHERE defaultVolumeUnitPopularity=0;";
                    Log.d(TAG, sqlStatement);
                    dao.executeRaw(sqlStatement);
                }
            }

            sqlStatement = "UPDATE item SET defaultWeightMagnitudePopularity=1, defaultWeightMagnitudeValue=1 WHERE defaultWeightMagnitudePopularity=0;";
            Log.d(TAG, sqlStatement);
            dao.executeRaw(sqlStatement);

            {
                sqlStatement = "SELECT DISTINCT weightUnitId FROM weightUnit WHERE name='lb';";
                //Log.d(TAG, sqlStatement);
                rawResults = dao.queryRaw(sqlStatement, new DataType[]{DataType.STRING});
                for (Object[] resultArray : rawResults) {
                    lbId = (String) resultArray[0];
                    sqlStatement = "UPDATE item SET defaultWeightUnitPopularity=1, defaultWeightUnitValueId='" + lbId + "' WHERE defaultWeightUnitPopularity=0;";
                    Log.d(TAG, sqlStatement);
                    dao.executeRaw(sqlStatement);
                }
            }

            // Fix ItemInstance values (e.g. volume/weight magnitudes == 0, volume/weight units == null).
            sqlStatement = "UPDATE itemInstance SET count=1 WHERE count=0;";
            Log.d(TAG, sqlStatement);
            dao.executeRaw(sqlStatement);

            sqlStatement = "UPDATE itemInstance SET volumeMagnitude=1 WHERE volumeMagnitude=0;";
            Log.d(TAG, sqlStatement);
            dao.executeRaw(sqlStatement);

            sqlStatement = "UPDATE itemInstance SET volumeUnitId='" + flozId + "' WHERE volumeUnitId is null;";
            Log.d(TAG, sqlStatement);
            dao.executeRaw(sqlStatement);

            sqlStatement = "UPDATE itemInstance SET weightMagnitude=1 WHERE weightMagnitude=0;";
            Log.d(TAG, sqlStatement);
            dao.executeRaw(sqlStatement);

            sqlStatement = "UPDATE itemInstance SET weightUnitId='" + lbId + "' WHERE weightUnitId is null;";
            Log.d(TAG, sqlStatement);
            dao.executeRaw(sqlStatement);

        } catch (SQLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }
}
