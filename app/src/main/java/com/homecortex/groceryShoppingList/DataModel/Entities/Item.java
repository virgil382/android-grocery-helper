// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Entities;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.ChildEntityCollectionManager;
import com.homecortex.groceryShoppingList.DataModel.DefaultValueTracker;
import com.homecortex.groceryShoppingList.DataModel.Entities.Helpers.ItemPurchaseFrequencyCalculator;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemAttributeExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.StoreItemExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.VolumeUnitExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.WeightUnitExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IChildEntity;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntityCollectionQueryResultHandler;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.Util.ObjectUtils;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * An <code>Item</code> represents an abstract item (i.e. a concrete item
 * but without specific attributes such as color, weight, volume, count).
 * An <code>Item</code> maintains a set of default values for some attributes
 * so that new {@link ItemInstance}s referencing the <code>Item</code> may
 * set their attributes to popular values (i.e. that are often set by the user).
 */
@DatabaseTable(tableName = "item")
public class Item implements IChildEntity {
	/// ORMLite needs a parameterless constructor.
	public Item() { }

	public Item(String name) {
		this.name = name;
		ItemExtent.instance().create(this);
	}

	private boolean m_isOpen = false;
	@Override
	public void open() {
		if (m_isOpen) return;
		m_isOpen = true;
		//m_childStoreItemsManager.open();
		//m_childItemInstancesManager.open();
		//m_childItemAttributesManager.open();
		//m_volumeUnitAutoLoader.open();
		//m_weightUnitAutoLoader.open();
	}
	@Override
	public void close() {
		if (!m_isOpen) return;
		m_isOpen = false;
		m_childStoreItemsManager.close();
		//m_childItemInstancesManager.close();
		m_childItemAttributesManager.close();
		//m_volumeUnitAutoLoader.close();
		//m_weightUnitAutoLoader.close();
	}

	@Override
	public void delete() {
		ItemExtent.instance().delete(this);
	}

	@Override
	public void setParent(String foreignKeyName, IEntity parent) { }
	
	@DatabaseField(generatedId = true)
	private UUID itemId;
	public UUID getId() { return itemId; }
	public void setId(UUID newValue) {
		if(ObjectUtils.equals(newValue, itemId)) return;
		itemId = newValue;
		ItemExtent.instance().update(this, new UpdateSet("itemId"));
	}
	
	@DatabaseField
	private String name;
	public String getName() { return name; }
	public void setName(String newValue) {
		if(ObjectUtils.equals(newValue, name)) return;
		name = newValue;
		ItemExtent.instance().update(this, new UpdateSet("name"));
	}

	// Fields used to track the default value of hasCount to be assigned to new ItemInstance.
	@DatabaseField
	private Boolean defaultHasCountValue;
	public Boolean getDefaultHasCountValue() {
		if(defaultHasCountValue == null) return(false);
		return(defaultHasCountValue);
	}

	@DatabaseField
	private Integer defaultHasCountPopularity;
	protected DefaultValueTracker<Boolean> m_defaultHasCountTracker = new DefaultValueTracker<>(this, "defaultHasCountValue", "defaultHasCountPopularity", 3);
	private void proposeDefaultHasCountValue(boolean hasCount) {
		Boolean previousValue = defaultHasCountValue;
		Integer previousPopularity = defaultHasCountPopularity;
		
		m_defaultHasCountTracker.proposeDefaultValue(hasCount);

		if(!ObjectUtils.equals(defaultHasCountValue, previousValue)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultHasCountValue"));
		}
		if(!ObjectUtils.equals(defaultHasCountPopularity, previousPopularity)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultHasCountPopularity"));
		}
	}

	// Fields used to track the default value of count to be assigned to new ItemInstances
	@DatabaseField
	private Integer defaultCountValue;
	public Integer getDefaultCountValue() {
		// If no sane value has been set, then pick one.
		if (defaultCountValue == null) {
			defaultCountValue = 1;
			proposeDefaultCountValue(defaultCountValue);
		}
		return(defaultCountValue);
	}

	@DatabaseField
	private Integer defaultCountPopularity;
	//public Integer getDefaultCountPopularity() { return(defaultCountPopularity); }
	protected DefaultValueTracker<Integer> m_defaultCountTracker = new DefaultValueTracker<>(this, "defaultCountValue", "defaultCountPopularity", 1);
	private void proposeDefaultCountValue(int count) {
		Integer previousValue = defaultCountValue;
		Integer previousPopularity = defaultCountPopularity;
		
		m_defaultCountTracker.proposeDefaultValue(count);

		if(!ObjectUtils.equals(defaultCountValue, previousValue)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultCountValue"));
		}
		if(!ObjectUtils.equals(defaultCountPopularity, previousPopularity)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultCountPopularity"));
		}
	}

	// Fields used to track the default value of hasVolume to be assigned to new ItemInstance.
	@DatabaseField
	private Boolean defaultHasVolumeValue;
	public Boolean getDefaultHasVolumeValue() {
		if (defaultHasVolumeValue == null) return(false);
		return(defaultHasVolumeValue);
	}

	@DatabaseField
	private Integer defaultHasVolumePopularity;
	//public Integer getDefaultHasVolumePopularity() { return(defaultHasVolumePopularity); }
	protected DefaultValueTracker<Boolean> m_defaultHasVolumeTracker = new DefaultValueTracker<>(this, "defaultHasVolumeValue", "defaultHasVolumePopularity", 3);
	private void proposeDefaultHasVolumeValue(boolean hasVolume) {
		Boolean previousValue = defaultHasVolumeValue;
		Integer previousPopularity = defaultHasVolumePopularity;
		
		m_defaultHasVolumeTracker.proposeDefaultValue(hasVolume);

		if(!ObjectUtils.equals(defaultHasVolumeValue, previousValue)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultHasVolumeValue"));
		}
		if(!ObjectUtils.equals(defaultHasVolumePopularity, previousPopularity)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultHasVolumePopularity"));
		}
	}
	
	// Fields used to track the default value of volumeUnit to be assigned to new ItemInstance.
	@DatabaseField(canBeNull = true)
	private UUID defaultVolumeUnitValueId;
	public UUID getDefaultVolumeUnitValueId() {
		return(defaultVolumeUnitValueId);
	}
	//private EntityAutoLoader<Item, VolumeUnit> m_volumeUnitAutoLoader =
	//		new EntityAutoLoader<Item, VolumeUnit>(this, "defaultVolumeUnitValueId", ItemExtent.instance(), VolumeUnitExtent.instance());
	//public VolumeUnit getDefaultVolumeUnitValue() { return(m_volumeUnitAutoLoader.getEntity()); }

	@DatabaseField
	private Integer defaultVolumeUnitPopularity;
	protected DefaultValueTracker<UUID> m_defaultVolumeUnitTracker = new DefaultValueTracker<>(this, "defaultVolumeUnitValueId", "defaultVolumeUnitPopularity", 3);
	private void proposeDefaultVolumeUnitValue(UUID volumeUnitId) {
		UUID previousValue = defaultVolumeUnitValueId;
		Integer previousPopularity = defaultVolumeUnitPopularity;

		m_defaultVolumeUnitTracker.proposeDefaultValue(volumeUnitId);

		if(!ObjectUtils.equals(defaultVolumeUnitValueId, previousValue)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultVolumeUnitValueId"));
		}
		if(!ObjectUtils.equals(defaultVolumeUnitPopularity, previousPopularity)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultVolumeUnitPopularity"));
		}
	}

	// Fields used to track the default value of volumeMagnitude to be assigned to new ItemInstance.
	@DatabaseField
	private Float defaultVolumeMagnitudeValue;
	public float getDefaultVolumeMagnitudeValue() {
		if (defaultVolumeMagnitudeValue == null) return(0.0f);
		return(defaultVolumeMagnitudeValue);
	}

	@DatabaseField
	private Integer defaultVolumeMagnitudePopularity;
	protected DefaultValueTracker<Float> m_defaultVolumeMagnitudeTracker = new DefaultValueTracker<>(this, "defaultVolumeMagnitudeValue", "defaultVolumeMagnitudePopularity", 3);
	private void proposeDefaultVolumeMagnitudeValue(float volumeMagnitude) {
		Float previousValue = defaultVolumeMagnitudeValue;
		Integer previousPopularity = defaultVolumeMagnitudePopularity;

		m_defaultVolumeMagnitudeTracker.proposeDefaultValue(volumeMagnitude);

		if(!ObjectUtils.equals(defaultVolumeMagnitudeValue, previousValue)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultVolumeMagnitudeValue"));
		}
		if(!ObjectUtils.equals(defaultVolumeMagnitudePopularity, previousPopularity)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultVolumeMagnitudePopularity"));
		}
	}

	// Fields used to track the default value of hasWeight to be assigned to new ItemInstance.
	@DatabaseField
	private Boolean defaultHasWeightValue;
	public Boolean getDefaultHasWeightValue() {
		if (defaultHasWeightValue == null) return(false);
		return(defaultHasWeightValue);
	}

	@DatabaseField
	private Integer defaultHasWeightPopularity;
	protected DefaultValueTracker<Boolean> m_defaultHasWeightTracker = new DefaultValueTracker<>(this, "defaultHasWeightValue", "defaultHasWeightPopularity", 3);
	private void proposeDefaultHasWeightValue(boolean hasVolume) {
		Boolean previousValue = defaultHasWeightValue;
		Integer previousPopularity = defaultHasWeightPopularity;

		m_defaultHasWeightTracker.proposeDefaultValue(hasVolume);

		if(!ObjectUtils.equals(defaultHasWeightValue, previousValue)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultHasWeightValue"));
		}
		if(!ObjectUtils.equals(defaultHasWeightPopularity, previousPopularity)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultHasWeightPopularity"));
		}
	}

	// Fields used to track the default value of weightUnit to be assigned to new ItemInstance.
	@DatabaseField(canBeNull = true)
	private UUID defaultWeightUnitValueId;
	public UUID getDefaultWeightUnitValueId() { return(defaultWeightUnitValueId); }
	//private EntityAutoLoader<Item, WeightUnit> m_weightUnitAutoLoader =
	//		new EntityAutoLoader<Item, WeightUnit>(this, "defaultWeightUnitValueId", ItemExtent.instance(), WeightUnitExtent.instance());
	//public WeightUnit getDefaultWeightUnitValue() { return(m_weightUnitAutoLoader.getEntity()); }

	@DatabaseField
	private Integer defaultWeightUnitPopularity;
	protected DefaultValueTracker<UUID> m_defaultWeightUnitTracker = new DefaultValueTracker<>(this, "defaultWeightUnitValueId", "defaultWeightUnitPopularity", 3);
	private void proposeDefaultWeightUnitValue(UUID volumeUnitId) {
		UUID previousValue = defaultWeightUnitValueId;
		Integer previousPopularity = defaultWeightUnitPopularity;

		m_defaultWeightUnitTracker.proposeDefaultValue(volumeUnitId);

		if(!ObjectUtils.equals(defaultWeightUnitValueId, previousValue)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultWeightUnitValueId"));
		}
		if(!ObjectUtils.equals(defaultWeightUnitPopularity, previousPopularity)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultWeightUnitPopularity"));
		}
	}

	// Fields used to track the default value of weightMagnitude to be assigned to new ItemInstance.
	@DatabaseField
	private Float defaultWeightMagnitudeValue;
	public float getDefaultWeightMagnitudeValue() {
		if (defaultWeightMagnitudeValue == null) return(0.0f);
		return(defaultWeightMagnitudeValue);
	}

	@DatabaseField
	public Integer defaultWeightMagnitudePopularity;
	protected DefaultValueTracker<Float> m_defaultWeightMagnitudeTracker = new DefaultValueTracker<>(this, "defaultWeightMagnitudeValue", "defaultWeightMagnitudePopularity", 3);
	private void proposeDefaultWeightMagnitudeValue(float weightMagnitude) {
		Float previousValue = defaultWeightMagnitudeValue;
		Integer previousPopularity = defaultWeightMagnitudePopularity;

		m_defaultWeightMagnitudeTracker.proposeDefaultValue(weightMagnitude);

		if(!ObjectUtils.equals(defaultWeightMagnitudeValue, previousValue)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultWeightMagnitudeValue"));
		}
		if(!ObjectUtils.equals(defaultWeightMagnitudePopularity, previousPopularity)) {
			ItemExtent.instance().update(this, new UpdateSet("defaultWeightMagnitudePopularity"));
		}
	}

	/**
	 * Propose new default values and update their popularity counts.
	 * 
	 * @param itemInstance The ItemInstance from which the proposed default values are retrieved. 
	 * @throws SQLException
	 */
	public void proposeDefaultValues(ItemInstance itemInstance) throws SQLException {
		proposeDefaultHasCountValue(itemInstance.getHasCount());
		proposeDefaultCountValue(itemInstance.getCount());
		proposeDefaultHasVolumeValue(itemInstance.getHasVolume());
		proposeDefaultVolumeUnitValue(itemInstance.getVolumeUnitId());
		proposeDefaultVolumeMagnitudeValue(itemInstance.getVolumeMagnitude());
		proposeDefaultHasWeightValue(itemInstance.getHasWeight());
		proposeDefaultWeightUnitValue(itemInstance.getWeightUnitId());
		proposeDefaultWeightMagnitudeValue(itemInstance.getWeightMagnitude());

		// Propose default values for ItemAttributes.
		
		// This Collection initially holds all ItemAttribute (both default and non-default).
		Collection<ItemAttribute> allItemAttributes = new ArrayList<>();
		allItemAttributes.addAll(getItemAttributes());

		// Propose as default each ItemAttribute for which the ItemInstance holds a corresponding
		// ItemInstanceAttributes and remove them from the Collection.
		for(ItemInstanceAttribute itemInstanceAttribute : itemInstance.getItemInstanceAttributes()) {
			ItemAttribute itemAttribute = itemInstanceAttribute.getItemAttribute();
			itemAttribute.proposeIsDefaultValue(true);
			allItemAttributes.remove(itemAttribute);
		}

		// Propose as non-default the remaining ItemAttribute in the Collection.
		for (ItemAttribute itemAttribute : allItemAttributes) {
			itemAttribute.proposeIsDefaultValue(false);
		}
	}
	
	///////////////// StoreItems
	private ChildEntityCollectionManager<StoreItem> m_childStoreItemsManager =
			new ChildEntityCollectionManager<>(this, "itemId", StoreItemExtent.instance());
	public Collection<StoreItem> getStoreItems() {
		m_childStoreItemsManager.open();
		return(m_childStoreItemsManager.getCollection());
	}
	public StoreItem getStoreItem(UUID storeId) {
		for(StoreItem storeItem : getStoreItems()) {
			if (storeItem.getStoreId().equals(storeId)) return(storeItem);
		}
		return(null);
	}
	
	///////////////// ItemAttributes
	private ChildEntityCollectionManager<ItemAttribute> m_childItemAttributesManager =
			new ChildEntityCollectionManager<>(this, "itemId", ItemAttributeExtent.instance());
	public Collection<ItemAttribute> getItemAttributes() {
        m_childItemAttributesManager.open();
        return(m_childItemAttributesManager.getCollection());
    }

    /**
     * Set the initial default values into a newly constructed Item (i.e. that does not exist in
     * the database).  Arguably, this code belongs in the non-default constructor (which is not
     * used by the ORM framework).  But it is considered bad form to execute extensive (and
     * potentially slow) logic in constructors.
     */
    public void setDefaults()
    {
        defaultHasCountValue = false;
        defaultHasCountPopularity = 1;

        defaultCountValue = 1;
        defaultCountPopularity = 1;

        defaultHasVolumeValue = false;
        defaultHasVolumePopularity = 1;

        // Set defaultVolumeUnitValueId in a roundabout way
        VolumeUnitExtent.instance().getByFieldValue("name", "fl oz", new IEntityCollectionQueryResultHandler<VolumeUnit>() {
            @Override
            public void onResult(Collection<VolumeUnit> objects) {
                defaultVolumeUnitValueId = objects.iterator().next().getId();
            }
        });
        defaultVolumeUnitPopularity = 1;

        defaultVolumeMagnitudeValue = 1.0F;
        defaultVolumeMagnitudePopularity = 1;

        defaultHasWeightValue = false;
        defaultHasWeightPopularity = 1;

        // Set defaultWeightUnitValueId in a roundabout way
        WeightUnitExtent.instance().getByFieldValue("name", "lb", new IEntityCollectionQueryResultHandler<WeightUnit>() {
            @Override
            public void onResult(Collection<WeightUnit> objects) {
                defaultWeightUnitValueId = objects.iterator().next().getId();
            }
        });
        defaultWeightUnitPopularity = 1;

        defaultWeightMagnitudeValue = 1.0F;
        defaultWeightMagnitudePopularity = 1;
    }

	public static Item getItem(String name) throws SQLException
	{
		Dao<Item, UUID> classDao = ApplicationPm.getDatabaseHelper().getItemDao();
		List<Item> items = classDao.queryForEq("name", name);
		if (items.size() == 0) return null;
		return items.get(0);
	}
	
	/**
	 * Represent an <code>Item</code> as a human-readable String.
	 * @return A human-readable representation of this <code>Item</code>.
	 */
	@Override
	public String toString() { return getName(); }

	/**
	 * Get the lowest price of any StoreItem associated with this Item.
	 * @return The lowest price.
	 */
	public Double getLowestPrice() {
		Double lowestPrice = Double.MAX_VALUE;
		Collection<StoreItem> storeItems = getStoreItems();
		for(StoreItem storeItem : storeItems) {
			Double price = storeItem.getUnitPrice();
			if (price == null) continue;
			if (price < lowestPrice) lowestPrice = price;
		}
		if (lowestPrice == Double.MAX_VALUE) return null;

		return (lowestPrice);
	}

    private ItemPurchaseFrequencyCalculator m_itemPurchaseFrequencyCalculator;
    public Integer getPurchaseFrequency() {
        if(m_itemPurchaseFrequencyCalculator == null) {
            m_itemPurchaseFrequencyCalculator = new ItemPurchaseFrequencyCalculator(getId());
            m_itemPurchaseFrequencyCalculator.open();
        }

        return(m_itemPurchaseFrequencyCalculator.getPurchaseFrequency());
    }
}
