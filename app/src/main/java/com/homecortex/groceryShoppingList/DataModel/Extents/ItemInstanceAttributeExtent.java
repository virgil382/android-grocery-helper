// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Extents;

import java.sql.SQLException;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstanceAttribute;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.j256.ormlite.dao.Dao;

public class ItemInstanceAttributeExtent extends EntityService<ItemInstanceAttribute> {
	private static ItemInstanceAttributeExtent instance;
	public static ItemInstanceAttributeExtent instance() {
		if (instance == null) instance = new ItemInstanceAttributeExtent();
		return instance;
	}
	
	// The DB connection.
	Dao<ItemInstanceAttribute, UUID> classDao;
	@Override
	public Dao<ItemInstanceAttribute, UUID> classDao() {
		if (classDao != null) return classDao;
		classDao = ApplicationPm.getDatabaseHelper().getItemInstanceAttributeDao();
		return classDao;
	}

	@Override
	public void delete(ItemInstanceAttribute itemInstanceAttribute) {
		try {
			classDao().delete(itemInstanceAttribute);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		notifyDelete(itemInstanceAttribute);
	}
}
