// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.Item;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemAttribute;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstance;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstanceAttribute;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemPurchaseEvent;
import com.homecortex.groceryShoppingList.DataModel.Entities.Settings;
import com.homecortex.groceryShoppingList.DataModel.Entities.ShoppingList;
import com.homecortex.groceryShoppingList.DataModel.Entities.Store;
import com.homecortex.groceryShoppingList.DataModel.Entities.StoreItem;
import com.homecortex.groceryShoppingList.DataModel.Entities.StoreItemEdge;
import com.homecortex.groceryShoppingList.DataModel.Entities.VolumeUnit;
import com.homecortex.groceryShoppingList.DataModel.Entities.WeightUnit;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IDatabaseHelper;
import com.homecortex.groceryShoppingList.DataModel.Upgrade.DbUpgradeStrategies.DbUpgrade2to4;
import com.homecortex.groceryShoppingList.DataModel.Upgrade.DbUpgradeStrategies.DbUpgrade3to4;
import com.homecortex.groceryShoppingList.DataModel.Upgrade.DbUpgradeStrategies.IDbUpgradeStrategy;
import com.homecortex.groceryShoppingList.DataModel.Upgrade.ShortestPathCalculator.ShortestPathCalculator;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * A DatabaseHelper is an IDatabaseHelper that contains the application's database-specifics details
 * such as the database file name and the methods for creating, destroying, and updating the database.
 * It also contains methods for getting the DAOs of the application's database entity classes.  Each
 * DAO may be used to perform CRUD operations on it's corresponding database table.
 */
public class DatabaseHelper implements IDatabaseHelper
{
	protected ConnectionSource m_connectionSource;
	
	private InitialDatabaseContentsBuilder m_initialDatabaseContentsBuilder = new InitialDatabaseContentsBuilder();
	
	// The name of the database file.
	public static final String DATABASE_NAME = "shoppingHelper.db";
	public static final int CURRENT_DATABASE_VERSION = 4;

	// The DAOs used to access the database tables.
	private Dao<ShoppingList, UUID> m_shoppingListDao = null;
	private Dao<WeightUnit, UUID> m_weightUnitDao = null;
	private Dao<VolumeUnit, UUID> m_volumeUnitDao = null;
	private Dao<Item, UUID> m_itemDao = null;
	private Dao<ItemInstance, UUID> m_itemInstanceDao = null;
	private Dao<ItemAttribute, UUID> m_itemAttributeDao = null;
	private Dao<ItemInstanceAttribute, UUID> m_itemInstanceAttributeDao = null;
	private Dao<Store, UUID> m_storeDao = null;
	private Dao<StoreItem, UUID> m_storeItemDao = null;
	private Dao<StoreItemEdge, UUID> m_storeItemEdgeDao = null;
	private Dao<Settings, UUID> m_settingsDao = null;
	private Dao<ItemPurchaseEvent, UUID> m_itemPurchaseEventDao = null;

	public DatabaseHelper(ConnectionSource connectionSource)
	{
		m_connectionSource = connectionSource;
	}

	public ConnectionSource getConnectionSource() {
		return m_connectionSource;
	}

	public void destroy(ConnectionSource connectionSource)
	{
		try {
			TableUtils.dropTable(connectionSource, ShoppingList.class, true);
			TableUtils.dropTable(connectionSource, WeightUnit.class, true);
			TableUtils.dropTable(connectionSource, VolumeUnit.class, true);
			TableUtils.dropTable(connectionSource, Item.class, true);
			TableUtils.dropTable(connectionSource, ItemAttribute.class, true);
			TableUtils.dropTable(connectionSource, ItemInstanceAttribute.class, true);
			TableUtils.dropTable(connectionSource, ItemInstance.class, true);
			TableUtils.dropTable(connectionSource, Store.class, true);
			TableUtils.dropTable(connectionSource, StoreItem.class, true);
			TableUtils.dropTable(connectionSource, StoreItemEdge.class, true);
			TableUtils.dropTable(connectionSource, Settings.class, true);
			TableUtils.dropTable(connectionSource, ItemPurchaseEvent.class, true);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void create(ConnectionSource connectionSource)
	{
		try {
			TableUtils.createTable(connectionSource, ShoppingList.class);
			TableUtils.createTable(connectionSource, WeightUnit.class);
			TableUtils.createTable(connectionSource, VolumeUnit.class);
			TableUtils.createTable(connectionSource, Item.class);
			TableUtils.createTable(connectionSource, ItemAttribute.class);
			TableUtils.createTable(connectionSource, ItemInstanceAttribute.class);
			TableUtils.createTable(connectionSource, ItemInstance.class);
			TableUtils.createTable(connectionSource, Store.class);
			TableUtils.createTable(connectionSource, StoreItem.class);
			TableUtils.createTable(connectionSource, StoreItemEdge.class);
			TableUtils.createTable(connectionSource, Settings.class);
			TableUtils.createTable(connectionSource, ItemPurchaseEvent.class);

			if (m_initialDatabaseContentsBuilder != null) m_initialDatabaseContentsBuilder.Build();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// Upgrade the current DB schema one version at a time starting from current version + 1.
	public void performUpgrades(ConnectionSource connectionSource, int oldVersion, int newVersion)
	{
		// Use the ShortestPathCalculator to calculate an optimum upgrade path between oldVersion
        // and newVersion.  This is done in two phases:

        // Phase 1: Register available DbUpgradeStrategies as edges between DB versions in order
        // to build a graph that describes all available upgrade paths.
        ShortestPathCalculator<IDbUpgradeStrategy> spc = new ShortestPathCalculator<>();
        spc.addEdge(new DbUpgrade2to4());
        spc.addEdge(new DbUpgrade3to4());

        // Phase 2: Find the shortest path between oldVersion and newVersion.  The path consists
        // of appropriate DbUpgradeStrategies, so execute them.
        List<IDbUpgradeStrategy> upgradePath = spc.getShortestPath(oldVersion, newVersion);
        if(upgradePath != null) {
            // An upgrade path exists, so follow it by executing each IDbUpgradeStrategy.
            for (IDbUpgradeStrategy strategy : upgradePath) {
                strategy.doUpgrade(connectionSource);
            }
        } else {
            // No upgrade path exists, so recreate the database.
            destroy(connectionSource);
            create(connectionSource);
        }
	}

	public Dao<ShoppingList, UUID> getShoppingListDao()
	{
		if (m_shoppingListDao == null) {
			try {
				m_shoppingListDao = DaoManager.createDao(getConnectionSource(), ShoppingList.class);
				m_shoppingListDao.setObjectCache(true);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		return m_shoppingListDao;
	}
	
	public Dao<WeightUnit, UUID> getWeightUnitDao()
	{
		if (m_weightUnitDao == null) {
			try {
				m_weightUnitDao = DaoManager.createDao(getConnectionSource(), WeightUnit.class);
				m_weightUnitDao.setObjectCache(true);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		return m_weightUnitDao;
	}
	
	public Dao<VolumeUnit, UUID> getVolumeUnitDao()
	{
		if (m_volumeUnitDao == null) {
			try {
				m_volumeUnitDao = DaoManager.createDao(getConnectionSource(), VolumeUnit.class);
				m_volumeUnitDao.setObjectCache(true);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		return m_volumeUnitDao;
	}

	/**
	 * Get the DAO used to perform CRUD operations on the "item" table.
	 * @return The DAO.
	 */
	public Dao<Item, UUID> getItemDao() {
		if (m_itemDao == null) {
			try {
				m_itemDao = DaoManager.createDao(getConnectionSource(), Item.class);
				m_itemDao.setObjectCache(true);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		return m_itemDao;
	}

	/**
	 * Get the DAO used to perform CRUD operations on the "itemInstance" table.
	 * @return The DAO.
	 */
	public Dao<ItemInstance, UUID> getItemInstanceDao() {
		if (m_itemInstanceDao == null) {
			try {
				m_itemInstanceDao = DaoManager.createDao(getConnectionSource(), ItemInstance.class);
				m_itemInstanceDao.setObjectCache(true);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		return m_itemInstanceDao;
	}

	public Dao<ItemAttribute, UUID> getItemAttributeDao()
	{
		if (m_itemAttributeDao == null) {
			try {
				m_itemAttributeDao = DaoManager.createDao(getConnectionSource(), ItemAttribute.class);
				m_itemAttributeDao.setObjectCache(true);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		return m_itemAttributeDao;
	}

	public Dao<ItemInstanceAttribute, UUID> getItemInstanceAttributeDao()
	{
		if (m_itemInstanceAttributeDao == null) {
			try {
				m_itemInstanceAttributeDao = DaoManager.createDao(getConnectionSource(), ItemInstanceAttribute.class);
				m_itemInstanceAttributeDao.setObjectCache(true);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		return m_itemInstanceAttributeDao;
	}

	@Override
	public Dao<Store, UUID> getStoreDao() 
	{
		if (m_storeDao == null) {
			try {
				m_storeDao = DaoManager.createDao(getConnectionSource(), Store.class);
				m_storeDao.setObjectCache(true);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		return m_storeDao;
	}

	public Dao<StoreItem, UUID> getStoreItemDao()
	{
		if (m_storeItemDao == null) {
			try {
				m_storeItemDao = DaoManager.createDao(getConnectionSource(), StoreItem.class);
				m_storeItemDao.setObjectCache(true);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		return m_storeItemDao;
	}

	@Override
	public Dao<StoreItemEdge, UUID> getStoreItemEdgeDao() {
		if (m_storeItemEdgeDao == null) {
			try {
				m_storeItemEdgeDao = DaoManager.createDao(getConnectionSource(), StoreItemEdge.class);
				m_storeItemEdgeDao.setObjectCache(true);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		return m_storeItemEdgeDao;
	}

	@Override
	public Dao<Settings, UUID> getSettingsDao() {
		if (m_settingsDao == null) {
			try {
				m_settingsDao = DaoManager.createDao(getConnectionSource(), Settings.class);
				m_settingsDao.setObjectCache(true);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		return m_settingsDao;
	}

	@Override
	public Dao<ItemPurchaseEvent, UUID> getItemPurchaseEventDao() {
		if (m_itemPurchaseEventDao == null) {
			try {
				m_itemPurchaseEventDao = DaoManager.createDao(getConnectionSource(), ItemPurchaseEvent.class);
				m_itemPurchaseEventDao.setObjectCache(true);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		return m_itemPurchaseEventDao;
	}

	@Override
	public void close() {
		m_itemDao = null;
		m_itemInstanceDao = null;
	}
}
