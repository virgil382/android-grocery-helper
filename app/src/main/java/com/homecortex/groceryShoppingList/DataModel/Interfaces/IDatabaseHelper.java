// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Interfaces;

import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.Item;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemAttribute;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstance;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstanceAttribute;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemPurchaseEvent;
import com.homecortex.groceryShoppingList.DataModel.Entities.Settings;
import com.homecortex.groceryShoppingList.DataModel.Entities.ShoppingList;
import com.homecortex.groceryShoppingList.DataModel.Entities.Store;
import com.homecortex.groceryShoppingList.DataModel.Entities.StoreItem;
import com.homecortex.groceryShoppingList.DataModel.Entities.StoreItemEdge;
import com.homecortex.groceryShoppingList.DataModel.Entities.VolumeUnit;
import com.homecortex.groceryShoppingList.DataModel.Entities.WeightUnit;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;

public interface IDatabaseHelper 
{
	ConnectionSource getConnectionSource();
	void close();
	Dao<ShoppingList, UUID> getShoppingListDao();
	Dao<WeightUnit, UUID> getWeightUnitDao();
	Dao<VolumeUnit, UUID> getVolumeUnitDao();
	Dao<Item, UUID> getItemDao();
	Dao<ItemInstance, UUID> getItemInstanceDao();
	Dao<ItemAttribute, UUID> getItemAttributeDao();
	Dao<ItemInstanceAttribute, UUID> getItemInstanceAttributeDao();
	Dao<Store, UUID> getStoreDao();
	Dao<StoreItem, UUID> getStoreItemDao();
	Dao<StoreItemEdge, UUID> getStoreItemEdgeDao();
	Dao<Settings, UUID> getSettingsDao();
	Dao<ItemPurchaseEvent, UUID> getItemPurchaseEventDao();
}
