// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Entities;

import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.EntityAutoLoader;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemAttributeExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemInstanceAttributeExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IChildEntity;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "itemInstanceAttribute")
public class ItemInstanceAttribute implements IChildEntity
{
	/// ORMLite needs a parameterless constructor.
	public ItemInstanceAttribute() { }
	
	public ItemInstanceAttribute(UUID itemAttributeId, UUID itemInstanceId) {
		this.itemAttributeId = itemAttributeId;
		this.itemInstanceId = itemInstanceId;

		ItemInstanceAttributeExtent.instance().create(this);
	}

	private boolean m_isOpen = false;
	@Override
	public void open() {
		if (m_isOpen) return;
		m_isOpen = true;
        //m_itemAttributeAutoLoader.open();
	}
	@Override
	public void close() { 
		if (!m_isOpen) return;
		m_isOpen = false;
        m_itemAttributeAutoLoader.close();
	}

	@Override
	public void delete() {
		ItemInstanceAttributeExtent.instance().delete(this);
	}

	@Override
	public void setParent(String foreignKeyName, IEntity parent) { }

	@DatabaseField(generatedId = true)
	private UUID itemInstanceAttributeId;
	public UUID getId() { return itemInstanceAttributeId; }
	public void setId(UUID newId) { itemInstanceAttributeId = newId; }
	
	@DatabaseField(canBeNull = false, index = true)
	private UUID itemAttributeId;
	public UUID getItemAttributeId() { return(itemAttributeId); }
    private EntityAutoLoader<ItemInstanceAttribute, ItemAttribute> m_itemAttributeAutoLoader =
            new EntityAutoLoader<>(this, "itemAttributeId", ItemInstanceAttributeExtent.instance(), ItemAttributeExtent.instance());
    public ItemAttribute getItemAttribute() {
		m_itemAttributeAutoLoader.open();
		return(m_itemAttributeAutoLoader.getEntity());
	}

	@DatabaseField(canBeNull = false, index = true)
	private UUID itemInstanceId;
}
