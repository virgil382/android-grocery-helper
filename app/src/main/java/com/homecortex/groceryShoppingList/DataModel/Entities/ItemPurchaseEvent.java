package com.homecortex.groceryShoppingList.DataModel.Entities;

import com.homecortex.groceryShoppingList.DataModel.Extents.ItemPurchaseEventExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntity;
import com.homecortex.groceryShoppingList.Util.ObjectUtils;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@DatabaseTable(tableName = "itemPurchaseEvent")
public class ItemPurchaseEvent  implements IEntity {
    /// ORMLite needs a parameterless constructor.
    public ItemPurchaseEvent() { }

    public ItemPurchaseEvent(UUID itemId) {
        // Set the current time in GMT.
        java.util.Date date = new java.util.Date();
        this.purchaseTime = new Timestamp(date.getTime());

        // Set the Item ID.
        this.itemId = itemId;

        ItemPurchaseEventExtent.instance().create(this);
    }

    private boolean m_isOpen = false;
    @Override
    public void open() {
        if (m_isOpen) return;
        m_isOpen = true;
    }
    @Override
    public void close() {
        if (!m_isOpen) return;
        m_isOpen = false;
    }

    @Override
    public void delete() {
        ItemPurchaseEventExtent.instance().delete(this);
    }

    @DatabaseField(generatedId = true)
    private UUID itemPurchaseEventId;
    public UUID getId() { return itemPurchaseEventId; }
    public void setId(UUID newValue) {
        if(ObjectUtils.equals(newValue, itemPurchaseEventId)) return;
        itemPurchaseEventId = newValue;
        ItemPurchaseEventExtent.instance().update(this, new UpdateSet("itemPurchaseEventId"));
    }

    // The purchase time is persisted as milliseconds since epoch.
    @DatabaseField(canBeNull = false, dataType = DataType.DATE_LONG, index = true)
    Date purchaseTime;

    /// The Item that was purchased.
    @DatabaseField(canBeNull = false, index = true)
    UUID itemId;
    public UUID getItemId() { return(itemId); }
}
