// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.DataModel.Entities;

import java.util.HashMap;
import java.util.UUID;

/**
 * A StoreItemReachabilityOracle caches answers to the question "Is StoreItem X reachable
 * from StoreItem Y?"  The validity of its contents is ephemeral (e.g. while updating the
 * natural shopping order at a Store), so it is implemented as a singleton for pragmatic
 * reasons.  
 */
public class StoreItemReachabilityOracle {
	static StoreItemReachabilityOracle theInstance = new StoreItemReachabilityOracle();
	public static StoreItemReachabilityOracle instance() {
		return theInstance;
	}
	
	// The data structure used to answer the question.
	HashMap<UUID, HashMap<UUID, Boolean>> reachabilityOracle;

	/**
	 * Reset the contents of the StoreItemReachabilityOracle.  This relinquishes the
	 * reference to the previous contents, which will eventually free up memory.
	 */
	public void reset() {
		reachabilityOracle = new HashMap<>();
	}

	/**
	 * Ask the StoreItemReachabilityOracle if StoreItem X is reachable from StoreItem Y.
	 * @param X Node X.
	 * @param Y Node Y.
	 * @return null if the answer is unknown, otherwise a Boolean specifying the answer.
	 */
	public Boolean getIsNodeReachable(UUID X, UUID Y) {
		HashMap<UUID, Boolean> sourceNodesMap = reachabilityOracle.get(X);
		if (sourceNodesMap == null) return null;
		
		return sourceNodesMap.get(Y);
	}
	
	/**
	 * Save the answer to the question if StoreItem X is reachable from StoreItem Y.
	 * @param X Node X.
	 * @param Y Node Y.
	 * @param isReachable The answer to the question. 
	 */
	public void setIsNodeReachable(UUID X, UUID Y, boolean isReachable) {
		HashMap<UUID, Boolean> sourceNodesMap = reachabilityOracle.get(X);
		if (sourceNodesMap == null) {
			sourceNodesMap = new HashMap<>();
			reachabilityOracle.put(X, sourceNodesMap);
		}
		
		sourceNodesMap.put(Y, isReachable);
	}
}
