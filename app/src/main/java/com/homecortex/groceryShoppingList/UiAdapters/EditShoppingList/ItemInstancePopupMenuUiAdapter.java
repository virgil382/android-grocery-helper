// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList;

import java.sql.SQLException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.EditShoppingListPm;
import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.R;

/**
 * ItemInstancePopupMenuUiAdapter is a UiAdapter that displays a popup menu that allows the user
 * to initiate one of several activities related to an ItemInstance.  After he does, the popup
 * menu notifies the EditShoppingListPm which activity the user wishes to initiate.
 * 
 * Collaborators: EditShoppingListPm
 */
public class ItemInstancePopupMenuUiAdapter extends UiAdapter {

	public ItemInstancePopupMenuUiAdapter(UiPm correspondingPm) {
		super(correspondingPm);
	}

	private EditShoppingListPm getPm() {
		return (EditShoppingListPm) m_pm;
	}

	@Override
	public void displayUi() {
		Activity currentActivity = ApplicationPm.m_currentActivity; 

		// Determine if we should show the "Edit Price" menu item.
		boolean isPriceEditPossible;
		isPriceEditPossible = (getPm().getSelectedStoreName() != null);

		// Allocate the appropriate number of menu item entries.
		final CharSequence[] popupMenuItems;
		if (isPriceEditPossible)
			popupMenuItems = new CharSequence[5];
		else
			popupMenuItems = new CharSequence[4];
			
		// Build a "popup menu" with the names of the operations to perform on the selected ItemAttribute.
		popupMenuItems[0] = currentActivity.getResources().getText(R.string.edit_item_instance_attributes);
		popupMenuItems[1] = currentActivity.getResources().getText(R.string.edit_name);
		popupMenuItems[2] = currentActivity.getResources().getText(R.string.delete);
		popupMenuItems[3] = currentActivity.getResources().getText(R.string.delete_permanently);
		if (isPriceEditPossible)
			popupMenuItems[4] = currentActivity.getResources().getText(R.string.edit_price);
		
		// Display the AlertDialog containing the "popup menu" and set myself as dialog click listener.
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(currentActivity);
		dialogBuilder
			.setTitle(currentActivity.getResources().getText(R.string.popup_menu_title_item_instance_operations))
			.setItems(popupMenuItems, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					switch(id) {
					case 0:
						getPm().onEditSelectedItemInstanceAttributes();
						return;

					case 1:
						getPm().onEditSelectedItemInstanceName();
						return;
						
					case 2:
						try {
							getPm().onRemoveSelectedItemInstance();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return;

					case 3:
						try {
							getPm().onPermanentlyRemoveSelectedItemInstance();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return;

					case 4:
						getPm().onEditSelectedItemInstancePrice();
						return;

					}
				}
			 })
			.setCancelable(true);

		// Set the AlertDialog to be cancelable if the user clicks outside.
		AlertDialog dialog = dialogBuilder.create();
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}

	@Override
	public void closeUi() {
		throw new UnsupportedOperationException();
	}

}
