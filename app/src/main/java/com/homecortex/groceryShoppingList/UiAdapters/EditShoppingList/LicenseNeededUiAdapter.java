package com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.TextView;

import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.EditShoppingListPm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.Factory.UiAdapterFactory;
import com.homecortex.groceryShoppingList.Util.LicenseManager;
import com.homecortex.groceryShoppingList.R;

public class LicenseNeededUiAdapter extends UiAdapter {
	TextView m_textView;
	Activity m_currentActivity;

	public LicenseNeededUiAdapter(EditShoppingListPm correspondingPm) {
		super(correspondingPm);
	}

	private EditShoppingListPm getPm() {
		return (EditShoppingListPm) m_pm;
	}

	@Override
	public void displayUi() {
		m_currentActivity = ApplicationPm.m_currentActivity; 
        final LicenseManager licenseManager = LicenseManager.instance();

        // Prepare the TextView displayed in the AlertDialog.
        String msg;
		m_textView = new TextView(m_currentActivity);
        m_textView.setId(0);
        if (licenseManager.isInGracePeriod()) {
            String msgTemplate = m_currentActivity.getResources().getString(R.string.grace_period_message);
            msg = String.format(msgTemplate, licenseManager.getGraceUsagesCount());
        } else {
        	msg = m_currentActivity.getResources().getString(R.string.license_required_message);
        }
		m_textView.setText(msg);

        // Create the AlertDialog asking for the new name of a shopping list, and set myself
		// dialog click listener.
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(m_currentActivity);
		dialogBuilder
			.setTitle(m_currentActivity.getResources().getText(R.string.grace_period_title))
			.setView(m_textView)
			.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					
					// Display the UI for purchasing a license.
					licenseManager.initiateBuyLicense(m_currentActivity);
					
	                return;
				}
			 })
			.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					if (licenseManager.isInGracePeriod())
				        UiAdapterFactory.instance.getLearnShoppingOrderYesNoUiAdapter(getPm()).displayUi();
	                return;
				}
			 });
        final AlertDialog dialog = dialogBuilder.create();

        // Display the AlertDialog, and set focus to the EditText, and display the keyboard.
        dialog.show();
		m_textView.requestFocus();
	}

	@Override
	public void closeUi() {
		throw new UnsupportedOperationException();
	}
}
