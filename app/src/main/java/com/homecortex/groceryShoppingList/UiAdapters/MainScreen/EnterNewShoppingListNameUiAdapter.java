// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.MainScreen;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.MainScreenPm;
import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.R;

/**
 * EnterNewShoppingListNameUiAdapter is a UiAdapter that displays a dialog box that allows the user
 * to specify a the name of a new ShoppingList.  After he does, the dialog box notifies the
 * MainScreenPm of the new name.
 * 
 * Collaborators: MainScreenPm
 */
public class EnterNewShoppingListNameUiAdapter extends UiAdapter
{
	/**
	 * Construct an <code>EnterNewShoppingListNameUiAdapter</code>.
	 * @param correspondingPm The {@link MainScreenPm}.
	 */
	public EnterNewShoppingListNameUiAdapter(UiPm correspondingPm) {
		super(correspondingPm);
	}

	/**
	 * Get the {@link MainScreenPm} specified in the constructor.
	 * @return The {@link MainScreenPm} specified in the constructor.
	 */
	private MainScreenPm getPm() {
		return (MainScreenPm) m_pm;
	}

	/**
	 * Create and show an {@link AlertDialog} that displays a default name for a new
	 * ShoppingList, selects the entire text, displays a soft keyboard, and
	 * allows the user to edit it.  The {@link AlertDialog} also has an "OK" and a 
	 * "Cancel" button for which this method registers {@link DialogInterface.OnClickListener}s.
	 * 
	 * <p>If the user clicks the "OK" button, then the corresponding
	 * {@link DialogInterface.OnClickListener} tells the {@link MainScreenPm} to add a new
	 * ShoppingList with the name specified by the user, and closes the
	 * {@link AlertDialog}.
	 * 
	 * <p>If the user press the "Cancel" button, then the corresponding 
	 * {@link DialogInterface.OnClickListener} closes the {@link AlertDialog}.
	 */
	@Override
	public void displayUi() {
		final Activity currentActivity = ApplicationPm.m_currentActivity; 

		final EditText input = new EditText(currentActivity);
        input.setId(0);
        input.setText(currentActivity.getResources().getText(R.string.default_shopping_list_name));
        input.setSingleLine();
        input.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
            	input.selectAll();
            	input.post(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager inputMethodManager= (InputMethodManager) currentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(input, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
            }
        });
        
        // Display the AlertDialog asking for the new name of a shopping list, and set myself
		// dialog click listener.
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(currentActivity);
		dialogBuilder
			.setTitle(currentActivity.getResources().getText(R.string.popup_menu_title_edit_shopping_list_name))
			.setView(input)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// Get the name set by the user.
					String value = input.getText().toString();
					getPm().addAnotherShoppingList(value);
	                return;
				}
			 })
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
	                return;
				}
			 });
		
        input.requestFocus();
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
	}

	/**
	 * Not supported.
	 */
	@Override
	public void closeUi() {
		throw new UnsupportedOperationException();
	}

}
