// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.MainScreen;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.MainScreenPm;
import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.R;

/**
 * ShoppingListPopupMenuUiAdapter is a UiAdapter that displays a popup menu that allows the user
 * to initiate one of several activities related to an ShoppingList.  After he does, the popup
 * menu notifies the MainScreenPm which activity the user wishes to initiate.
 * 
 * Collaborators: MainScreenPm
 */
public class ShoppingListPopupMenuUiAdapter extends UiAdapter 
{
	public ShoppingListPopupMenuUiAdapter(UiPm correspondingPm) {
		super(correspondingPm);
	}

	private MainScreenPm getPm() {
		return (MainScreenPm) m_pm;
	}
	
	/**
	 * Create and show an AlertDialog that displays the names of 2 operations that the user
	 * may perform on the selected ShoppingList (like a popup menu): "Edit Name", and "Delete".
	 */
	@Override
	public void displayUi() {
		Activity currentActivity = ApplicationPm.m_currentActivity; 
		
		// Build a "popup menu" with the names of the operations to perform on the selected ShoppingList.
		final CharSequence[] popupMenuItems = new CharSequence[2];
		popupMenuItems[0] = currentActivity.getResources().getText(R.string.edit_name);
		popupMenuItems[1] = currentActivity.getResources().getText(R.string.delete);
		
		// Display the AlertDialog containing the "popup menu" and set myself as dialog click listener.
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(currentActivity);
		dialogBuilder
			.setTitle(currentActivity.getResources().getText(R.string.popup_menu_title_shopping_list_operations))
			.setItems(popupMenuItems, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					switch(id) {
					case 0:
						getPm().onEditSelectedShoppingListName();
						return;

					case 1:
						getPm().onDeleteSelectedShoppingList();
						return;
					}
				}
			 })
			.setCancelable(true);

		// Set the AlertDialog to be cancelable if the user clicks outside.
		AlertDialog dialog = dialogBuilder.create();
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}

	@Override
	public void closeUi() {
		throw new UnsupportedOperationException();
	}
}
