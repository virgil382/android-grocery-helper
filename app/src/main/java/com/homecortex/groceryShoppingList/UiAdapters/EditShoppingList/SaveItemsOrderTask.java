package com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList;

import java.sql.SQLException;
import java.util.concurrent.Callable;

import android.os.AsyncTask;
import android.util.Log;

import com.homecortex.groceryShoppingList.DataModel.Entities.ShoppingList;
import com.homecortex.groceryShoppingList.DataModel.Extents.NaturalShoppingOrderLearningExtent;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.Util.LicenseManager;
import com.j256.ormlite.misc.TransactionManager;

/**
 * A SaveItemsOrderTask is an AsyncTask that animates a ShoppingList's saveNaturalShoppingOrder() via a 
 * thread different than the UI thread.  It mediates the interaction between saveNaturalShoppingOrder() and
 * a UiAdapter.  Specifically, UiAapter may call SaveItemsOrderTask.cancel().  saveNaturalShoppingOrder()
 * may poll SaveItemsOrderTask.isCanceled() and may call SaveItemsOrderTask.updateProgress(), which calls
 * UiAdapter.updateProgress().
 */
public class SaveItemsOrderTask extends AsyncTask<Void, Integer, Void> {
	private static final String TAG = "SaveItemsOrderTask";

	private final ShoppingList m_shoppingList;
	private final boolean m_keepPickedUpItems;
	private final UiAdapter m_progressUi;
	private int m_nSteps;
	private int n_nStep;
	private final SaveItemsOrderTask m_this;
	
	public class CancelledException extends Exception {
		private static final long serialVersionUID = -2811355287705261446L;
	}
	
	public SaveItemsOrderTask(ShoppingList shoppingList, boolean keepPickedUpItems, UiAdapter progressUi) {
		m_shoppingList = shoppingList;
		m_keepPickedUpItems = keepPickedUpItems;
		m_progressUi = progressUi;
		m_this = this;
	}

	// Display the progress UI.  This method executes in the UI thread before the worker thread starts.
	@Override
	protected void onPreExecute() {
		m_progressUi.displayUi();
	}

	// Execute ShoppingList.saveNaturalShoppingOrder(), which is a CPU-intensive operation.  While executing,
	// the algorithm will periodically call the AsyncTasks's isCancelled() and publishProgress().  This method
	// executes in a separate worker thread (i.e. not the UI thread). 
	@Override
	protected Void doInBackground(Void... params) {
		try {
			TransactionManager.callInTransaction(ApplicationPm.getDatabaseHelper().getConnectionSource(),
				new Callable<Void>() {
			        public Void call() throws Exception {
			        	NaturalShoppingOrderLearningExtent.instance().reloadCache();
						m_shoppingList.saveNaturalShoppingOrder(m_keepPickedUpItems, m_this);
			        	NaturalShoppingOrderLearningExtent.instance().clearCache();
			            return null;
			        }
				}
			);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void checkCancelledAndThrow(String currentActivity) throws CancelledException {
		if (isCancelled()) {
			Log.d(TAG, "saveNaturalShoppingOrder(): cancelled");
			throw new CancelledException();
		}
	}
	
	public void setTotalSteps(int nSteps){
		m_nSteps = nSteps;
		n_nStep = 0;
		publishProgress(0);
	}
	
	/**
	 * Update the percent progress.  This method may be called from the worker thread (i.e. not the UI thread).
	 */
	public void incrementStep() {
		n_nStep++;
		publishProgress((int) ((n_nStep / (float) m_nSteps) * 100));
	}

	// Handle a progress update initiate by a call to updateProgress() from the worker thread.  Forward the
	// progress value to UiAdapter.updateProgress.  This method executes in the UI thread.
	@Override
	protected void onProgressUpdate(Integer... progress) {
		m_progressUi.updateProgress(progress[0]);
	}
	
	// Close the progress UI.  This method executes in the UI thread after the worker thread exits.
	@Override
	protected void onPostExecute(Void result) {
		doPostExecute(false);
	}
	
	@Override
	protected void onCancelled()
	{
		doPostExecute(true);
	}
	
	private void doPostExecute(boolean isCancelled) {
		m_progressUi.closeUi();
		m_shoppingList.sortOrderedItemInstances();
		
		if (!isCancelled()) {
	        LicenseManager licenseManager = LicenseManager.instance();
	        licenseManager.incrementItemOrderLearningCount();  // Also decrement the grace usage count.
		}
	}
}
