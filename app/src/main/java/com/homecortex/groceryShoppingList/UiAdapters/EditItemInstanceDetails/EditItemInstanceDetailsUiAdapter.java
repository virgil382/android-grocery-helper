// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.EditItemInstanceDetails;

import android.app.Activity;
import android.content.Intent;

import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.Ui.EditItemInstanceDetails.EditItemInstanceDetailsUi;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.Registry.IActivityId;
import com.homecortex.groceryShoppingList.UiAdapters.Registry.UiPmRegistry;
import com.homecortex.groceryShoppingList.R;

public class EditItemInstanceDetailsUiAdapter extends UiAdapter implements IActivityId 
{
	public EditItemInstanceDetailsUiAdapter(UiPm correspondingPm) {
		super(correspondingPm);
		UiPmRegistry.registerPm(id(), correspondingPm);
	}

	@Override
	public int id() {
		return R.layout.edititeminstancedetails;
	}

	@Override
	public void displayUi() {
		Intent intent = new Intent(ApplicationPm.m_currentActivity, EditItemInstanceDetailsUi.class);
		ApplicationPm.m_currentActivity.startActivityForResult(intent, id());
	}

	@Override
	public void closeUi() {
		Activity currentActivity = ApplicationPm.m_currentActivity;

		currentActivity.setResult(Activity.RESULT_OK);
		currentActivity.finish();
	}

}
