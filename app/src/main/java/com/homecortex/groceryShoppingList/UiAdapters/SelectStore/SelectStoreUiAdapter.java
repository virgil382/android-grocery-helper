// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.SelectStore;

import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.Ui.SelectStore.SelectStoreUi;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.Registry.IActivityId;
import com.homecortex.groceryShoppingList.UiAdapters.Registry.UiPmRegistry;
import com.homecortex.groceryShoppingList.R;

import android.app.Activity;
import android.content.Intent;

public class SelectStoreUiAdapter extends UiAdapter implements IActivityId
{
	public SelectStoreUiAdapter(UiPm correspondingPm)
	{
		super(correspondingPm);
		UiPmRegistry.registerPm(id(), correspondingPm);
	}

	/// Satisfy the requirements of IActivityId.
	@Override
	public int id() {
		return R.layout.selectstore;
	}

	/// Satisfy the requirements of UiAdapter.
	@Override
	public void displayUi() {
		Intent intent = new Intent(ApplicationPm.m_currentActivity, SelectStoreUi.class);
		ApplicationPm.m_currentActivity.startActivityForResult(intent, id());
	}

	/// Satisfy the requirements of UiAdapter.
	@Override
	public void closeUi() {
		Activity currentActivity = ApplicationPm.m_currentActivity;

		currentActivity.setResult(Activity.RESULT_OK);
		currentActivity.finish();
	}
}
