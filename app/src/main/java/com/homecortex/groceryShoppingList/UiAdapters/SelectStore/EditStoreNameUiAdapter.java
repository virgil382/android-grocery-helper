// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.SelectStore;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.MainScreenPm;
import com.homecortex.groceryShoppingList.PresentationModel.SelectStorePm;
import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.R;

/**
 * EditStoreNameUiAdapter is a UiAdapter that displays a dialog box that allows the user
 * to specify a new name for a Store.  After he does, the dialog box notifies the
 * SelectStorePm of the new name.
 * 
 * Collaborators: SelectStorePm
 */
public class EditStoreNameUiAdapter extends UiAdapter {

	public EditStoreNameUiAdapter(UiPm correspondingPm) {
		super(correspondingPm);
	}

	private SelectStorePm getPm() {
		return (SelectStorePm) m_pm;
	}

	/**
	 * Create and show an {@link AlertDialog} that displays the name of the ShoppingList
	 * selected in the {@link MainScreenPm}, selects the entire text, displays a soft
	 * keyboard, and allows the user to edit name.  The {@link AlertDialog} also has an "OK"
	 * and a "Cancel" button for which this method registers {@link DialogInterface.OnClickListener}s.
	 * 
	 * <p>If the user clicks the "OK" button, then the corresponding
	 * {@link DialogInterface.OnClickListener} tells the {@link MainScreenPm} to set the
	 * name of the selected ShoppingList to the new name specified by the user, and
	 * closes the {@link AlertDialog}.
	 * 
	 * <p>If the user press the "Cancel" button, then the corresponding
	 * {@link DialogInterface.OnClickListener} closes the {@link AlertDialog}.
	 */
	@Override
	public void displayUi() {
		final Activity currentActivity = ApplicationPm.m_currentActivity; 

		// Prepare the EditText displayed in the AlertDialog.
		final EditText input = new EditText(currentActivity);
        input.setId(0);
        input.setText(getPm().getNameOfClickedStore());
        input.setSingleLine();
        input.setImeOptions(EditorInfo.IME_ACTION_SEND);
        input.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
            	input.selectAll();
            	input.post(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager inputMethodManager= (InputMethodManager) currentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(input, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
            }
        });

        // Create the AlertDialog asking for the new name of a shopping list, and set myself
		// dialog click listener.
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(currentActivity);
		dialogBuilder
			.setTitle(currentActivity.getResources().getText(R.string.popup_menu_title_edit_shopping_list_name))
			.setView(input)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// Get the name set by the user.
					String value = input.getText().toString();
					getPm().setNameOfClickedStore(value);
	                return;
				}
			 })
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
	                return;
				}
			 });
        final AlertDialog dialog = dialogBuilder.create();

        // Add a handler for when the user presses "Send" on the keyboard.
        input.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                	dialog.dismiss();
                	
					// Get the name set by the user.
					String value = input.getText().toString();
					getPm().setNameOfClickedStore(value);
                    handled = true;
                }
                return handled;
            }
        });

        // Display the AlertDialog, and set focus to the EditText, and display the keyboard.
        dialog.show();
		input.requestFocus();
	}

	/**
	 * Not supported.
	 */
	@Override
	public void closeUi() {
		throw new UnsupportedOperationException();
	}
}
