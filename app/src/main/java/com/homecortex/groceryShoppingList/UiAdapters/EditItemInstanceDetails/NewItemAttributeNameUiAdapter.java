// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.EditItemInstanceDetails;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.EditItemInstanceDetailsPm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.R;

/**
 * NewItemAttributeNameUiAdapter is a UiAdapter that displays a dialog box that allows the user
 * to specify a the name of a new ItemInstanceAttribute.  After he does, the dialog box notifies the
 * EditItemInstanceDetailsPm of the new name.
 * 
 * Collaborators: EditItemInstanceDetailsPm
 */
public class NewItemAttributeNameUiAdapter extends UiAdapter {
	EditText m_input;
	Activity m_currentActivity;

	public NewItemAttributeNameUiAdapter(EditItemInstanceDetailsPm correspondingPm) {
		super(correspondingPm);
	}

	private EditItemInstanceDetailsPm getPm() {
		return (EditItemInstanceDetailsPm) m_pm;
	}

	@Override
	public void displayUi() {
		m_currentActivity = ApplicationPm.m_currentActivity; 

		m_input = new EditText(m_currentActivity);
        m_input.setId(0);
        m_input.setText(m_currentActivity.getResources().getText(R.string.default_item_attribute_name));
        m_input.setSingleLine();
        m_input.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
            	m_input.selectAll();
            	m_input.post(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager inputMethodManager= (InputMethodManager) m_currentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(m_input, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
            }
        });
        
        // Display the AlertDialog asking for the new name of a shopping list, and set myself
		// dialog click listener.
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(m_currentActivity);
		dialogBuilder
			.setTitle(m_currentActivity.getResources().getText(R.string.popup_dialog_title_edit_item_attribute_name))
			.setView(m_input)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// Get the name set by the user.
					String value = m_input.getText().toString();
					getPm().addAnotherItemAttribute(value);
	                dismissSoftKeyboard();
	                return;
				}
			 })
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
	                dismissSoftKeyboard();
	                return;
				}
			 });
		
        m_input.requestFocus();
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
	}

	private void dismissSoftKeyboard() {
		InputMethodManager imm = (InputMethodManager) m_currentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(m_input.getWindowToken(), 0);
	}

	@Override
	public void closeUi() {
		throw new UnsupportedOperationException();
	}
}
