// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.Factory;

import com.homecortex.groceryShoppingList.PresentationModel.EditItemInstanceDetailsPm;
import com.homecortex.groceryShoppingList.PresentationModel.EditShoppingListPm;
import com.homecortex.groceryShoppingList.PresentationModel.MainScreenPm;
import com.homecortex.groceryShoppingList.PresentationModel.SelectLocationPm;
import com.homecortex.groceryShoppingList.PresentationModel.SelectStorePm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;

/**
 * An <code>IUiAdapterFactory</code> is an Abstract Factory of {@link UiAdapter}s.  It
 * describes operations that create new instances of concrete {@link UiAdapter}s. 
 */
public interface IUiAdapterFactory {
	/**
	 * Get the {@link UiAdapter} corresponding to the specified {@link MainScreenPm}.
	 * @param correspondingPm The {@link MainScreenPm}.
	 * @return The corresponding {@link UiAdapter}.
	 */
	UiAdapter getMainScreenUiAdapter(MainScreenPm correspondingPm);

	UiAdapter getShoppingListPopupMenuUiAdapter(MainScreenPm correspondingPm);
	
	UiAdapter getEditShoppingListNameUiAdapter(MainScreenPm correspondingPm);

	UiAdapter getEnterNewShoppingListNameUiAdapter(MainScreenPm correspondingPm);

	/**
	 * Get the {@link UiAdapter} corresponding to the specified {@link EditShoppingListPm}.
	 * @param correspondingPm The {@link EditShoppingListPm}.
	 * @return The corresponding {@link UiAdapter}.
	 */
	UiAdapter getEditShoppingListUiAdapter(EditShoppingListPm correspondingPm);

	UiAdapter getLearnShoppingOrderYesNoUiAdapter(EditShoppingListPm correspondingPm);

	UiAdapter getItemInstancePopupMenuUiAdapter(EditShoppingListPm correspondingPm);

	UiAdapter getEditItemInstanceNameUiAdapter(EditShoppingListPm correspondingPm);
	UiAdapter getEditItemInstancePriceUiAdapter(EditShoppingListPm editShoppingListPm);

	/**
	 * Get the {@link UiAdapter} corresponding to the specified {@link SelectStorePm}.
	 * @param correspondingPm The {@link SelectStorePm}.
	 * @return The corresponding {@link UiAdapter}.
	 */
	UiAdapter getSelectStoreUiAdapter(SelectStorePm correspondingPm);
	UiAdapter getEnterNewStoreNameUiAdapter(SelectStorePm correspondingPm);
	UiAdapter getStorePopupMenuUiAdapter(SelectStorePm selectStorePm);
	UiAdapter getEditNewStoreNameUiAdapter(SelectStorePm selectStorePm);

	UiAdapter getSelectLocationUiAdapter(SelectLocationPm selectLocationPm);

	UiAdapter getEditItemInstanceDetailsUiAdapter(EditItemInstanceDetailsPm correspondingPm);

	UiAdapter getItemInstanceAttributePopupMenuUiAdapter(EditItemInstanceDetailsPm correspondingPm);

	UiAdapter getEnterNewItemAttributeNameUiAdapter(EditItemInstanceDetailsPm editItemInstanceDetailsPm);

	UiAdapter getEditItemAttributeNameUiAdapter(EditItemInstanceDetailsPm editItemInstanceDetailsPm);

	UiAdapter getSavingItemsOrderUiAdapter(EditShoppingListPm editShoppingListPm);
	
	UiAdapter getLicenseNeededUiAdapter(EditShoppingListPm editShoppingListPm);
	
}
