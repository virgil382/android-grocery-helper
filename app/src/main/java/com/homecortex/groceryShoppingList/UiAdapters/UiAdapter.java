// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters;

import com.homecortex.groceryShoppingList.PresentationModel.UiPm;

public abstract class UiAdapter {
	public UiPm m_pm;

	public UiAdapter(UiPm correspondingPm)
	{
		m_pm = correspondingPm;
	}
	
	public abstract void displayUi();
	public abstract void closeUi();
	
	public void updateProgress(int progress) { }
}
