// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.Registry;

import java.util.TreeMap;

import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;

/**
 * An <code>UiPmRegistry</code> maps unique IDs to {@link UiPm}s.
 * 
 * <p>It allows instances of Android Activity handling
 * <code>onActivityResult(int, int, Intent)</code> to locate the requested {@link UiPm}
 * (i.e. the one providing the result) by its unique ID in order to notify it of the
 * result.  It expects each {@link UiAdapter} corresponding to an {@link UiPm} to 
 * implement the {@link IActivityId} interface to supply a unique ID (which could be
 * the ID of the activity's layout).
 */
public class UiPmRegistry 
{
	private static TreeMap<Integer, UiPm> registeredActivityPms = new TreeMap<>();

	/**
	 * Add an {@link UiPm} to the <code>UiPmRegistry</code>.
	 * @param activityId The android activity ID.
	 * @param pm The UiPm.
	 */
	public static void registerPm(int activityId, UiPm pm)
	{
		registeredActivityPms.put(activityId, pm);
	}
	
	/**
	 * Find the {@link UiPm} whose corresponding {@link UiAdapter} ID matches the one specified.
	 * @param activityId The ID.
	 * @return An {@link UiPm} if successful, otherwise null.
	 */
	public static UiPm getActivityPm(int activityId)
	{
		if (registeredActivityPms.containsKey(activityId))
			return (registeredActivityPms.get(activityId));
		
		return null;
	}
}
