// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.Factory;

import com.homecortex.groceryShoppingList.PresentationModel.EditItemInstanceDetailsPm;
import com.homecortex.groceryShoppingList.PresentationModel.EditShoppingListPm;
import com.homecortex.groceryShoppingList.PresentationModel.MainScreenPm;
import com.homecortex.groceryShoppingList.PresentationModel.SelectLocationPm;
import com.homecortex.groceryShoppingList.PresentationModel.SelectStorePm;
import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.EditItemInstanceDetails.EditAttributeNameUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.EditItemInstanceDetails.EditItemInstanceDetailsUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.EditItemInstanceDetails.ItemInstanceAttributePopupMenuUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.EditItemInstanceDetails.NewItemAttributeNameUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList.EditItemInstanceNameUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList.EditItemInstancePriceUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList.EditShoppingListUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList.ItemInstancePopupMenuUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList.LearnShoppingOrderYesNoUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList.LicenseNeededUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList.SavingItemsOrderUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.MainScreen.EditShoppingListNameUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.MainScreen.EnterNewShoppingListNameUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.MainScreen.MainScreenUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.MainScreen.ShoppingListPopupMenuUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.SelectLocation.SelectLocationUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.SelectStore.EditStoreNameUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.SelectStore.EnterNewStoreNameUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.SelectStore.SelectStoreUiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.SelectStore.StorePopupMenuUiAdapter;

/**
 * An <code>UiAdapterFactory</code> is a singleton that implements the
 * {@link IUiAdapterFactory} interface.  It creates {@link UiAdapter}s 
 * corresponding to concrete {@link UiPm}s.  Its behavior is specific to the
 * application, but unit tests may substituting the singleton instance with one whose
 * behavior meets the needs of the unit tests. 
 */
public class UiAdapterFactory implements IUiAdapterFactory
{
	/*
	 * The instance of {@link IUiAdapterFactory}.  Unit tests may set this to a concrete
	 * {@link IUiAdapterFactory} used for unit testing.
	 */
	public static IUiAdapterFactory instance = new UiAdapterFactory();

	// Singleton requirement.
	private UiAdapterFactory() {}

	// Lazy initialization of m_mainScreenUiAdapter.
	public static MainScreenUiAdapter m_mainScreenUiAdapter;
	@Override
	public UiAdapter getMainScreenUiAdapter(MainScreenPm correspondingPm) {
		if (m_mainScreenUiAdapter == null) {
			m_mainScreenUiAdapter = new MainScreenUiAdapter(correspondingPm);
		}
		return m_mainScreenUiAdapter;
	}

	// Lazy initialization of m_shoppingListPopupMenuUiAdapter.
	public static ShoppingListPopupMenuUiAdapter m_shoppingListPopupMenuUiAdapter;
	@Override
	public UiAdapter getShoppingListPopupMenuUiAdapter(MainScreenPm correspondingPm)
	{
		if (m_shoppingListPopupMenuUiAdapter == null) {
			m_shoppingListPopupMenuUiAdapter = new ShoppingListPopupMenuUiAdapter(correspondingPm);
		}
		return m_shoppingListPopupMenuUiAdapter;
	}
	
	// Lazy initialization of m_editShoppingListNameUiAdapter.
	public static EditShoppingListNameUiAdapter m_editShoppingListNameUiAdapter;
	@Override
	public UiAdapter getEditShoppingListNameUiAdapter(MainScreenPm correspondingPm)
	{
		if (m_editShoppingListNameUiAdapter == null) {
			m_editShoppingListNameUiAdapter = new EditShoppingListNameUiAdapter(correspondingPm);
		}
		return m_editShoppingListNameUiAdapter;
	}

	// Lazy initialization of m_enterNewShoppingListNameUiAdapter.
	public static EnterNewShoppingListNameUiAdapter m_enterNewShoppingListNameUiAdapter;
	@Override
	public UiAdapter getEnterNewShoppingListNameUiAdapter(MainScreenPm correspondingPm)
	{
		if (m_enterNewShoppingListNameUiAdapter == null) {
			m_enterNewShoppingListNameUiAdapter = new EnterNewShoppingListNameUiAdapter(correspondingPm);
		}
		return m_enterNewShoppingListNameUiAdapter;
	}
	
	// Lazy initialization of m_editShoppingListUiAdapter.
	public static EditShoppingListUiAdapter m_editShoppingListUiAdapter;
	@Override
	public UiAdapter getEditShoppingListUiAdapter(EditShoppingListPm correspondingPm) {
		if (m_editShoppingListUiAdapter == null) {
			m_editShoppingListUiAdapter = new EditShoppingListUiAdapter(correspondingPm);
		}
		return m_editShoppingListUiAdapter;
	}

	// Lazy initialization of m_learnShoppingOrderYesNoUiAdapter.
	public static LearnShoppingOrderYesNoUiAdapter m_learnShoppingOrderYesNoUiAdapter;
	@Override
	public UiAdapter getLearnShoppingOrderYesNoUiAdapter(EditShoppingListPm correspondingPm) {
		if (m_learnShoppingOrderYesNoUiAdapter == null) {
			m_learnShoppingOrderYesNoUiAdapter = new LearnShoppingOrderYesNoUiAdapter(correspondingPm);
		}
		return m_learnShoppingOrderYesNoUiAdapter;
	}
	
	// Lazy initialization of m_itemInstancePopupMenuUiAdapter.
	public static ItemInstancePopupMenuUiAdapter m_itemInstancePopupMenuUiAdapter;
	@Override
	public UiAdapter getItemInstancePopupMenuUiAdapter(EditShoppingListPm correspondingPm) {
		if (m_itemInstancePopupMenuUiAdapter == null) {
			m_itemInstancePopupMenuUiAdapter = new ItemInstancePopupMenuUiAdapter(correspondingPm);
		}
		return m_itemInstancePopupMenuUiAdapter;
	}

	// Lazy initialization of m_selectStoreUiAdapter.
	public static EditItemInstanceNameUiAdapter m_editItemInstanceNameUiAdapter;
	@Override
	public UiAdapter getEditItemInstanceNameUiAdapter(EditShoppingListPm correspondingPm) {
		if (m_editItemInstanceNameUiAdapter == null) {
			m_editItemInstanceNameUiAdapter = new EditItemInstanceNameUiAdapter(correspondingPm);
		}
		return m_editItemInstanceNameUiAdapter;
	}

	// Lazy initialization of m_selectStoreUiAdapter.
	public static EditItemInstancePriceUiAdapter m_editItemInstancePriceUiAdapter;
	@Override
	public UiAdapter getEditItemInstancePriceUiAdapter(EditShoppingListPm correspondingPm) {
		if (m_editItemInstancePriceUiAdapter == null) {
			m_editItemInstancePriceUiAdapter = new EditItemInstancePriceUiAdapter(correspondingPm);
		}
		return m_editItemInstancePriceUiAdapter;
	}

	// Lazy initialization of m_selectStoreUiAdapter.
	public static SelectStoreUiAdapter m_selectStoreUiAdapter;
	@Override
	public UiAdapter getSelectStoreUiAdapter(SelectStorePm correspondingPm) {
		if (m_selectStoreUiAdapter == null)
			m_selectStoreUiAdapter = new SelectStoreUiAdapter(correspondingPm);
		return m_selectStoreUiAdapter;
	}

	// Lazy initialization of m_enterNewStoreNameUiAdapter.
	public static EnterNewStoreNameUiAdapter m_enterNewStoreNameUiAdapter;
	@Override
	public UiAdapter getEnterNewStoreNameUiAdapter(SelectStorePm correspondingPm) {
		if (m_enterNewStoreNameUiAdapter == null) {
			m_enterNewStoreNameUiAdapter = new EnterNewStoreNameUiAdapter(correspondingPm);
		}
		return m_enterNewStoreNameUiAdapter;
	}

	// Lazy initialization of m_shoppingListPopupMenuUiAdapter.
	public static StorePopupMenuUiAdapter m_storePopupMenuUiAdapter;
	@Override
	public UiAdapter getStorePopupMenuUiAdapter(SelectStorePm correspondingPm) {
		if (m_storePopupMenuUiAdapter == null) {
			m_storePopupMenuUiAdapter = new StorePopupMenuUiAdapter(correspondingPm);
		}
		return m_storePopupMenuUiAdapter;
	}

	// Lazy initialization of m_editShoppingListNameUiAdapter.
	public static EditStoreNameUiAdapter m_editStoreNameUiAdapter;
	@Override
	public UiAdapter getEditNewStoreNameUiAdapter(SelectStorePm correspondingPm) {
		if (m_editStoreNameUiAdapter == null) {
			m_editStoreNameUiAdapter = new EditStoreNameUiAdapter(correspondingPm);
		}
		return m_editStoreNameUiAdapter;
	}
	
	// Lazy initialization of m_editShoppingListNameUiAdapter.
	public static SelectLocationUiAdapter m_selectLocationUiAdapter;
	@Override
	public UiAdapter getSelectLocationUiAdapter(SelectLocationPm correspondingPm) {
		if (m_selectLocationUiAdapter == null) {
			m_selectLocationUiAdapter = new SelectLocationUiAdapter(correspondingPm);
		}
		return m_selectLocationUiAdapter;
	}

	// Lazy initialization of m_editItemInstanceDetailsUiAdapter.
	public static EditItemInstanceDetailsUiAdapter m_editItemInstanceDetailsUiAdapter;
	@Override
	public UiAdapter getEditItemInstanceDetailsUiAdapter(EditItemInstanceDetailsPm correspondingPm) {
		if (m_editItemInstanceDetailsUiAdapter == null) {
			m_editItemInstanceDetailsUiAdapter = new EditItemInstanceDetailsUiAdapter(correspondingPm);
		}
		return m_editItemInstanceDetailsUiAdapter;
	}

	// Lazy initialization of m_itemInstanceAttributePopupMenuUiAdapter.
	public static ItemInstanceAttributePopupMenuUiAdapter m_itemInstanceAttributePopupMenuUiAdapter;
	@Override
	public UiAdapter getItemInstanceAttributePopupMenuUiAdapter(EditItemInstanceDetailsPm correspondingPm) {
		if (m_itemInstanceAttributePopupMenuUiAdapter == null) {
			m_itemInstanceAttributePopupMenuUiAdapter = new ItemInstanceAttributePopupMenuUiAdapter(correspondingPm);
		}
		return m_itemInstanceAttributePopupMenuUiAdapter;
	}

	// Lazy initialization of m_enterNewItemAttributeNameUiAdapter.
	public static NewItemAttributeNameUiAdapter m_enterNewItemAttributeNameUiAdapter;
	@Override
	public UiAdapter getEnterNewItemAttributeNameUiAdapter(EditItemInstanceDetailsPm correspondingPm) {
		if (m_enterNewItemAttributeNameUiAdapter == null) {
			m_enterNewItemAttributeNameUiAdapter = new NewItemAttributeNameUiAdapter(correspondingPm);
		}
		return m_enterNewItemAttributeNameUiAdapter;
	}
	
	// Lazy initialization of m_enterNewItemAttributeNameUiAdapter.
	public static EditAttributeNameUiAdapter m_editItemAttributeNameUiAdapter;
	@Override
	public UiAdapter getEditItemAttributeNameUiAdapter(EditItemInstanceDetailsPm correspondingPm) {
		if (m_editItemAttributeNameUiAdapter == null) {
			m_editItemAttributeNameUiAdapter = new EditAttributeNameUiAdapter(correspondingPm);
		}
		return m_editItemAttributeNameUiAdapter;
	}

	// Lazy initialization of m_enterNewItemAttributeNameUiAdapter.
	public static SavingItemsOrderUiAdapter m_savingItemsOrderUiAdapter;
	@Override
	public UiAdapter getSavingItemsOrderUiAdapter(EditShoppingListPm correspondingPm) {
		if (m_savingItemsOrderUiAdapter == null) {
			m_savingItemsOrderUiAdapter = new SavingItemsOrderUiAdapter(correspondingPm);
		}
		return m_savingItemsOrderUiAdapter;
	}

	// Lazy initialization of m_licenseNeededUiAdapter.
	public static LicenseNeededUiAdapter m_licenseNeededUiAdapter;
	@Override
	public UiAdapter getLicenseNeededUiAdapter(EditShoppingListPm correspondingPm) {
		if (m_licenseNeededUiAdapter == null) {
			m_licenseNeededUiAdapter = new LicenseNeededUiAdapter(correspondingPm);
		}
		return m_licenseNeededUiAdapter;
	}
}
