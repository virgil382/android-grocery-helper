// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.Registry;

public interface IActivityId 
{
	int id();
}
