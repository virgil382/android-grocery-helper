// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.SelectLocation;

import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.Ui.SelectLocation.SelectLocationUi;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.Registry.IActivityId;
import com.homecortex.groceryShoppingList.UiAdapters.Registry.UiPmRegistry;

import android.app.Activity;
import android.content.Intent;

public class SelectLocationUiAdapter extends UiAdapter implements IActivityId
{
	public SelectLocationUiAdapter(UiPm correspondingPm)
	{
		super(correspondingPm);
		UiPmRegistry.registerPm(id(), correspondingPm);
	}

	/// Satisfy the requirements of IActivityId.
	@Override
	public int id() {
		//return R.layout.selectlocation;
		return 2222;  // Only lower 16 bits may be used for FragmentActivity.
	}

	/// Satisfy the requirements of UiAdapter.
	@Override
	public void displayUi() {
		Intent intent = new Intent(ApplicationPm.m_currentActivity, SelectLocationUi.class);
		ApplicationPm.m_currentActivity.startActivityForResult(intent, id());
	}

	/// Satisfy the requirements of UiAdapter.
	@Override
	public void closeUi() {
		Activity currentActivity = ApplicationPm.m_currentActivity;

		currentActivity.setResult(Activity.RESULT_OK);
		currentActivity.finish();
		
		// Hack to avoid problem with parent activity's onActivityResult() not being called.
		ApplicationPm.getSelectStorePm().onResult(ApplicationPm.getSelectLocationPm(), false);
	}
}
