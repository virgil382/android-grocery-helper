// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.EditItemInstanceDetails;

import java.sql.SQLException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.EditItemInstanceDetailsPm;
import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.R;

/**
 * ItemInstanceAttributePopupMenuUiAdapter is a UiAdapter that displays a popup menu that allows the user
 * to initiate one of several activities related to an ItemInstanceAttribute.  After he does, the popup
 * menu notifies the EditItemInstanceDetailsPm which activity the user wishes to initiate.
 * 
 * Collaborators: EditItemInstanceDetailsPm
 */
public class ItemInstanceAttributePopupMenuUiAdapter extends UiAdapter {

	public ItemInstanceAttributePopupMenuUiAdapter(UiPm correspondingPm) {
		super(correspondingPm);
	}

	private EditItemInstanceDetailsPm getPm() {
		return (EditItemInstanceDetailsPm) m_pm;
	}

	@Override
	public void displayUi() {
		Activity currentActivity = ApplicationPm.m_currentActivity; 
		
		// Build a "popup menu" with the names of the operations to perform on the selected ItemAttribute.
		final CharSequence[] popupMenuItems = new CharSequence[2];
		popupMenuItems[0] = currentActivity.getResources().getText(R.string.edit_name);
		popupMenuItems[1] = currentActivity.getResources().getText(R.string.delete);
		
		// Display the AlertDialog containing the "popup menu" and set myself as dialog click listener.
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(currentActivity);
		dialogBuilder
			.setTitle(currentActivity.getResources().getText(R.string.popup_menu_title_item_attribute_operations))
			.setItems(popupMenuItems, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					switch(id) {
					case 0:
						getPm().onEditItemAttributeName();
						return;

					case 1:
						try {
							getPm().onDeleteItemAttribute();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return;
					}
				}
			 })
			.setCancelable(true);

		// Set the AlertDialog to be cancelable if the user clicks outside.
		AlertDialog dialog = dialogBuilder.create();
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}

	@Override
	public void closeUi() {
		throw new UnsupportedOperationException();
	}

}
