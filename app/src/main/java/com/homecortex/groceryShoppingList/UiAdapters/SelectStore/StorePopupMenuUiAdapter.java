// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.SelectStore;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.SelectStorePm;
import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.R;

/**
 * StorePopupMenuUiAdapter is a UiAdapter that displays a popup menu that allows the user
 * to initiate one of several activities related to an Store.  After he does, the popup
 * menu notifies the SelectStorePm which activity the user wishes to initiate.
 * 
 * Collaborators: SelectStorePm
 */
public class StorePopupMenuUiAdapter extends UiAdapter 
{
	public StorePopupMenuUiAdapter(UiPm correspondingPm) {
		super(correspondingPm);
	}

	private SelectStorePm getPm() {
		return (SelectStorePm) m_pm;
	}
	
	/**
	 * Create and show an AlertDialog that displays the names of 2 operations that the user
	 * may perform on the selected ShoppingList (like a popup menu): "Edit Name", and "Delete".
	 */
	/**
	 * Create and show an {@link AlertDialog} that displays the the operations that a user may
	 * perform on the ShoppingList currently selected in the MainScreenPm.  This
	 * method registers a {@link DialogInterface.OnClickListener} that handles clicks on the
	 * items.
	 * 
	 * <p>If the user clicks the "Edit Name" item, then the {@link DialogInterface.OnClickListener}
	 * tells the MainScreenPm that the user wishes to change the name, and closes the
	 * {@link AlertDialog}.
	 * 
	 * <p>If the user press the "Cancel" button, then the {@link DialogInterface.OnClickListener}
	 * closes the {@link AlertDialog}.
	 */
	@Override
	public void displayUi() {
		Activity currentActivity = ApplicationPm.m_currentActivity; 
		
		// Build a "popup menu" with the names of the operations to perform on the clicked Store.
		final CharSequence[] popupMenuItems = new CharSequence[2];
		popupMenuItems[0] = currentActivity.getResources().getText(R.string.edit_name);
		popupMenuItems[1] = currentActivity.getResources().getText(R.string.delete);
		
		// Display the AlertDialog containing the "popup menu" and set myself as dialog click listener.
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(currentActivity);
		dialogBuilder
			.setTitle(currentActivity.getResources().getText(R.string.popup_menu_title_store_operations))
			.setItems(popupMenuItems, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					switch(id) {
					case 0:
						getPm().onEditClickedStoreName();
						return;

					case 1:
						getPm().onDeleteClickedStore();
						return;
					}
				}
			 })
			.setCancelable(true);

		// Set the AlertDialog to be cancelable if the user clicks outside.
		AlertDialog dialog = dialogBuilder.create();
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}

	@Override
	public void closeUi() {
		throw new UnsupportedOperationException();
	}
}
