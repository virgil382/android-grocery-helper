// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList;

import java.sql.SQLException;

import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.EditShoppingListPm;
import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * An <code>LearnShoppingOrderYesNoUiAdapter</code> is a {@link UiAdapter} responsible for
 * displaying an {@link AlertDialog} that asks the user if after learning the shopping order
 * of the currently picked-up ItemInstances, the system should keep them in the
 * ShoppingList (i.e. should not delete them).  The {@link AlertDialog} contains
 * a "Yes" and a "No" button.
 * 
 * <p> Note that there is no separate Ui class that handles the interaction between the
 * user and the {@link AlertDialog} that this {@link UiAdapter} displays.  This is because
 * the interaction code is very simple (see <code>displayUi()</code>).</p>
 */
public class LearnShoppingOrderYesNoUiAdapter  extends UiAdapter
{
	public LearnShoppingOrderYesNoUiAdapter(UiPm correspondingPm) {
		super(correspondingPm);
	}

	private EditShoppingListPm getPm() {
		return (EditShoppingListPm) m_pm;
	}

	@Override
	public void displayUi() {
		Activity currentActivity = ApplicationPm.m_currentActivity;
		
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        switch (which){
		        case DialogInterface.BUTTON_POSITIVE:
		            //Yes button clicked
		        	try {
						getPm().saveItemsOrder(true);
					} catch (SQLException e) {
						e.printStackTrace();
					}
		            break;

		        case DialogInterface.BUTTON_NEGATIVE:
		            //No button clicked
					try {
						getPm().saveItemsOrder(false);
					} catch (SQLException e) {
						e.printStackTrace();
					}
		            break;
		        }
		    }
		};
		
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(currentActivity);
		dialogBuilder
			.setMessage(currentActivity.getString(R.string.keep_picked_up_items))
			.setPositiveButton(currentActivity.getString(R.string.yes), dialogClickListener)
			.setNegativeButton(currentActivity.getString(R.string.no), dialogClickListener)
			.setCancelable(true);

		// Set the AlertDialog to be cancelable if the user clicks outside.
		AlertDialog dialog = dialogBuilder.create();
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}

	@Override
	public void closeUi() {
		throw new UnsupportedOperationException();
	}
}
