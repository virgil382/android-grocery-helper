// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList;

import java.sql.SQLException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.EditShoppingListPm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.R;

/**
 * EditItemInstanceNameUiAdapter is a UiAdapter that displays a dialog box that allows the user
 * to specify a new name for an ItemInstance.  After he does, the dialog box notifies the
 * EditShoppingListPm of the new name.
 * 
 * Collaborators: EditShoppingListPm
 */
public class EditItemInstanceNameUiAdapter extends UiAdapter {
	EditText m_input;
	Activity m_currentActivity;

	public EditItemInstanceNameUiAdapter(EditShoppingListPm correspondingPm) {
		super(correspondingPm);
	}

	private EditShoppingListPm getPm() {
		return (EditShoppingListPm) m_pm;
	}

	@Override
	public void displayUi() {
		m_currentActivity = ApplicationPm.m_currentActivity; 

		// Prepare the EditText displayed in the AlertDialog.
		m_input = new EditText(m_currentActivity);
        m_input.setId(0);
        try {
			m_input.setText(getPm().getSelectedItemInstanceName());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        m_input.setSingleLine();
        m_input.setImeOptions(EditorInfo.IME_ACTION_SEND);
        m_input.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
            	m_input.selectAll();
            	m_input.post(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager inputMethodManager= (InputMethodManager) m_currentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(m_input, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
            }
        });

        // Create the AlertDialog asking for the new name of a shopping list, and set myself
		// dialog click listener.
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(m_currentActivity);
		dialogBuilder
			.setTitle(m_currentActivity.getResources().getText(R.string.popup_dialog_title_edit_item_instance_name))
			.setView(m_input)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// Get the name set by the user.
					String value = m_input.getText().toString();
					try {
						getPm().setSelectedItemInstanceName(value);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	                dismissSoftKeyboard();
	                return;
				}
			 })
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
	                dismissSoftKeyboard();
	                return;
				}
			 });
        final AlertDialog dialog = dialogBuilder.create();

        // Add a handler for when the user presses "Send" on the keyboard.
        m_input.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                	dialog.dismiss();
                	
					// Get the name set by the user.
					String value = m_input.getText().toString();
					try {
						getPm().setSelectedItemInstanceName(value);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                    handled = true;
                }
                dismissSoftKeyboard();
                return handled;
            }
        });

        // Display the AlertDialog, and set focus to the EditText, and display the keyboard.
        dialog.show();
		m_input.requestFocus();
	}

	private void dismissSoftKeyboard() {
		InputMethodManager imm = (InputMethodManager) m_currentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(m_input.getWindowToken(), 0);
	}

	@Override
	public void closeUi() {
		throw new UnsupportedOperationException();
	}
}
