package com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.ProgressBar;

import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.EditShoppingListPm;
import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.R;

public class SavingItemsOrderUiAdapter extends UiAdapter {

	Activity m_currentActivity;
	ProgressBar m_progressBar;
	AlertDialog m_alertDialog;

	public SavingItemsOrderUiAdapter(UiPm correspondingPm) {
		super(correspondingPm);
	}
	
	private EditShoppingListPm getPm() {
		return (EditShoppingListPm) m_pm;
	}

	@Override
	public void displayUi() {
		m_currentActivity = ApplicationPm.m_currentActivity; 

		m_progressBar = new ProgressBar(m_currentActivity, null, android.R.attr.progressBarStyleHorizontal);
		
        // Create the AlertDialog showing a single cancel button.
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(m_currentActivity);
		dialogBuilder
			.setTitle(m_currentActivity.getResources().getText(R.string.popup_dialog_title_saving_items_order))
			.setView(m_progressBar)
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
			    	getPm().cancelSaveItemsOrder();
				}
			 });

        m_alertDialog = dialogBuilder.create();
        m_alertDialog.show();
	}

	@Override
	public void updateProgress(int progress) {
		// Update the progress bar.
		m_progressBar.setProgress(progress);
	}
	
	@Override
	public void closeUi() {
		m_alertDialog.dismiss();
	}
}
