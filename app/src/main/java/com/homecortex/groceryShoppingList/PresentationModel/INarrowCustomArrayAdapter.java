// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.PresentationModel;

import java.util.Comparator;
import java.util.List;

/**
 * An <code>INarrowCustomArrayAdapter</code> declares an interface realized by
 * CustomArrayAdapter that is "narrower" than that of a CustomArrayAdapter.  This
 * "narrow" interface represents the minimum set of operations that a concrete {@link UiPm} would
 * need to operate on the array of entities within a CustomArrayAdapter in order to
 * influence how its items are displayed by a UI control that observes it (such as a ListView).
 *
 * @param <T> The type of the entities in the array within the CustomArrayAdapter.
 */
public interface INarrowCustomArrayAdapter<T>
{
	/**
	 * Select the specified <code>T</code>.
	 * @param selectedObject The <code>T</code> to select.
	 */
	void setSelection(T selectedObject);
	
	/**
	 * Select the <code>T</code> at the specified 0-indexed position.
	 * @param pos The index of the selected row.  A negative value indicates no selection. A
	 * value >= the number of <code>T</code>s sets the selection index to <code>-1</code> (i.e. no
	 * selection).
	 */
	T setSelection(int pos);
	
	/**
	 * Get the currently selected <code>T</code>.
	 * @return The currently selected <code>T</code>.
	 */
	T getSelection();

	List<T> getObjects();
	
	/**
	 * Add a new <code>T</code> to the end of the array.
	 * @param newItem The new <code>T</code> to add.
	 * @param selectNewItem Flag indicating whether or not to select the
	 * newly added <code>T</code>.
	 */
	void add(T newItem, boolean selectNewItem);
	void addFromAnyThread(T newItem, boolean selectNewItem);

	/**
	 * Add a new <code>T</code> at the specified position in the array.
	 * @param newItem The new <code>T</code> to add.
	 * @param position The 0-indexed position of the <code>T</code> before which to
	 * add the new <code>T</code>. 
	 * @param selectNewItem Flag indicating whether or not to select the
	 * newly added <code>T</code>.
	 */
	void add(T newItem, int position, boolean selectNewItem);

	/**
	 * Remove a <code>T</code>.  If removing the selected one, then select the next
	 * (remaining) <code>T</code>.  If removing the last <code>T</code> and it is 
	 * selected, then select the previous (remaining) <code>T</code>.
	 */
	void remove(T doomedItem);
	void removeFromAnyThread(T doomedItem);

	void clear();
	
	T get(int location);

	/**
     * Sorts the content of this <code>INarrowCustomArrayAdapter</code> using the
     * specified comparator.
     * @param comparator The comparator used to sort the objects contained
     *        in this <code>INarrowCustomArrayAdapter</code>.
     */
	void sort(Comparator<? super T> comparator);
	void sortFromAnyThread(Comparator<? super T> comparator);

    /**
	 * Notify the UI observing the CustomArrayAdapter that the contents of
	 * the array, or the selected <code>T</code> within the array, have changed.
	 */
	void notifyDataSetChanged();

}
