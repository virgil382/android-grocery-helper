// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.PresentationModel;

import com.homecortex.groceryShoppingList.DataModel.Interfaces.IDatabaseHelper;

import android.app.Activity;

public class ApplicationPm 
{
	public static Activity m_currentActivity;
	
	private static IDatabaseHelper m_databaseHelper;

	public static void setDatabaseHelper(IDatabaseHelper newDatabaseHelper)
	{
		m_databaseHelper = newDatabaseHelper;
	}
	
	public static IDatabaseHelper getDatabaseHelper()
	{
		return m_databaseHelper;
	}
	
	// Lazy initialization of mainScreenPm.
	public static MainScreenPm m_mainScreenPm;
	public static MainScreenPm getMainScreenPm()
	{
		if (m_mainScreenPm == null) m_mainScreenPm = new MainScreenPm();
		return m_mainScreenPm;
	}

	// Lazy initialization of editShoppingListPm.
	public static EditShoppingListPm m_editShoppingListPm;
	public static EditShoppingListPm getEditShoppingListPm()
	{
		if (m_editShoppingListPm == null) m_editShoppingListPm = new EditShoppingListPm();
		return m_editShoppingListPm;
	}

	// Lazy initialization of selectStorePm.
	public static SelectStorePm m_selectStorePm;
	public static SelectStorePm getSelectStorePm()
	{
		if (m_selectStorePm == null) m_selectStorePm = new SelectStorePm();
		return m_selectStorePm;
	}

	// Lazy initialization of selectLocationPm.
	public static SelectLocationPm m_selectLocationPm;
	public static SelectLocationPm getSelectLocationPm() {
		if (m_selectLocationPm == null) m_selectLocationPm = new SelectLocationPm();
		return m_selectLocationPm;
	}

	// Lazy initialization of editItemInstanceDetailsPm.
	public static EditItemInstanceDetailsPm m_editItemInstanceDetailsPm;
	public static EditItemInstanceDetailsPm getEditItemInstanceDetailsPm()
	{
		if (m_editItemInstanceDetailsPm == null) m_editItemInstanceDetailsPm = new EditItemInstanceDetailsPm();
		return m_editItemInstanceDetailsPm;
	}
}
