// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.PresentationModel;

public interface IMapAdapter {
	void setCameraLocation(double lat, double lng);
	void setMarker(double lat, double lng, double radius, String name);
	void setRadius(double radius);
}
