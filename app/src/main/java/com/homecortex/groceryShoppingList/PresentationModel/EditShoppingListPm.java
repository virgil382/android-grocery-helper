// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.PresentationModel;

import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.UpdateSet;
import com.homecortex.groceryShoppingList.DataModel.EntityCollectionManager;
import com.homecortex.groceryShoppingList.DataModel.Entities.Item;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstance;
import com.homecortex.groceryShoppingList.DataModel.Entities.ShoppingList;
import com.homecortex.groceryShoppingList.DataModel.Entities.Store;
import com.homecortex.groceryShoppingList.DataModel.EntityObserver;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.StoreExtent;
import com.homecortex.groceryShoppingList.Location.ILocationListener;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.EditShoppingList.SaveItemsOrderTask;
import com.homecortex.groceryShoppingList.UiAdapters.Factory.UiAdapterFactory;
import com.homecortex.groceryShoppingList.Util.BooleanResult;
import com.homecortex.groceryShoppingList.Util.LicenseManager;

import android.location.Location;
import android.util.Log;

public class EditShoppingListPm extends UiPm implements ILocationListener
{
	private static final String TAG = "EditShoppingListPm";
	int selectedItemInstancePosition;
	
	public EditShoppingListPm() {
		m_allStores.open();
        m_itemNameChangeHandler.open();
	}
	
	private ShoppingList m_currentShoppingList;
	public void setCurrentShoppingList(ShoppingList currentShoppingList)
	{
		m_currentShoppingList = currentShoppingList;
	}
	public ShoppingList getCurrentShoppingList()
	{
		return m_currentShoppingList;
	}
	
	public String getSelectedStoreName() {
		if (getCurrentShoppingList() == null) return null;
		if (getCurrentShoppingList().getStore() == null) return null;
		return getCurrentShoppingList().getStore().getName();
	}

	public boolean isAutoSelectStore() {
		return (getCurrentShoppingList() == null || getCurrentShoppingList().getIsAutoSelectStore());
	}

	public void setAutoSelectStore(boolean isAutoSelectEnabled) throws SQLException {
		if (getCurrentShoppingList() == null) return;
		getCurrentShoppingList().setIsAutoSelectStore(isAutoSelectEnabled);
		if (isAutoSelectEnabled) getCurrentShoppingList().setStoreId(null);
	}

	public UiAdapter getActivityUiAdapter()
	{
		return (UiAdapterFactory.instance.getEditShoppingListUiAdapter(this));
	}

	@Override
	public boolean dispatchResult(UiPm requesterPm, boolean isCanceled) {
		return requesterPm.onResult(this, isCanceled);
	}
	
	/**
	 * Set the list of ItemInstances that the current ShoppingList will update as appropriate.
	 * @throws SQLException
	 */
	public void setOrderedItemInstances(INarrowCustomArrayAdapter<ItemInstance> orderedItemInstances) throws SQLException
	{
		if (getCurrentShoppingList() != null) {
            getCurrentShoppingList().setOrderedItemInstances(orderedItemInstances);
            m_itemNameChangeHandler.setOrderedItemInstances(orderedItemInstances);
        }
	}

	public void sortOrderedItemInstancesIfNecessary() {
		if (getCurrentShoppingList() != null)
			getCurrentShoppingList().sortOrderedItemInstancesIfNecessary();
}

	public void addItemInstance(String itemName) throws SQLException
	{
		itemName = itemName.trim();
		Log.d(TAG, "addItemInstance: " + itemName);
		
		if (getCurrentShoppingList() == null) return;
		if (itemName.compareTo("") == 0) return;

		// Try to find an Item with the specified name.  If not found, then create one with no defaults.
		Item item = Item.getItem(itemName);
		if (item == null) {
			item = new Item(itemName);
            item.setDefaults();
		}

		// Create a new ItemInstance out of this Item and add it to the current ShoppingList and
        // populate it with defaults (e.g. default ItemInstanceAttributes).
		ItemInstance newItemInstance = new ItemInstance(getCurrentShoppingList().getId(), item);
        newItemInstance.setDefaults(item);
	}

	public void onButtonClickSelectStore() throws SQLException {
		Log.d(TAG, "onButtonClickSelectStore");

		if (getCurrentShoppingList() == null) return;

		ApplicationPm.getSelectStorePm().setCloseOnSelect(true);
		ApplicationPm.getSelectStorePm().setSelectedStore(getCurrentShoppingList().getStore());
		ApplicationPm.getSelectStorePm().displayUi();
	}

	public void onClickItemInstance(int position) {
		Log.d(TAG, "onClickItemInstance");
		editItemInstanceAttributes(position);
	}

	public void onLongClickItemInstance(int position) {
		Log.d(TAG, "onLongClickItemInstance");
		selectedItemInstancePosition = position;
		UiAdapterFactory.instance.getItemInstancePopupMenuUiAdapter(this).displayUi();	
	}

	private void editItemInstanceAttributes(int position) {
		Log.d(TAG, "editItemInstanceAttributes");
		
		if (getCurrentShoppingList() == null) return;
		
		ItemInstance itemInstance = getCurrentShoppingList().getItemInstance(position);
		if (itemInstance == null) return;

		ApplicationPm.getEditItemInstanceDetailsPm().setItemInstance(itemInstance);
		ApplicationPm.getEditItemInstanceDetailsPm().displayUi();
	}
	
	public void onMoveItemInstance(int from, int to) throws SQLException {
		ShoppingList shoppingList = getCurrentShoppingList();
		if (shoppingList != null)
			shoppingList.moveItemInstance(from, to);
	}

	public void onRemoveItemInstance(int position) throws SQLException {
		ShoppingList shoppingList = getCurrentShoppingList();
		if (shoppingList != null)
			shoppingList.deleteItemInstance(position);
	}

	public void onPermanentlyRemoveItemInstance(int position) throws SQLException {
		ShoppingList shoppingList = getCurrentShoppingList();
		if (shoppingList != null) {
			Item item = shoppingList.getItem(position);
			shoppingList.deleteAllItemInstances(item);
			ItemExtent.instance().delete(item);
		}
	}

	public void onEditSelectedItemInstanceAttributes() {
		Log.d(TAG, "onEditSelectedItemInstanceAttributes");
		editItemInstanceAttributes(selectedItemInstancePosition);
	}
	public void onEditSelectedItemInstanceName() {
		Log.d(TAG, "onEditSelectedItemInstanceName");
		UiAdapterFactory.instance.getEditItemInstanceNameUiAdapter(this).displayUi();
	}
	public void onEditSelectedItemInstancePrice() {
		Log.d(TAG, "onEditSelectedItemInstancePrice");
		UiAdapterFactory.instance.getEditItemInstancePriceUiAdapter(this).displayUi();
	}

	public void onRemoveSelectedItemInstance() throws SQLException {
		onRemoveItemInstance(selectedItemInstancePosition);
	}

	public void onPermanentlyRemoveSelectedItemInstance() throws SQLException {
		onPermanentlyRemoveItemInstance(selectedItemInstancePosition);
	}

	public CharSequence getSelectedItemInstanceName() throws SQLException {
		if (getCurrentShoppingList() == null) return "";
		
		ItemInstance itemInstance = getCurrentShoppingList().getItemInstance(selectedItemInstancePosition);
		if (itemInstance == null) return "";

		return itemInstance.getItem().getName();
	}
	public void setSelectedItemInstanceName(String value) throws SQLException {
		if (getCurrentShoppingList() == null) return;
		
		ItemInstance itemInstance = getCurrentShoppingList().getItemInstance(selectedItemInstancePosition);
		if (itemInstance == null) return;

		itemInstance.getItem().setName(value);
	}
	
	public Double getSelectedItemInstancePrice(BooleanResult isKnownPrice) {
		if (getCurrentShoppingList() == null) return null;
		
		ItemInstance itemInstance = getCurrentShoppingList().getItemInstance(selectedItemInstancePosition);
		if (itemInstance == null) return null;

		return itemInstance.getStoreItemPrice(isKnownPrice);
	}
	public void setSelectedItemInstancePrice(Double value) throws SQLException {
		if (getCurrentShoppingList() == null) return;
		
		ItemInstance itemInstance = getCurrentShoppingList().getItemInstance(selectedItemInstancePosition);
		if (itemInstance == null) return;

		itemInstance.setStoreItemPrice(value);
	}
	
	public void onButtonClickSaveItemsOrder() throws SQLException {
		// Check license and (if needed) display the message asking the user to purchase a license.
        LicenseManager licenseManager = LicenseManager.instance();
        if (!licenseManager.isLicensed()) {
            UiAdapterFactory.instance.getLicenseNeededUiAdapter(this).displayUi();
        	return;
        }

        // Otherwise, the user has purchased a license, so proceed normally.
        UiAdapterFactory.instance.getLearnShoppingOrderYesNoUiAdapter(this).displayUi();
	}

	SaveItemsOrderTask m_saveItemsOrderTask;
	public void saveItemsOrder(final boolean keepPickedUpItems) throws SQLException {
		if (getCurrentShoppingList() == null) return;

		// Get the UiAdapter that we will use to display the progress UI.
		final UiAdapter progressUi = UiAdapterFactory.instance.getSavingItemsOrderUiAdapter(this);

		// Create the AsyncTask that displays the progress UI, saves the items order, and closes the progress UI.
		m_saveItemsOrderTask = new SaveItemsOrderTask(getCurrentShoppingList(), keepPickedUpItems, progressUi);
		m_saveItemsOrderTask.execute((Void) null);
	}
	public void cancelSaveItemsOrder() {
		m_saveItemsOrderTask.cancel(true);
	}

	public String getTotalPickedUp() {
		if (getCurrentShoppingList() == null) return "";
		
		if(getCurrentShoppingList().getStore() == null) return "set store";

		// Get the total.
		Double value = getCurrentShoppingList().getTotalPickedUp();

		if (value == null) return "";
		NumberFormat baseFormat = NumberFormat.getCurrencyInstance();
		return baseFormat.format(value);
	}

	public String getTotal() {
		if (getCurrentShoppingList() == null) return "";
		
		if(getCurrentShoppingList().getStore() == null) return "set store";

		// Get the total.
		Double value = getCurrentShoppingList().getTotal();

		if (value == null) return "";
		NumberFormat baseFormat = NumberFormat.getCurrencyInstance();
		return baseFormat.format(value);
	}
	
	@Override
	public boolean onResult(SelectStorePm requestedPm, boolean isCanceled) {
		if (isCanceled) return false;

		Store selectedStore = requestedPm.getClickedStore();
		if (getCurrentShoppingList() != null) {
			getCurrentShoppingList().setStoreId(selectedStore.getId());
		}
		return true;
	}
	
	// Tracks all Stores.
	private EntityCollectionManager<Store> m_allStores = new EntityCollectionManager<>(StoreExtent.instance());

    /**
     * This class handles Item name change events.  It responds to them by notifying the ShoppingList's
     * INarrowCustomArrayAdapter<ItemInstance> that its data has changed.
     */
    private class ItemNameChangeHandler extends EntityObserver<Item> {
        INarrowCustomArrayAdapter<ItemInstance> m_orderedItemInstances;

        public void setOrderedItemInstances(INarrowCustomArrayAdapter<ItemInstance> orderedItemInstances) {
            m_orderedItemInstances = orderedItemInstances;
        }

        @Override
        public void open() { ItemExtent.instance().addObserver(this); }

        @Override
        public void close() { ItemExtent.instance().dropObserver(this); }

        @Override
        public void onUpdate(Item object, UpdateSet updates)
        {
            if(!updates.contains("name")) return;

            if(m_orderedItemInstances != null) {
                if(!isReferenceByShoppingList(object.getId())) return;
                m_orderedItemInstances.notifyDataSetChanged();
            }
        }

        /**
         * Determine if the specified Item ID is referenced by any of the ItemInstances in
         * m_orderedItemInstances.
         *
         * @param itemId The Item ID.
         * @return True if it is, otherwise false.
         */
        boolean isReferenceByShoppingList(UUID itemId) {
            for(ItemInstance itemInstance : m_orderedItemInstances.getObjects()) {
                if(itemId.equals(itemInstance.getItemId())) return(true);
            }
            return(false);
        }
    }
    private ItemNameChangeHandler m_itemNameChangeHandler = new ItemNameChangeHandler();

	@Override
	public void onLocationAvailable(Location location) {
		if (!isAutoSelectStore()) return;
		
		// Get all the Stores.
		Collection<Store> stores = m_allStores.getCollection();

		// Find the closest in-range Store.
		float smallestDistanceMeters = Float.MAX_VALUE;
		Store closestStore = null;
		for(Store store : stores) {
			if (store.getLatitude() == null || store.getLongitude() == null || store.getRadius() == null) continue;
			
			// Construct a Location from the Store's coordinates.
			Location storeLocation = new Location(location);
			storeLocation.setLatitude(store.getLatitude());
			storeLocation.setLongitude(store.getLongitude());

			// Compute the distance between the current device Location and the Store.
			float distance = location.distanceTo(storeLocation);
			if (distance < smallestDistanceMeters) {
				if (distance <= store.getRadius()) {
					smallestDistanceMeters = distance;
					closestStore = store;
				}
			}
		}

		// Set the closest in-range store.
		if (closestStore != null) {
			Log.d(TAG, "onLocationAvailable: closest store = " + closestStore.getName());
			getCurrentShoppingList().setStoreId(closestStore.getId());
		} else {
			Log.d(TAG, "onLocationAvailable: no store in range");
			getCurrentShoppingList().setStoreId(null);
		}

	}
}
