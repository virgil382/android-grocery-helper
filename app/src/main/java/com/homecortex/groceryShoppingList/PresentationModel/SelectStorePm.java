// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.PresentationModel;

import java.sql.SQLException;
import java.util.Collection;

import com.homecortex.groceryShoppingList.DataModel.Entities.Store;
import com.homecortex.groceryShoppingList.DataModel.Extents.StoreExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntityCollectionQueryResultHandler;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.Factory.UiAdapterFactory;

import android.util.Log;

public class SelectStorePm extends UiPm
{
	private static final String TAG = "SelectStorePm";
	
	private INarrowCustomArrayAdapter<Store> m_storesAdapter;
	private Store m_clickedStore;
	private boolean m_isCloseOnSelect = false;
	private Store m_selectedStore;
	
	public UiAdapter getActivityUiAdapter()
	{
		return (UiAdapterFactory.instance.getSelectStoreUiAdapter(this));
	}

	@Override
	public boolean dispatchResult(UiPm requesterPm, boolean isCanceled) {
		return requesterPm.onResult(this, isCanceled);
	}

	public void setClickedStore(Store value)
	{
		m_clickedStore = value;
	}
	
	public Store getClickedStore()
	{
		return m_clickedStore;
	}

	public void setCloseOnSelect(boolean isCloseOnSelect) {
		m_isCloseOnSelect = isCloseOnSelect;
	}

	public void setSelectedStore(Store value)
	{
		m_selectedStore = value;
	}
	
	public Store getSelectedStore()
	{
		return m_selectedStore;
	}

	/**
	 * Set the INarrowCustomArrayAdapter that contains Stores and fill it with Stores from the DB.
	 * @param storesAdapter The INarrowCustomArrayAdapter that contains Stores.
	 * @throws SQLException
	 */
	public void setStoresAdapter(INarrowCustomArrayAdapter<Store> storesAdapter) throws SQLException {
		final INarrowCustomArrayAdapter<Store> localStoresAdapter = storesAdapter;
		this.m_storesAdapter = storesAdapter;

		storesAdapter.clear();
		StoreExtent.instance().getAll(new IEntityCollectionQueryResultHandler<Store>() {
			@Override
			public void onResult(Collection<Store> stores) {
				for(Store store : stores) localStoresAdapter.add(store, false);
			}
		});
	}

	public void onClickStore(int position) {
		Log.d(TAG, "onClickStore");

		if (m_storesAdapter == null) return;

		Store selectedStore = m_storesAdapter.getObjects().get(position);
		setClickedStore(selectedStore);

		if(m_isCloseOnSelect) {
			// A store was clicked, so close the store selection activity.
			getActivityUiAdapter().closeUi();
		}
	}

	public void onLongClickStore(int position) {
		Log.d(TAG, "onLongClickStore");

		if (m_storesAdapter == null) return;

		Store selectedStore = m_storesAdapter.getObjects().get(position);
		setClickedStore(selectedStore);
		
		UiAdapterFactory.instance.getStorePopupMenuUiAdapter(this).displayUi();
	}

	public void onAddAnotherStore() {
		Log.d(TAG, "onAddAnotherStore");
		UiAdapterFactory.instance.getEnterNewStoreNameUiAdapter(this).displayUi();
	}

	public void addAnotherStore(String value) {
		if (m_storesAdapter == null) return;
		
		Store newStore = new Store(value);
		m_storesAdapter.add(newStore, false);
	}

	public void onEditClickedStoreName() {
		Log.d(TAG, "onEditClickedStoreName");
		UiAdapterFactory.instance.getEditNewStoreNameUiAdapter(this).displayUi();
	}

	public String getNameOfClickedStore() {
		Store clickedStore = getClickedStore();
		if (clickedStore == null) return "";
		return clickedStore.getName();
	}
	
	public void setNameOfClickedStore(String name) {
		if (m_storesAdapter == null) return;
		Store clickedStore = getClickedStore();
		if (clickedStore == null) return;

		clickedStore.setName(name);
		m_storesAdapter.notifyDataSetChanged();
	}

	public void onDeleteClickedStore() {
		Log.d(TAG, "onDeleteClickedStore");
		if (m_storesAdapter == null) return;
		Store doomedStore = getClickedStore();
		if (doomedStore == null) return;
		
		m_storesAdapter.remove(doomedStore);
		doomedStore.delete();
		setClickedStore(null);
	}

	public void onPickStoreCoordinatesWithMap(Store store) {
		Log.d(TAG, "onPickStoreCoordinatesWithMap: " + store.getName());
		setClickedStore(store);
		ApplicationPm.getSelectLocationPm().setMarkerInformation(store.getLatitude(), store.getLongitude(), store.getRadius(), store.getName());
		ApplicationPm.getSelectLocationPm().displayUi();
	}

	/**
	 * Handle the result of the SelectLocationPm.  Copy the coordinates into the
	 * clicked Store.
	 */
	@Override
	public boolean onResult(SelectLocationPm requestedPm, boolean isCanceled)
	{
		if (isCanceled) return false;

		if (m_storesAdapter == null) return false;
		Store clickedStore = getClickedStore();
		if (clickedStore == null) return false;

		Double lat = requestedPm.getLatitude();
		Double lng = requestedPm.getLongitude();
		Double radius = requestedPm.getRadius();

		clickedStore.setCoordinates(lat, lng, radius);
		m_storesAdapter.notifyDataSetChanged();
		
		return true;
	}
}
