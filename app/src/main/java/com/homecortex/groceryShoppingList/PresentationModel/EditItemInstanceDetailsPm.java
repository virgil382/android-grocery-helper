// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.PresentationModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import com.homecortex.groceryShoppingList.DataModel.Entities.ItemAttribute;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstance;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstanceAttribute;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstanceAttributeDesireIndicator;
import com.homecortex.groceryShoppingList.DataModel.Entities.UpdateSet;
import com.homecortex.groceryShoppingList.DataModel.Entities.VolumeUnit;
import com.homecortex.groceryShoppingList.DataModel.Entities.WeightUnit;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemAttributeExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemInstanceAttributeExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.VolumeUnitExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.WeightUnitExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntityCollectionQueryResultHandler;
import com.homecortex.groceryShoppingList.DataModel.EntityObserver;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.Factory.UiAdapterFactory;
import com.homecortex.groceryShoppingList.Util.ObjectUtils;
import com.homecortex.groceryShoppingList.WheelWidget.adapters.ArrayWheelAdapter;

public class EditItemInstanceDetailsPm extends UiPm
{
	private ItemInstance m_itemInstance;

	public EditItemInstanceDetailsPm() {
        // The following 2 mechanisms only react to create/update/delete, so it's OK to open
        // them once here.  I.e. the open method does not initialize any data structure.
		m_privateAttributeObserver.open();
		m_itemInstanceAttributeObserver.open();
	}
	
	@Override
	public UiAdapter getActivityUiAdapter() {
		return (UiAdapterFactory.instance.getEditItemInstanceDetailsUiAdapter(this));
	}

	public void setItemInstance(ItemInstance itemInstance) {
		m_itemInstance = itemInstance;
	}

	public String getItemInstanceDescription() {
		return m_itemInstance.toString();
	}
	
	// Quantity control

	ArrayList<String> quantityValues;
	public List<String> getQuantities() {
		if (quantityValues != null) return quantityValues;
		
		quantityValues = new ArrayList<>();
		for(Integer i = 1; i <= 100; i++) {
			quantityValues.add(i.toString());
		}
		return quantityValues;
	}

	public boolean isQuantityEnabled() {
		return m_itemInstance.getHasCount();
	}

	public void isQuantityEnabled(boolean val) {
		if (m_itemInstance.getHasCount() == val) return;
		
		m_itemInstance.setHasCount(val);
	}

	public void setQuantityIndex(int index)
	{
		String quantityString = getQuantities().get(index);
		int quantity = Integer.valueOf(quantityString);
		
		if (m_itemInstance.getCount() == quantity) return;
		
		m_itemInstance.setCount(quantity);
	}
	
	public int getQuantityIndex() {
		int closestIndex = 0;
		double smallestDistance = Integer.MAX_VALUE;
		
		for(int i = 0; i < getQuantities().size(); i++) {
			String quantityString = getQuantities().get(i);
			double quantity = Integer.valueOf(quantityString);
			double distance = Math.abs(m_itemInstance.getCount() - quantity);
			
			if (distance < smallestDistance) {
				smallestDistance = distance;
				closestIndex = i;
			}
		}
		
		return closestIndex;
	}
	
	// Volume control
	
	ArrayList<String> volumeValues;
	public List<String> getVolumes() {
		if (volumeValues != null) return volumeValues;

		volumeValues = new ArrayList<>();
		for(Float i = 0.5f; i <= 32.0f; i += 0.25f) {
			volumeValues.add(String.format("%.2f", i));
		}
		return volumeValues;
	}

	ArrayWheelAdapter<VolumeUnit> volumeUnitsAdapter;
	private EntityObserver<VolumeUnit> m_volumeUnitsManager = new EntityObserver<VolumeUnit>() {
		@Override
		public void open() {
			VolumeUnitExtent.instance().addObserver(this);
			VolumeUnitExtent.instance().getAll(new IEntityCollectionQueryResultHandler<VolumeUnit>() {
				@Override
				public void onResult(Collection<VolumeUnit> objects) {
					for(VolumeUnit object : objects) volumeUnitsAdapter.add(object); 
				}
			});
		}
		@Override
		public void close() { VolumeUnitExtent.instance().dropObserver(this); }
		@Override
		public void onCreate(VolumeUnit object) { volumeUnitsAdapter.add(object); }
		@Override
		public void onDelete(VolumeUnit object) { volumeUnitsAdapter.remove(object); }
	};

	public void setVolumeUnitsAdapter(ArrayWheelAdapter<VolumeUnit> adapter) {
		m_volumeUnitsManager.close();
		volumeUnitsAdapter = adapter;
		m_volumeUnitsManager.open();
	}
	
	public List<VolumeUnit> getVolumeUnits() {
		return(volumeUnitsAdapter.getItems());
	}

	public boolean isVolumeEnabled() {
		return m_itemInstance.getHasVolume();
	}

	public void isVolumeEnabled(boolean val) {
		if (m_itemInstance.getHasVolume() == val) return;
		
		m_itemInstance.setHasVolume(val);
	}

	public void setVolumeIndex(int index)
	{
		String volumeString = getVolumes().get(index);
		float volume = Float.valueOf(volumeString);
		
		if (m_itemInstance.getVolumeMagnitude() == volume) return;
		
		m_itemInstance.setVolumeMagnitude(volume);
	}

	public int getVolumeIndex() {
		int closestIndex = 0;
		double smallestDistance = Integer.MAX_VALUE;
		
		for(int i = 0; i < getVolumes().size(); i++) {
			String volumeString = getVolumes().get(i);
			float volume = Float.valueOf(volumeString);
			float distance = Math.abs(m_itemInstance.getVolumeMagnitude() - volume);
			
			if (distance < smallestDistance) {
				smallestDistance = distance;
				closestIndex = i;
			}
		}
		
		return closestIndex;
	}

	public void setVolumeUnitIndex(int index) {
		VolumeUnit selectedVolumeUnit = getVolumeUnits().get(index);
		m_itemInstance.setVolumeUnitId(selectedVolumeUnit.getId());
	}

	public int getVolumeUnitIndex() {
		for(int i = 0; i < getVolumeUnits().size(); i++) {
			VolumeUnit volumeUnit = getVolumeUnits().get(i);
			VolumeUnit itemVolumeUnit = m_itemInstance.getVolumeUnit();
			if (itemVolumeUnit == null) return 0;
			
			if(ObjectUtils.equals(itemVolumeUnit.getId(), volumeUnit.getId())) return(i);
		}
		
		return 0;
	}

	// Weight control
	
	public List<String> getWeights() {
		ArrayList<String> values = new ArrayList<>();
		
		for(Integer i = 1; i <= 50; i++) {
			values.add(i.toString());
		}
		
		return values;
	}

	ArrayWheelAdapter<WeightUnit> weightUnitsAdapter;
	private EntityObserver<WeightUnit> m_weightUnitsManager = new EntityObserver<WeightUnit>() {
		@Override
		public void open() {
			WeightUnitExtent.instance().addObserver(this);
			WeightUnitExtent.instance().getAll(new IEntityCollectionQueryResultHandler<WeightUnit>() {
				@Override
				public void onResult(Collection<WeightUnit> objects) {
					for(WeightUnit object : objects) weightUnitsAdapter.add(object); 
				}
			});
		}
		@Override
		public void close() { WeightUnitExtent.instance().dropObserver(this); }
		@Override
		public void onCreate(WeightUnit object) { weightUnitsAdapter.add(object); }
		@Override
		public void onDelete(WeightUnit object) { weightUnitsAdapter.remove(object); }
	};

	public void setWeightUnitsAdapter(ArrayWheelAdapter<WeightUnit> adapter) {
		m_weightUnitsManager.close();
		weightUnitsAdapter = adapter;
		m_weightUnitsManager.open();
	}
	
	public List<WeightUnit> getWeightUnits() {
		return(weightUnitsAdapter.getItems());
	}
	
	public boolean isWeightEnabled() {
		return m_itemInstance.getHasWeight();
	}

	public void isWeightEnabled(boolean val) {
		if (m_itemInstance.getHasWeight() == val) return;
		
		m_itemInstance.setHasWeight(val);
	}

	public void setWeightIndex(int index)
	{
		String weightString = getWeights().get(index);
		float weight = Float.valueOf(weightString);
		
		if (m_itemInstance.getWeightMagnitude() == weight) return;
		
		m_itemInstance.setWeightMagnitude(weight);
	}

	public int getWeightIndex() {
		int closestIndex = 0;
		double smallestDistance = Integer.MAX_VALUE;
		
		for(int i = 0; i < getWeights().size(); i++) {
			String weightString = getWeights().get(i);
			float weight = Float.valueOf(weightString);
			float distance = Math.abs(m_itemInstance.getWeightMagnitude() - weight);
			
			if (distance < smallestDistance) {
				smallestDistance = distance;
				closestIndex = i;
			}
		}
		
		return closestIndex;
	}

	public void setWeightUnitIndex(int index) {
		WeightUnit selectedWeightUnit = getWeightUnits().get(index);
		m_itemInstance.setWeightUnitId(selectedWeightUnit.getId());
	}

	public int getWeightUnitIndex() {
		for(int i = 0; i < getWeightUnits().size(); i++) {
			WeightUnit weightUnit = getWeightUnits().get(i);
			WeightUnit itemWeightUnit = m_itemInstance.getWeightUnit();
			if (itemWeightUnit == null) return 0;
			
			if(ObjectUtils.equals(itemWeightUnit.getId(), weightUnit.getId())) return(i);
		}
		
		return 0;
	}

	// Attributes control

	INarrowCustomArrayAdapter<ItemInstanceAttributeDesireIndicator> m_attributes;
	public void setItemAttributes(INarrowCustomArrayAdapter<ItemInstanceAttributeDesireIndicator> attributes) throws SQLException
	{
		m_attributes = attributes;

		ItemAttributeExtent.instance().getByFieldValue("itemId", m_itemInstance.getItemId(), new IEntityCollectionQueryResultHandler<ItemAttribute>() {
			@Override
			public void onResult(Collection<ItemAttribute> itemAttributes) {
				// Create an ItemInstanceAttributeDesireIndicator for each ItemAttribute.
				for(ItemAttribute itemAttribute : itemAttributes) {
					ItemInstanceAttributeDesireIndicator indicator =
						new ItemInstanceAttributeDesireIndicator(m_itemInstance.getId(), itemAttribute);
					m_attributes.add(indicator, false);
				}

                // For each ItemInstanceAttribute, mark as "desired" the corresponding ItemInstanceAttributeDesireIndicator created above.
                ItemInstanceAttributeExtent.instance().getByFieldValue("itemInstanceId", m_itemInstance.getId(), new IEntityCollectionQueryResultHandler<ItemInstanceAttribute>() {
                    @Override
                    public void onResult(Collection<ItemInstanceAttribute> itemInstanceAttributes) {
                        for(ItemInstanceAttribute itemInstanceAttribute : itemInstanceAttributes) {
							ItemInstanceAttributeDesireIndicator desireIndicator = findItemInstanceAttributeDesireIndicatorByItemAttributeId(
									itemInstanceAttribute.getItemAttributeId());
                            if (desireIndicator == null) continue;  // should not happen
                            desireIndicator.isDesired(itemInstanceAttribute);
                        }

                        notifyAttributesChanged();
                    }
                });
			}
		});
	}

//	private ItemInstanceAttributeDesireIndicator findItemInstanceAttributeDesireIndicatorByItemInstanceId(UUID itemInstanceId) {
//		if (m_attributes == null) return(null);
//
//		for(ItemInstanceAttributeDesireIndicator desireIndicator : m_attributes.getObjects()) {
//			if(itemInstanceId.equals(desireIndicator.getItemInstanceId())) return(desireIndicator);
//		}
//		return(null);
//	}
	
	private ItemInstanceAttributeDesireIndicator findItemInstanceAttributeDesireIndicatorByItemAttributeId(UUID itemAttributeId) {
		if (m_attributes == null) return(null);
		
		for(ItemInstanceAttributeDesireIndicator desireIndicator : m_attributes.getObjects()) {
			if(itemAttributeId.equals(desireIndicator.getItemAttributeId())) return(desireIndicator);
		}
		return(null);
	}
	
	public void notifyAttributesChanged() {
		if (m_attributes != null)
			m_attributes.notifyDataSetChanged();
	}

	public void onAddAnotherItemAttribute() {
		UiAdapterFactory.instance.getEnterNewItemAttributeNameUiAdapter(this).displayUi();
		// will call addAnotherItemAttribute()
	}

	public void addAnotherItemAttribute(String name) {
		ItemAttribute newItemAttribute = new ItemAttribute(name, m_itemInstance.getItemId(), true, 0);

        // Also create an ItemInstanceAttribute linking the current ItemInstance and the
        // newly created ItemAttribute.
        new ItemInstanceAttribute(newItemAttribute.getId(), m_itemInstance.getId());
    }

    /**
     * The ItemAttributeObserver monitors ItemAttribute creations/deletions and adds/deletes
     * corresponding ItemInstanceAttributeDesireIndicators.
     */
	private class ItemAttributeObserver extends EntityObserver<ItemAttribute> {
		@Override
		public void open() { ItemAttributeExtent.instance().addObserver(this); }
		@Override
		public void close() { ItemAttributeExtent.instance().dropObserver(this); }

		// A new ItemAttribute was created.  We create a corresponding ItemInstanceAttributeDesireIndicator.
		@Override
		public void onCreate(ItemAttribute object) {
			if(!m_itemInstance.getItemId().equals(object.getItemId())) return;

			ItemInstanceAttributeDesireIndicator indicator = new ItemInstanceAttributeDesireIndicator(m_itemInstance.getId(), object);
			m_attributes.add(indicator, false);

			notifyAttributesChanged();
		}

		@Override
		public void onUpdate(ItemAttribute object, UpdateSet updates) {
			if(!m_itemInstance.getItemId().equals(object.getItemId())) return;
			notifyAttributesChanged();
		}

		@Override
		public void onDelete(ItemAttribute object) {
			// Remove the corresponding ItemInstanceAttributeDesireIndicator
			ItemInstanceAttributeDesireIndicator desireIndicator = findItemInstanceAttributeDesireIndicatorByItemAttributeId(object.getId());
			if (desireIndicator == null) return;  // should not happen
			m_attributes.remove(desireIndicator);
			notifyAttributesChanged();
		}
	}
	private ItemAttributeObserver m_privateAttributeObserver = new ItemAttributeObserver();

	public void onLongClickItemAttribute(int position) {
		m_attributes.setSelection(position);
		UiAdapterFactory.instance.getItemInstanceAttributePopupMenuUiAdapter(this).displayUi();	
	}

	public void onClickItemAttribute(int position) {
		m_attributes.setSelection(position);
		UiAdapterFactory.instance.getEditItemAttributeNameUiAdapter(this).displayUi();
	}

	public void onEditItemAttributeName() {
		UiAdapterFactory.instance.getEditItemAttributeNameUiAdapter(this).displayUi();
	}

	public String getSelectedAttributeName() {
		ItemInstanceAttributeDesireIndicator selection = m_attributes.getSelection();
		if (selection == null) return null;
		return selection.getName();
	}
	
	public void setSelectedAttributeName(String value) {
		ItemInstanceAttributeDesireIndicator selection = m_attributes.getSelection();
		if (selection == null) return;
		selection.setName(value);
	}

	public void onDeleteItemAttribute() throws SQLException {
		ItemInstanceAttributeDesireIndicator selection = m_attributes.getSelection();
		if (selection == null) return;
		
		selection.delete();
		// this will call onDelete(ItemAttribute)
		// this will call onDelete(ItemInstanceAttribute)
		
		m_attributes.remove(selection);
	}

	private class ItemInstanceAttributeObserver extends EntityObserver<ItemInstanceAttribute> {
		public void open() { ItemInstanceAttributeExtent.instance().addObserver(this); }

		@Override
		public void close() { ItemInstanceAttributeExtent.instance().dropObserver(this); }

		@Override
		public void onCreate(ItemInstanceAttribute object) {
			// Set the corresponding ItemInstanceAttributeDesireIndicator to desired.
			ItemInstanceAttributeDesireIndicator desireIndicator = findItemInstanceAttributeDesireIndicatorByItemAttributeId(object.getItemAttributeId());
			
			if (desireIndicator == null) return;  // should not happen
			
			desireIndicator.isDesired(object);
			notifyAttributesChanged();
		}

		@Override
		public void onUpdate(ItemInstanceAttribute object, UpdateSet updates) { }

		@Override
		public void onDelete(ItemInstanceAttribute object) {
			// Set the corresponding ItemInstanceAttributeDesireIndicator to undesired.
			ItemInstanceAttributeDesireIndicator desireIndicator = findItemInstanceAttributeDesireIndicatorByItemAttributeId(object.getItemAttributeId());
			if (desireIndicator == null) return;  // should not happen
			desireIndicator.isDesired(null);
			notifyAttributesChanged();
		}
	}
	private ItemInstanceAttributeObserver m_itemInstanceAttributeObserver = new ItemInstanceAttributeObserver();
	

	// Finalization
	
	@Override
	public boolean dispatchResult(UiPm requesterPm, boolean isCanceled) {
		try {
			//updateItemInstanceAttributes();
			proposeDefaultValuesInCorrespondingItem();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return requesterPm.onResult(this, isCanceled);
	}

	/*
	public void updateItemInstanceAttributes() throws SQLException {
		for(ItemInstanceAttributeDesireIndicator attributeDesirability : m_attributes.getObjects()) {
			boolean isPresent = m_itemInstance.hasItemAttribute(attributeDesirability.getItemAttribute());
			if (attributeDesirability.isDesired() && !isPresent) {
				new ItemInstanceAttribute(attributeDesirability.getItemAttribute(), m_itemInstance);
				continue;
			}
			if (!attributeDesirability.isDesired() && isPresent) {
				ItemInstanceAttribute itemInstanceAttribute =
					m_itemInstance.getItemInstanceAttributeFor(attributeDesirability.getItemAttribute());
				itemInstanceAttribute.delete();
				continue;
			}
		}
	}
	*/

	/**
	 * Propose default values in the Item associated with the ItemInstance whose details/attributes
	 * were modified via this PM.
	 * 
	 * @throws SQLException
	 */
	private void proposeDefaultValuesInCorrespondingItem() throws SQLException {
		if (m_itemInstance == null) return;
		m_itemInstance.proposeDefaultValuesInCorrespondingItem();
	}
}
