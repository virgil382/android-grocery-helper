// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.PresentationModel;

import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.Factory.UiAdapterFactory;

public class SelectLocationPm extends UiPm {
	private IMapAdapter mMapAdapter;
	Double mLatitude;
	Double mLongitude;
	Double mRadius;
	String mMarkerName;

	@Override
	public UiAdapter getActivityUiAdapter() {
		return (UiAdapterFactory.instance.getSelectLocationUiAdapter(this));
	}

	@Override
	public boolean dispatchResult(UiPm requesterPm, boolean isCanceled) {
		return requesterPm.onResult(this, isCanceled);
	}

	public double getRadius() {
		if (mRadius != null) return mRadius;
		return 100;
	}
	
	public String getMarkerName() {
		if (mMarkerName != null) return mMarkerName;
		return "Store";
	}
	
	public void setInitialCoordinates(Double lat, Double lng) {
		// If the IMapAdapter is not yet set, then do nothing.
		if (mMapAdapter == null) return;

		// If the user already clicked to set a marker, then don't change the camera location.
		if (mLatitude != null) return;
		if (mLongitude != null) return;
		
		// Else, change the camera location to the specified coordinates.
		mMapAdapter.setCameraLocation(lat, lng);
	}
		
	public void setMarkerInformation(Double lat, Double lng, Double radius, String markerName) {
		mLatitude = lat;
		mLongitude = lng;
		mRadius = radius;
		mMarkerName = markerName;
		if (mMapAdapter != null && lat != null && lng != null && radius != null) {
			mMapAdapter.setMarker(lat, lng, getRadius(), getMarkerName());
			mMapAdapter.setCameraLocation(lat, lng);
		}
	}

	public void setMapAdapter(IMapAdapter map) {
		mMapAdapter = map;
		if(mLatitude != null && mLongitude != null) {
			mMapAdapter.setMarker(mLatitude, mLongitude, getRadius(), getMarkerName());
			mMapAdapter.setCameraLocation(mLatitude, mLongitude);
		}
	}

	public void onMapClick(double lat, double lng, double radius) {
		if (mMapAdapter != null) {
			mMapAdapter.setMarker(lat, lng, radius, getMarkerName());
		}
		mLatitude = lat;
		mLongitude = lng;
		mRadius = radius;
	}
	
	public void setRadius(int radius) {
		mRadius = (double) radius;
		if (mMapAdapter != null) mMapAdapter.setRadius(radius);
	}
	public Double getLatitude() { return mLatitude; }
	public Double getLongitude() { return mLongitude; }

	public void onButtonClickOk() {
		getActivityUiAdapter().closeUi();
	}
}
