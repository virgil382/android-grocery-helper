// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.PresentationModel;

import java.util.Collection;

import com.homecortex.groceryShoppingList.DataModel.Entities.ShoppingList;
import com.homecortex.groceryShoppingList.DataModel.Entities.Store;
import com.homecortex.groceryShoppingList.DataModel.Extents.ShoppingListExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntityCollectionQueryResultHandler;
import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.Factory.UiAdapterFactory;

import android.util.Log;

public class MainScreenPm extends UiPm
{
	private static final String TAG = "MainScreenPm";

	INarrowCustomArrayAdapter<ShoppingList> m_shoppingListsAdapter;

	@Override
	public UiAdapter getActivityUiAdapter() {
		return (UiAdapterFactory.instance.getMainScreenUiAdapter(this));
	}

	@Override
	public boolean dispatchResult(UiPm requesterPm, boolean isCanceled) {
		return requesterPm.onResult(this, isCanceled);
	}

	public void setShoppingListsAdapter(INarrowCustomArrayAdapter<ShoppingList> customArrayAdapter) {
		final INarrowCustomArrayAdapter<ShoppingList> localAdapter = customArrayAdapter;
		this.m_shoppingListsAdapter = customArrayAdapter;

		m_shoppingListsAdapter.clear();
		ShoppingListExtent.instance().getAll(new IEntityCollectionQueryResultHandler<ShoppingList>() {
			@Override
			public void onResult(Collection<ShoppingList> objects) {
				for(ShoppingList object : objects) localAdapter.add(object, false);
			}
		});
	}
	
	/**
	 * Handle an event from the UI layer indicating that the user clicked on a {@link ShoppingList}.
	 * 
	 * @param position The o-indexed position of the {@link ShoppingList}.
	 */
	public void onClickShoppingList(int position)
	{
		m_shoppingListsAdapter.setSelection(position);

		// Get the selected shopping list.
		ShoppingList selectedShoppingList = m_shoppingListsAdapter.getSelection();
		
		// Set it into the EditShoppingListPm and display its UI.
		EditShoppingListPm editShoppingListPm = ApplicationPm.getEditShoppingListPm();
		editShoppingListPm.setCurrentShoppingList(selectedShoppingList);
		editShoppingListPm.displayUi();
	}
	
	private void setSelectedShoppingList(ShoppingList shoppingList)
	{
		m_shoppingListsAdapter.setSelection(shoppingList);

		if (shoppingList == null)
			Log.d(TAG, "setSelectedShoppingList: null");
		else
			Log.d(TAG, "setSelectedShoppingList: " + shoppingList.getName());
	}

	/**
	 * Handle an event from the UI layer indicating that the user long-clicked on a
	 * {@link ShoppingList}.
	 * 
	 * @param position The o-indexed position of the {@link ShoppingList}.
	 */
	public void onLongClickShoppingList(int position)
	{
		m_shoppingListsAdapter.setSelection(position);
		UiAdapterFactory.instance.getShoppingListPopupMenuUiAdapter(this).displayUi();
	}
	
	/**
	 * Handle an event from the UI layer indicating that the wishes to rename the
	 * selected {@link ShoppingList}.
	 */
	public void onEditSelectedShoppingListName() {
		Log.d(TAG, "onEditShoppingListName");
		UiAdapterFactory.instance.getEditShoppingListNameUiAdapter(this).displayUi();
	}
	
	public String getSelectedShoppingListName()
	{
		ShoppingList selectedShoppingList = m_shoppingListsAdapter.getSelection();
		if (selectedShoppingList == null) return null;
		return selectedShoppingList.getName();
	}
	
	public void setSelectedShoppingListName(String name) {
		Log.d(TAG, "renameSelectedShoppingList");

		ShoppingList selectedShoppingList = m_shoppingListsAdapter.getSelection();
		if (selectedShoppingList == null) return;

		selectedShoppingList.setName(name);
		m_shoppingListsAdapter.notifyDataSetChanged();
	}
	
	/**
	 * Handle an event from the UI layer indicating that the wishes to delete the
	 * selected {@link ShoppingList}.
	 */
	public void onDeleteSelectedShoppingList(){
		Log.d(TAG, "onDeleteSelectedShoppingList");

		ShoppingList doomedShoppingList = m_shoppingListsAdapter.getSelection();
		if (doomedShoppingList == null) return;
		
		m_shoppingListsAdapter.remove(doomedShoppingList);
		setSelectedShoppingList(m_shoppingListsAdapter.getSelection());
		
		// Delete the doomedShoppingList ShoppingList from the DB.
		doomedShoppingList.delete();
	}

	/**
	 * Handle an event from the UI layer indicating that the wishes to add another
	 * {@link ShoppingList}.
	 */
	public void onAddAnotherShoppingList() {
		Log.d(TAG, "onAddAnotherShoppingList");
		UiAdapterFactory.instance.getEnterNewShoppingListNameUiAdapter(this).displayUi();
	}
	
	public void addAnotherShoppingList(String name) {
		Log.d(TAG, "addAnotherShoppingList");
		
		ShoppingList newShoppingList = new ShoppingList(name);
		m_shoppingListsAdapter.add(newShoppingList, true);
	}

	/**
	 * Handle an event from the UI layer indicating that the wishes to manage the
	 * {@link Store}s at which he has shopped in the past.
	 */
	public void onButtonClickManageStores() {
		Log.d(TAG, "onButtonClickManageStores");
		ApplicationPm.getSelectStorePm().setCloseOnSelect(false);
		ApplicationPm.getSelectStorePm().setSelectedStore(null);
		ApplicationPm.getSelectStorePm().displayUi();
	}

	/**
	 * Handle an event from the UI layer indicating that the wishes to manage the
	 * Items that he has created in the past.
	 */
	public void onButtonClickManageAllItems() {
		Log.d(TAG, "onButtonClickManageAllItems");
		// TODO: Use the appropriate UiAdapter to display the UI.
	}
}
