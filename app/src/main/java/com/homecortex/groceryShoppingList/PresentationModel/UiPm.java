// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.PresentationModel;

import com.homecortex.groceryShoppingList.UiAdapters.UiAdapter;


/**
 * An <code>UiPm</code> is a Presentation Model (see Fowler, M.) that encapsulates the
 * presentation, behavior, and state of an Android <code>Activity</code> independently of the UI.  
 * A concrete <code>UiPm</code> has a corresponding {@link UiAdapter} with which only it 
 * interacts (i.e. no other Presentation Model components may interact with it).  However, concrete 
 * <code>UiPm</code>s may interact with each other.
 */
public abstract class UiPm
{
	public abstract UiAdapter getActivityUiAdapter();
	
	/**
	 * Display the screen corresponding to the concrete <code>IScreenPm</code>.  This implementation
	 * calls the concrete <code>IScreenPm</code>'s corresponding <code>UiAdapter</code>, but concrete
	 * <code>IScreenPm</code>s may override this behavior and execute additional logic.
	 */
	public void displayUi()
	{
		getActivityUiAdapter().displayUi();
	}

	/**
	 * Close the screen corresponding to the concrete <code>IScreenPm</code>.  This implementation
	 * calls the concrete <code>IScreenPm</code>'s corresponding <code>UiAdapter</code>, but concrete
	 * <code>IScreenPm</code>s may override this behavior and execute additional logic.
	 */
	public void closeUi()
	{
		getActivityUiAdapter().closeUi();
	}

	/**
	 * Notify the requester <code>UiPm</code> that this <code>UiPm</code> has a result ready. 
	 * @param requesterPm The <code>UiPm</code> that to be notified.
	 * @param isCanceled True indicates that this <code>UiPm</code> does not have a result ready
	 * because the Activity was canceled, otherwise false.
	 * @return 
	 */
	public abstract boolean dispatchResult(UiPm requesterPm, boolean isCanceled);
	
	/**
	 * Handle a result from the {@link EditShoppingListPm}.
	 * @param requestedPm The {@link EditShoppingListPm} that contains the result.
	 * @param isCanceled True indicates that the {@link EditShoppingListPm} Activity was canceled,
	 * otherwise false.
	 */
	public boolean onResult(EditShoppingListPm requestedPm, boolean isCanceled) { return false; }

	/**
	 * Handle a result from the {@link SelectStorePm}.
	 * @param requestedPm The {@link SelectStorePm} that contains the result.
	 * @param isCanceled True indicates that the {@link SelectStorePm} Activity was canceled,
	 * otherwise false.
	 * @return 
	 */
	public boolean onResult(SelectStorePm requestedPm, boolean isCanceled) { return false; }

	/**
	 * Handle a result from the {@link MainScreenPm}.
	 * @param requestedPm The {@link MainScreenPm} that contains the result.
	 * @param isCanceled True indicates that the {@link MainScreenPm} Activity was canceled,
	 * otherwise false.
	 */
	public boolean onResult(MainScreenPm requestedPm, boolean isCanceled) { return false; }

	/**
	 * Handle a result from the {@link SelectLocationPm}.
	 * @param requestedPm The {@link SelectLocationPm} that contains the result.
	 * @param isCanceled True indicates that the {@link SelectLocationPm} Activity was canceled,
	 * otherwise false.
	 */
	public boolean onResult(SelectLocationPm requestedPm, boolean isCanceled) { return false; }

	/**
	 * Handle a result from the {@link EditItemInstanceDetailsPm}.
	 * @param requestedPm The {@link EditItemInstanceDetailsPm} that contains the result.
	 * @param isCanceled True indicates that the {@link EditItemInstanceDetailsPm} Activity was canceled,
	 * otherwise false.
	 */
	public boolean onResult(EditItemInstanceDetailsPm requestedPm, boolean isCanceled) { return false; }
}
