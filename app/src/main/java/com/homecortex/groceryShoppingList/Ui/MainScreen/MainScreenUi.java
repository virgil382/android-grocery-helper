// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui.MainScreen;

import java.util.ArrayList;

import com.homecortex.groceryShoppingList.DataModel.AndroidDatabaseHelper;
import com.homecortex.groceryShoppingList.DataModel.DatabaseHelper;
import com.homecortex.groceryShoppingList.DataModel.Entities.ShoppingList;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.MainScreenPm;
import com.homecortex.groceryShoppingList.Ui.CustomArrayAdapter;
import com.homecortex.groceryShoppingList.Util.DatabaseBackup;
import com.homecortex.groceryShoppingList.Util.DebugSettings;
import com.homecortex.groceryShoppingList.Util.ErrorReporter;
import com.homecortex.groceryShoppingList.Util.LicenseManager;
import com.homecortex.groceryShoppingList.R;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MainScreenUi extends Activity implements OnItemClickListener, OnItemLongClickListener
{
	private MainScreenPm m_pm;

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainscreen);

        // Add error capture and reporting.
        ErrorReporter errReporter = new ErrorReporter();
        errReporter.Init(this);
        errReporter.CheckErrorAndSendMail(this);

    	if (DebugSettings.m_showAppVersionToasts) displayVersion();

        // Perform a license check and get the amount of time elapsed since first run.
        LicenseManager.initialize(this);
        long millis = LicenseManager.instance().getMillisSinceFirstRun();

        // Perform a database save/restore.
        DatabaseBackup dbk = new DatabaseBackup(getPackageName(), DatabaseHelper.DATABASE_NAME);
        if (millis == 0) {
            if (dbk.restore()) {
            	if (DebugSettings.m_showDbBackupRestoreToasts) Toast.makeText(this, "DB Restored!", Toast.LENGTH_LONG).show();
            }
        } else {
            if (dbk.isBackupEnabled() && dbk.backup()) {
        		if(DebugSettings.m_showDbBackupRestoreToasts) Toast.makeText(this, "DB Exported!", Toast.LENGTH_LONG).show();
            }
        }

        // Set the IDatabaseHelper into the ApplicationPm (if not already set).
        if (ApplicationPm.getDatabaseHelper() == null)
        	ApplicationPm.setDatabaseHelper(new AndroidDatabaseHelper(this));

        m_pm = ApplicationPm.getMainScreenPm();

        // Inflate the View for the additional row in the ListView, and set its text.
		View extraRowView = getLayoutInflater().inflate(R.layout.list_item_addanotheritem, null);
		Button addButton = (Button) extraRowView.findViewById(R.id.buttonAddAnotherRow);
		addButton.setText(R.string.add_another_list);

		// Use a CustomAdapter along with the ShoppingListAdapter to bind the ListView
        // to the List containing ShoppingLists.  Tell it to include the extra row View.
		CustomArrayAdapter<ShoppingList, ShoppingListAdapter> customArrayAdapter =
			new CustomArrayAdapter<>(this, new ArrayList<ShoppingList>(), ShoppingListAdapter.class, extraRowView);
        m_pm.setShoppingListsAdapter(customArrayAdapter);

        // Bind the ListView to an item click handler and to the Adapter.
		ListView listViewShoppingLists = (ListView) findViewById(R.id.listViewShoppingLists);
		listViewShoppingLists.setOnItemClickListener(this);
		listViewShoppingLists.setOnItemLongClickListener(this);
        listViewShoppingLists.setAdapter(customArrayAdapter);
    }

    /**
     * Display the application's code version and version name in a toast.
     */
    private void displayVersion() {
    	PackageManager manager = this.getPackageManager();
    	PackageInfo info;
		try {
			info = manager.getPackageInfo(this.getPackageName(), 0);
	    	Toast.makeText(this,
	       	     "Version " + info.versionName + "(" + info.versionCode + ")", Toast.LENGTH_LONG).show();
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    /**
     * React to the user selecting a row in the ListView.
     */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		ApplicationPm.m_currentActivity = this;
		m_pm.onClickShoppingList(position);
	}

    /**
     * React to the user long-clicking a row in the ListView by displaying a popup menu
     * showing the operations that the user may perform on the selected ShoppingList.
     */
	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
	{
		ApplicationPm.m_currentActivity = this;
		m_pm.onLongClickShoppingList(position);
		return true; // consume the long click event
	}

    /**
     * React to the user clicking the button in the last row of the ListView
     * (to add a new ShoppingList).
     */
	public void onButtonAddAnotherRow(View v) {
		ApplicationPm.m_currentActivity = this;
		m_pm.onAddAnotherShoppingList();
	}

    /**
     * Handle the clicking of buttonManageStores.  The binding of the button to
     * this method is specified in mainscreen.xml.
     * @param v The Button that was clicked.
     */
	public void onButtonClickManageStores(View v) {
		ApplicationPm.m_currentActivity = this;
		m_pm.onButtonClickManageStores();
	}

    /**
     * Handle the clicking of buttonManageAllItems.  The binding of the button to
     * this method is specified in mainscreen.xml.
     * @param v The Button that was clicked.
     */
	public void onButtonClickManageAllItems(View v) {
		ApplicationPm.m_currentActivity = this;
		m_pm.onButtonClickManageAllItems();
	}
}
