// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui.SelectStore;

import java.text.DecimalFormat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.homecortex.groceryShoppingList.DataModel.Entities.Store;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.Ui.EntityAdapter;
import com.homecortex.groceryShoppingList.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * <p>A <code>StoreAdapter</code> is an {@link EntityAdapter} that creates a
 * {@link View} by inflating <code>list_item_store.xml</code> and that renders the
 * name of a {@link Store} into the {@link TextView} field contained within the
 * {@link View}.
 * 
 * <p>Also see <a href="http://www.lepus.org.uk/ref/companion/Flyweight.xml">Flyweight
 * Design Pattern</a>:  The <code>StoreAdapter</code> plays the role of a
 * <code>Concrete Flyweight</code> whose intrinsic state consists of references to the
 * UI elements of the {@link View} that it inflates from <code>list_item_store.xml</code>,
 * and whose extrinsic state is a {@link Store} passed to its 
 * <code>renderAdaptee()</code> method.
 */
public class StoreAdapter extends EntityAdapter<Store>
{
	private TextView textView;
	private TextView textViewStoreCoordinates;
	private ImageView imageViewSetCoordinates;

	/**
	 * Create a new {@link View} used by a ListView to render a row by inflating
	 * <code>list_item_store.xml</code>.  Store references to the {@link View} 
	 * LinerLayout and {@link TextView} so that at a later time
	 * <code>renderAdaptee()</code> may render a {@link Store} into the {@link View}.
	 * 
	 * @param activity The {@link Activity} via which this method may obtain a layout inflater.
	 * @return A new {@link View} to be displayed by a ListView.
	 */
	@Override
	public View getNewView(Activity activity) 
	{
		final Activity _activity = activity;
		
		LayoutInflater inflater = activity.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.list_item_store, null);
		textView = (TextView) rowView.findViewById(R.id.textViewStoreName);
		textViewStoreCoordinates = (TextView) rowView.findViewById(R.id.textViewStoreCoordinates);
		imageViewSetCoordinates = (ImageView) rowView.findViewById(R.id.imageViewSetCoordinates);
		
		imageViewSetCoordinates.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	int status=GooglePlayServicesUtil.isGooglePlayServicesAvailable(_activity.getBaseContext());
            	if (status != ConnectionResult.SUCCESS)
            	{
					Toast.makeText(_activity, "Map services NOT available.", Toast.LENGTH_LONG).show();
					return;
            	}            	
            	
            	Store obj = (Store) v.getTag();
            	ApplicationPm.getSelectStorePm().onPickStoreCoordinatesWithMap(obj);
            }
        });
		
		return rowView;
	}

	/**
	 * Copy the name of the specified {@link Store} into the {@link TextView} of the
	 * {@link View} created by <code>getNewView()</code>.
	 * @param obj The {@link Store}.
	 * @param isSelected Ignored.
	 */
	@Override
	public void renderAdaptee(Store obj, boolean isSelected)
	{
		imageViewSetCoordinates.setTag(obj);

		// Set the name, and the default-ness indicator.
		textView.setText(obj.getName());
		
		// Set the coordinates.
		DecimalFormat df = new DecimalFormat("#.####");
		
		StringBuilder coordinates = new StringBuilder("(");
		if (obj.getLatitude() == null) coordinates.append("------");
		else coordinates.append(df.format(obj.getLatitude()));
		coordinates.append(", ");
		if (obj.getLongitude() == null) coordinates.append("------");
		else coordinates.append(df.format(obj.getLongitude()));
		coordinates.append(")");
		
		textViewStoreCoordinates.setText(coordinates.toString());
		
		if (ApplicationPm.getSelectStorePm().getSelectedStore() == obj) {
			// This is the currently selected store.
			textView.setTextColor(0xFF00FF00);  // green
			textViewStoreCoordinates.setTextColor(0xFF00FF00);  // green
		} else {
			// This is not the currently selected store.
			textView.setTextColor(0xFFFFFFFF);  // white
			textViewStoreCoordinates.setTextColor(0xFFFFFFFF);  // white
		}
	}
}
