// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui.EditShoppingList;

import java.text.NumberFormat;

import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstance;
import com.homecortex.groceryShoppingList.Ui.EntityAdapter;
import com.homecortex.groceryShoppingList.Util.BooleanResult;
import com.homecortex.groceryShoppingList.R;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ItemInstanceAdapter extends EntityAdapter<ItemInstance>
{
	private static final String TAG = "ItemInstanceAdapter";

	private RelativeLayout layout;
	private CheckBox checkBox;
	private TextView textViewItemInstanceDescription;
	private TextView textViewItemInstanceAttributes;
	private TextView textViewStoreItemPrice;
	private ImageView dragHandle;
	private ImageView removeHandle;

	@Override
	public View getNewView(Activity activity) {
		final Activity currentActivity = activity;
		LayoutInflater inflater = activity.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.list_item_iteminstance, null);
		layout = (RelativeLayout) rowView.findViewById(R.id.layoutItemInstance);
		checkBox = (CheckBox) rowView.findViewById(R.id.checkBoxItemInstanceIsPickedUp);
		textViewItemInstanceDescription = (TextView) rowView.findViewById(R.id.textViewItemInstanceDescription);
		textViewItemInstanceAttributes = (TextView) rowView.findViewById(R.id.textViewItemInstanceAttributes);
		textViewStoreItemPrice = (TextView) rowView.findViewById(R.id.textViewStoreItemPrice);
		dragHandle = (ImageView) rowView.findViewById(R.id.drag_handle);
		removeHandle = (ImageView) rowView.findViewById(R.id.remove_handle);

		layout.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
		
		checkBox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	// Toggle the piked-up state of the ItemInstance.
            	ItemInstance obj = (ItemInstance) v.getTag();
				boolean previousPickupState = obj.isPickedUp();
				boolean pickupFailed = false;
				if (!obj.isPickedUp(!previousPickupState)) pickupFailed = true;
				
				if (pickupFailed) {
					checkBox.setChecked(false);
					CharSequence text = currentActivity.getString(R.string.no_store_selected);
				    Toast.makeText(currentActivity, text, Toast.LENGTH_LONG).show();
				}
            }
        });

		textViewStoreItemPrice.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
        		Log.d(TAG, "textViewStoreItemPrice.onClick");
            }
        });
		
		return rowView;
	}

	@Override
	public void renderAdaptee(ItemInstance obj, boolean isSelected) {
		if (isSelected) {
			layout.setBackgroundColor(0xff00A000);
		} else {
			layout.setBackgroundColor(0xff000000);
		}
		if (obj.isPickedUp()) {
			checkBox.setChecked(true);
			textViewItemInstanceDescription.setTextColor(0xFF00FF00);  // green
			textViewItemInstanceAttributes.setTextColor(0xFF00FF00);   // green
			textViewStoreItemPrice.setTextColor(0xFF00FF00);           // green
			dragHandle.getLayoutParams().width = LayoutParams.WRAP_CONTENT;
			dragHandle.setVisibility(View.VISIBLE);
			//removeHandle.getLayoutParams().width = 1;
			removeHandle.setVisibility(View.GONE);
		} else {
			checkBox.setChecked(false);
			textViewItemInstanceDescription.setTextColor(0xFFFFFFFF);  // white
			textViewItemInstanceAttributes.setTextColor(0xFFFFFFFF);   // white
			textViewStoreItemPrice.setTextColor(0xFFFFFFFF);           // white
			//dragHandle.getLayoutParams().width = 1;
			dragHandle.setVisibility(View.GONE);
			removeHandle.getLayoutParams().width = LayoutParams.WRAP_CONTENT;
			removeHandle.setVisibility(View.VISIBLE);
		}
		checkBox.setTag(obj);
		textViewItemInstanceDescription.setText(obj.toString());
		BooleanResult isKnownPrice = new BooleanResult();
		textViewStoreItemPrice.setText(getItemInstancePriceString(obj, isKnownPrice));
		if (isKnownPrice.get()) {
			textViewStoreItemPrice.setTextColor(0xFFFFFFFF);  // white
		} else {
			textViewStoreItemPrice.setTextColor(0xFF888888);  // dark gray
		}
		
		String attributesString = obj.getAttributesString();
		if (attributesString.length() > 0) {
			textViewItemInstanceAttributes.setText(obj.getAttributesString());
			textViewItemInstanceAttributes.setVisibility(View.VISIBLE);
		} else {
			textViewItemInstanceAttributes.setVisibility(View.GONE);
		}
	}
	
	private String getItemInstancePriceString(ItemInstance obj, BooleanResult isKnownPrice) {
		Double value = obj.getStoreItemPrice(isKnownPrice);
		if (value == null) return "";
		NumberFormat baseFormat = NumberFormat.getCurrencyInstance();
		return baseFormat.format(value);
	}
}
