// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui.EditItemInstanceDetails;

import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstanceAttributeDesireIndicator;
import com.homecortex.groceryShoppingList.Ui.EntityAdapter;
import com.homecortex.groceryShoppingList.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ItemInstanceAttributeDesireIndicatorAdapter extends EntityAdapter<ItemInstanceAttributeDesireIndicator>
{
	private RelativeLayout layout;
	private CheckBox checkBox;
	private TextView textView;

	@Override
	public View getNewView(Activity activity) {
		LayoutInflater inflater = activity.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.list_item_iteminstanceattribute, null);
		
		layout = (RelativeLayout) rowView.findViewById(R.id.layoutItemInstanceAttribute);
		checkBox = (CheckBox) rowView.findViewById(R.id.checkBoxItemInstanceAttributeIsDesired);
		textView = (TextView) rowView.findViewById(R.id.textViewItemInstanceAttributeName);
		
		layout.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
		layout.setFocusable(false);
		layout.setFocusableInTouchMode(false);
		checkBox.setFocusable(false);
		checkBox.setFocusableInTouchMode(false);
		textView.setFocusable(false);
		textView.setFocusableInTouchMode(false);

		checkBox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	// Toggle the is-present state of the ItemInstanceAttributeDesireIndicator.
            	ItemInstanceAttributeDesireIndicator obj = (ItemInstanceAttributeDesireIndicator) v.getTag();
            	obj.onClickCheckbox();
            }
        });
		
		return rowView;
	}

	@Override
	public void renderAdaptee(ItemInstanceAttributeDesireIndicator obj, boolean isSelected) {
		checkBox.setTag(obj);

		if (obj.isDesired()) {
			checkBox.setChecked(true);
			textView.setTextColor(0xFF00FF00);  // green
		} else {
			checkBox.setChecked(false);
			textView.setTextColor(0xFFFFFFFF);  // white
		}
		
		textView.setText(obj.getName());
	}

}
