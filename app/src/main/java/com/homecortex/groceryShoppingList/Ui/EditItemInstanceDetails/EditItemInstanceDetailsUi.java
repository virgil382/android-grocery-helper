// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui.EditItemInstanceDetails;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstanceAttributeDesireIndicator;
import com.homecortex.groceryShoppingList.DataModel.Entities.VolumeUnit;
import com.homecortex.groceryShoppingList.DataModel.Entities.WeightUnit;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.EditItemInstanceDetailsPm;
import com.homecortex.groceryShoppingList.Ui.CustomArrayAdapter;
import com.homecortex.groceryShoppingList.Ui.DataModelUpdateNotifier;
import com.homecortex.groceryShoppingList.Ui.IDataModelUpdateListener;
import com.homecortex.groceryShoppingList.WheelWidget.OnWheelScrollListener;
import com.homecortex.groceryShoppingList.WheelWidget.WheelView;
import com.homecortex.groceryShoppingList.WheelWidget.adapters.ArrayWheelAdapter;
import com.homecortex.groceryShoppingList.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.TextView;

public class EditItemInstanceDetailsUi  extends Activity implements OnItemLongClickListener, OnItemClickListener, IDataModelUpdateListener
{
	private EditItemInstanceDetailsPm pm;
	TextView m_textViewItemInstanceDescription;
	CheckBox m_checkBoxQuantity;
	CheckBox m_checkBoxVolume;
	CheckBox m_checkBoxWeight;
	WheelView m_quantityWheel;
	WheelView m_volumeWheel;
	WheelView m_volumeUnitWheel;
	WheelView m_weightWheel;
	WheelView m_weightUnitWheel;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edititeminstancedetails);
        
        pm = ApplicationPm.getEditItemInstanceDetailsPm();

        m_textViewItemInstanceDescription = (TextView) findViewById(R.id.textViewItemInstanceDescription);
        try {
	        initializeQuantityCheckBox();
	        initializeQuantityWheel();
	        
	        initializeVolumeCheckBox();
	        initializeVolumeWheel();
			initializeVolumeUnitWheel();
	
			initializeWeightCheckBox();
	        initializeWeightWheel();
	        initializeWeightUnitWheel();
	        
	        initializeItemInstanceAttributesListView();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        DataModelUpdateNotifier.instance().addListener(this);
        
		refreshControls();   // Sets the current values into all the UI controls initialized above.
    }
    
    // Quantity
    
    private void initializeQuantityCheckBox() {
        m_checkBoxQuantity = (CheckBox) findViewById(R.id.checkBoxQuantity);
    	m_checkBoxQuantity.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
				pm.isQuantityEnabled(m_checkBoxQuantity.isChecked());
        		//pm.setQuantityIndex(m_quantityWheel.getCurrentItem());
            }
        });
    }
    
    private void initializeQuantityWheel() {
    	List<String> values = pm.getQuantities();
       
    	m_quantityWheel = (WheelView) findViewById(R.id.wheelViewQuantity);
        ArrayWheelAdapter<String> adapter = new ArrayWheelAdapter<>(this, values);
        adapter.setItemResource(R.layout.wheel_item_units);
        adapter.setItemTextResource(R.id.text);
        adapter.setEmptyItemResource(R.layout.wheel_item_units);
        m_quantityWheel.setViewAdapter(adapter);
        m_quantityWheel.setVisibleItems(3);
        
        m_quantityWheel.addScrollingListener( new OnWheelScrollListener() {
            public void onScrollingStarted(WheelView wheel) { }
            public void onScrollingFinished(WheelView wheel) {
				pm.setQuantityIndex(m_quantityWheel.getCurrentItem());
            }
        });
    }
    
    // Volume
    
    private void initializeVolumeCheckBox() {
		m_checkBoxVolume = (CheckBox) findViewById(R.id.checkBoxVolume);
		m_checkBoxVolume.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
				pm.isVolumeEnabled(m_checkBoxVolume.isChecked());
        		//pm.setVolumeIndex(volumeWheel.getCurrentItem());  
        		//pm.setVolumeUnitIndex(volumeUnitWheel.getCurrentItem());  
            }
        });
    }

    private void initializeVolumeWheel() {
    	List<String> values = pm.getVolumes();

        m_volumeWheel = (WheelView) findViewById(R.id.wheelViewVolume);
        ArrayWheelAdapter<String> adapter = new ArrayWheelAdapter<>(this, values);
        adapter.setItemResource(R.layout.wheel_item_units);
        adapter.setItemTextResource(R.id.text);
        adapter.setEmptyItemResource(R.layout.wheel_item_units);
        m_volumeWheel.setViewAdapter(adapter);
        m_volumeWheel.setVisibleItems(3);

        m_volumeWheel.addScrollingListener(new OnWheelScrollListener() {
            public void onScrollingStarted(WheelView wheel) { }
            public void onScrollingFinished(WheelView wheel) {
				pm.setVolumeIndex(m_volumeWheel.getCurrentItem());
            }
        });
    }
    
    private void initializeVolumeUnitWheel() throws SQLException {
        ArrayWheelAdapter<VolumeUnit> adapter = new ArrayWheelAdapter<>(this, new ArrayList<VolumeUnit>());
        pm.setVolumeUnitsAdapter(adapter);
        adapter.setItemResource(R.layout.wheel_item_units);
        adapter.setItemTextResource(R.id.text);
        adapter.setEmptyItemResource(R.layout.wheel_item_units);
       
        m_volumeUnitWheel = (WheelView) findViewById(R.id.wheelViewVolumeUnit);
        m_volumeUnitWheel.setViewAdapter(adapter);
        m_volumeUnitWheel.setVisibleItems(3);
        m_volumeUnitWheel.addScrollingListener( new OnWheelScrollListener() {
            public void onScrollingStarted(WheelView wheel) { }
            public void onScrollingFinished(WheelView wheel) {
				pm.setVolumeUnitIndex(m_volumeUnitWheel.getCurrentItem());
            }
        });
    }

    // Weight
    
    private void initializeWeightCheckBox() {
		m_checkBoxWeight = (CheckBox) findViewById(R.id.checkBoxWeight);
		m_checkBoxWeight.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
				pm.isWeightEnabled(m_checkBoxWeight.isChecked());
        		//pm.setWeightIndex(weightWheel.getCurrentItem());  
        		//pm.setWeightUnitIndex(weightUnitWheel.getCurrentItem());  
            }
        });
    }

    private void initializeWeightWheel() {
    	List<String> values = pm.getWeights();
        
    	m_weightWheel = (WheelView) findViewById(R.id.wheelViewWeight);
        ArrayWheelAdapter<String> adapter = new ArrayWheelAdapter<>(this, values);
        adapter.setItemResource(R.layout.wheel_item_units);
        adapter.setItemTextResource(R.id.text);
        adapter.setEmptyItemResource(R.layout.wheel_item_units);
        m_weightWheel.setViewAdapter(adapter);
        m_weightWheel.setVisibleItems(3);
        
        m_weightWheel.addScrollingListener( new OnWheelScrollListener() {
            public void onScrollingStarted(WheelView wheel) { }
            public void onScrollingFinished(WheelView wheel) {
				pm.setWeightIndex(m_weightWheel.getCurrentItem());
            }
        });
    }
    
    private void initializeWeightUnitWheel() throws SQLException {
        ArrayWheelAdapter<WeightUnit> adapter = new ArrayWheelAdapter<>(this, new ArrayList<WeightUnit>());
        pm.setWeightUnitsAdapter(adapter);
        adapter.setItemResource(R.layout.wheel_item_units);
        adapter.setItemTextResource(R.id.text);
        adapter.setEmptyItemResource(R.layout.wheel_item_units);
       
        m_weightUnitWheel = (WheelView) findViewById(R.id.wheelViewWeightUnit);
        m_weightUnitWheel.setViewAdapter(adapter);
        m_weightUnitWheel.setVisibleItems(3);
        m_weightUnitWheel.addScrollingListener( new OnWheelScrollListener() {
            public void onScrollingStarted(WheelView wheel) { }
            public void onScrollingFinished(WheelView wheel) {
				pm.setWeightUnitIndex(m_weightUnitWheel.getCurrentItem());
            }
        });
    }

    // Attributes
    
    private void initializeItemInstanceAttributesListView() throws SQLException {
		ListView listViewItemInstanceAttributes = (ListView) findViewById(R.id.listViewItemInstanceAttributes);

		// Inflate the View for the additional row in the ListView, and set its text.
		View extraRowView = getLayoutInflater().inflate(R.layout.list_item_addanotheritem, null);
		Button addButton = (Button) extraRowView.findViewById(R.id.buttonAddAnotherRow);
		addButton.setText(R.string.add_another_item_instance_attribute);
		addButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				onButtonAddAnotherRow(arg0);
			}
		});
    	
		// Use a CustomAdapter along with the ShoppingListAdapter to bind the ListView
        // to the List containing ShoppingLists.  Tell it to include the extra row View.
		CustomArrayAdapter<ItemInstanceAttributeDesireIndicator, ItemInstanceAttributeDesireIndicatorAdapter> customArrayAdapter =
			new CustomArrayAdapter<>(
				this, new ArrayList<ItemInstanceAttributeDesireIndicator>(), ItemInstanceAttributeDesireIndicatorAdapter.class, extraRowView);

		listViewItemInstanceAttributes.setOnItemLongClickListener(this);
		listViewItemInstanceAttributes.setOnItemClickListener(this);
		listViewItemInstanceAttributes.setAdapter(customArrayAdapter);
		pm.setItemAttributes(customArrayAdapter);
    }
    
	public void onButtonAddAnotherRow(View v) {
		ApplicationPm.m_currentActivity = this;
		pm.onAddAnotherItemAttribute();
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		ApplicationPm.m_currentActivity = this;
		pm.onLongClickItemAttribute(position);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		ApplicationPm.m_currentActivity = this;
		pm.onClickItemAttribute(position);
	}

	@Override
	public void onDataModelChange() { refreshControls(); }

	private void refreshControls() {
		m_textViewItemInstanceDescription.setText(pm.getItemInstanceDescription());
    	m_checkBoxQuantity.setChecked(pm.isQuantityEnabled());
        m_quantityWheel.setCurrentItem(pm.getQuantityIndex());

        m_checkBoxVolume.setChecked(pm.isVolumeEnabled());
        m_volumeWheel.setCurrentItem(pm.getVolumeIndex());
		m_volumeUnitWheel.setCurrentItem(pm.getVolumeUnitIndex());

		m_checkBoxWeight.setChecked(pm.isWeightEnabled());
        m_weightWheel.setCurrentItem(pm.getWeightIndex());
        m_weightUnitWheel.setCurrentItem(pm.getWeightUnitIndex());
	}
}
