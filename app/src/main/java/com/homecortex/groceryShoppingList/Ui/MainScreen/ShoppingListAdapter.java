// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui.MainScreen;


import com.homecortex.groceryShoppingList.DataModel.Entities.ShoppingList;
import com.homecortex.groceryShoppingList.Ui.EntityAdapter;
import com.homecortex.groceryShoppingList.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

/**
 * <p>A <code>ShoppingListAdapter</code> is an {@link EntityAdapter} that creates a
 * {@link View} by inflating <code>shoppinglistrow.xml</code> and that renders the
 * name of a {@link ShoppingList} into the {@link TextView} field contained within the
 * {@link View}.
 * 
 * <p>Also see <a href="http://www.lepus.org.uk/ref/companion/Flyweight.xml">Flyweight
 * Design Pattern</a>:  The <code>ShoppingListAdapter</code> plays the role of a
 * <code>Concrete Flyweight</code> whose intrinsic state consists of references to the
 * UI elements of the {@link View} that it inflates from <code>shoppinglistrow.xml</code>,
 * and whose extrinsic state is a {@link ShoppingList} passed to its 
 * <code>renderAdaptee()</code> method.
 */
public class ShoppingListAdapter extends EntityAdapter<ShoppingList>
{
	private TextView textView;
	
	/**
	 * Create a new {@link View} used by a ListView to render a row by inflating
	 * <code>shoppinglistrow.xml</code>.  Store references to the {@link View} 
	 * LinerLayout and {@link TextView} so that at a later time
	 * <code>renderAdaptee()</code> may render a {@link ShoppingList} into the {@link View}.
	 * 
	 * @param activity The {@link Activity} via which this method may obtain a layout inflater.
	 * @return A new {@link View} to be displayed by a ListView.
	 */
	@Override
	public View getNewView(Activity activity) 
	{
		LayoutInflater inflater = activity.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.list_item_shoppinglist, null);
		//layout = (LinearLayout) rowView.findViewById(R.id.linearLayoutShoppingList);
		textView = (TextView) rowView.findViewById(R.id.textViewShoppingListName);
		textView.setTextColor(0xFFFFFFFF);  // white
		return rowView;
	}

	/**
	 * Copy the name of the specified {@link ShoppingList} into the {@link TextView} of the
	 * {@link View} created by <code>getNewView()</code>.
	 * @param obj The {@link ShoppingList}.
	 * @param isSelected Indicates if the {@link ShoppingList} should be rendered as "selected".
	 * If true, then set the background color of the LinerLayout to something conspicuous.
	 */
	@Override
	public void renderAdaptee(ShoppingList obj, boolean isSelected)
	{
//		if (isSelected) {
//			layout.setBackgroundColor(0xff00A000);
//		} else {
//			layout.setBackgroundColor(0xff000000);
//		}
		textView.setText(obj.getName());
	}
}
