// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui.SelectLocation;

import android.graphics.Color;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.homecortex.groceryShoppingList.PresentationModel.IMapAdapter;


public class GoogleMapAdapter implements IMapAdapter {

	private GoogleMap mMap;
	private Marker mMarker;
	private Circle mCircle;
	
	public GoogleMapAdapter(GoogleMap map) {
		mMap = map;
	}
	
	@Override
	public void setMarker(double lat, double lng, double radius, String name) {
		LatLng position = new LatLng(lat, lng);
		if (mMarker == null) {
			mMarker = mMap.addMarker(new MarkerOptions()
				.position(position)
				.title(name));
			mCircle = mMap.addCircle(new CircleOptions()
				.center(position)
				.radius(radius)
				.strokeWidth(3)
				.strokeColor(Color.RED));
		} else {
			mMarker.setPosition(position);
			mCircle.setCenter(position);
			mCircle.setRadius(radius);
		}
	}

	@Override
	public void setRadius(double radius) {
		if (mCircle != null) {
			mCircle.setRadius(radius);
		}
	}

	@Override
	public void setCameraLocation(double lat, double lng) {
		CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 14);
    	mMap.moveCamera(update);
	}

}
