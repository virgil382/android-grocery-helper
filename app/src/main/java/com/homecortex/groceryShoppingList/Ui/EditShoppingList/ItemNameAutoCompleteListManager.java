// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui.EditShoppingList;

import java.util.Collection;
import java.util.Comparator;

import android.widget.ArrayAdapter;

import com.homecortex.groceryShoppingList.DataModel.Entities.Item;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemPurchaseEvent;
import com.homecortex.groceryShoppingList.DataModel.Entities.UpdateSet;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemExtent;
import com.homecortex.groceryShoppingList.DataModel.Extents.ItemPurchaseEventExtent;
import com.homecortex.groceryShoppingList.DataModel.Interfaces.IEntityCollectionQueryResultHandler;
import com.homecortex.groceryShoppingList.DataModel.EntityObserver;

public class ItemNameAutoCompleteListManager {
	private ArrayAdapter<Item> m_adapter;
	private boolean m_isOpen = false;

	public ItemNameAutoCompleteListManager(ArrayAdapter<Item> adapter) {
		m_adapter = adapter;
	}

    public void open() {
        if (m_isOpen) return;
        m_isOpen = true;
        m_itemEventHandler.open();
        m_itemPurchaseEventHandler.open();
    }

    public void close() {
        if (!m_isOpen) return;
        m_isOpen = true;
        m_itemEventHandler.close();
        m_adapter.clear();
        m_itemPurchaseEventHandler.close();
    }

    /**
     * An ItemEventHandler handles Item creation/update/deletions by operating on the contents
     * of m_adapter.
     */
    private class ItemEventHandler extends EntityObserver<Item> {
        @Override
        public void open() {
            ItemExtent.instance().getAll(new IEntityCollectionQueryResultHandler<Item>() {
                @Override
                public void onResult(Collection<Item> objects) {
                    for(Item item : objects) m_adapter.add(item);
                }
            });

            ItemExtent.instance().addObserver(this);
            sortByPurchaseFrequency();
        }

        public void close() {
            ItemExtent.instance().dropObserver(this);
        }

        @Override
        public void onCreate(Item object) {
            m_adapter.add(object);
            sortByPurchaseFrequency();
        }

        @Override
        public void onUpdate(Item object, UpdateSet updates) {
            m_adapter.notifyDataSetChanged();
        }

        @Override
        public void onDelete(Item object) {
            m_adapter.remove(object);
        }
    }
    private ItemEventHandler m_itemEventHandler = new ItemEventHandler();


    /**
     * An ItemPurchaseEventHandler handles ItemPurchaseEvent creations by re-sorting the contents
     * of m_adapter.
     */
    private class ItemPurchaseEventHandler extends EntityObserver<ItemPurchaseEvent> {
        @Override
        public void open() {
            ItemPurchaseEventExtent.instance().addObserver(this);
        }

        public void close() {
            ItemPurchaseEventExtent.instance().dropObserver(this);
        }

        @Override
        public void onCreate(ItemPurchaseEvent object) {
            sortByPurchaseFrequency();
        }
    }
    private ItemPurchaseEventHandler m_itemPurchaseEventHandler = new ItemPurchaseEventHandler();

    /**
     * An ItemPurchaseFrequencyComparator compares two Items based on their purchase frequency.  It
     * is used to sort items based on their purchase frequency.
     */
    private class ItemPurchaseFrequencyComparator implements Comparator<Item> {
        public int compare(Item left, Item right) {
            return(-left.getPurchaseFrequency().compareTo(right.getPurchaseFrequency()));
        }
    }
    ItemPurchaseFrequencyComparator o_itemPurchaseFrequencyComparator = new ItemPurchaseFrequencyComparator();

    /**
     * Order the Items contained in m_adapter by the frequency with which they were purchased.
     */
	void sortByPurchaseFrequency() {
        m_adapter.sort(o_itemPurchaseFrequencyComparator);
	}
}
