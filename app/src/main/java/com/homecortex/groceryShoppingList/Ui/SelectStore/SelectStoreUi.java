// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui.SelectStore;

import java.sql.SQLException;
import java.util.ArrayList;





import com.homecortex.groceryShoppingList.DataModel.Entities.Store;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.SelectStorePm;
import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.Ui.CustomArrayAdapter;
import com.homecortex.groceryShoppingList.UiAdapters.Registry.UiPmRegistry;
import com.homecortex.groceryShoppingList.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

public class SelectStoreUi extends Activity implements OnItemClickListener, OnItemLongClickListener
{
	private SelectStorePm pm;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
        setContentView(R.layout.selectstore);
        
        pm = ApplicationPm.getSelectStorePm();

        // Inflate the View for the additional row in the ListView, and set its text.
		View extraRowView = getLayoutInflater().inflate(R.layout.list_item_addanotheritem, null);
		Button addButton = (Button) extraRowView.findViewById(R.id.buttonAddAnotherRow);
		addButton.setText(R.string.add_another_store);

		// Use a CustomAdapter along with the ItemInstanceAdapter to bind the ListView
        // to a new ArrayList containing ItemInstances.  Tell it to include the extra row View.
		CustomArrayAdapter<Store, StoreAdapter> customArrayAdapter =
			new CustomArrayAdapter<>(this, new ArrayList<Store>(), StoreAdapter.class, extraRowView);

		// Set it into the PM so that it may update the contents of the customArrayAdapter
		// and of the ListView as appropriate.
		try {
			pm.setStoresAdapter(customArrayAdapter);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		ListView listViewStores = (ListView) findViewById(R.id.listViewStores);
        listViewStores.setOnItemClickListener(this);
        listViewStores. setOnItemLongClickListener(this);
        listViewStores.setAdapter(customArrayAdapter);
	}
	
    public void onButtonAddAnotherRow(View v) {
		ApplicationPm.m_currentActivity = this;
		pm.onAddAnotherStore();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		ApplicationPm.m_currentActivity = this;
		pm.onClickStore(position);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		ApplicationPm.m_currentActivity = this;
		pm.onLongClickStore(position);
		return true; // consume the long click event
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		UiPm requestedActivityPm = UiPmRegistry.getActivityPm(requestCode);
		if(requestedActivityPm == null) return;
		if (!requestedActivityPm.dispatchResult(pm, resultCode == Activity.RESULT_CANCELED)) return;
		// TODO: Investigate this code.  Why is it like this?
	}
}
