// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui;

import java.util.Comparator;
import java.util.List;

import com.homecortex.groceryShoppingList.PresentationModel.INarrowCustomArrayAdapter;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * A <code>CustomArrayAdapter</code> is an {@link ArrayAdapter} that supplies a ListView
 * with {@link View}s used to display its rows.  A <code>CustomArrayAdapter</code> relies on 
 * {@link EntityAdapter}s to create {@link View}s and to render entity data into those {@link View}s.
 * A <code>CustomArrayAdapter</code> also maintains the index of the selected row in its state and
 * tells the corresponding {@link View} to render itself differently so that it visibly stands out as
 * selected.  A <code>CustomArrayAdapter</code> may be configured to return an extra {@link View}
 * corresponding to an extra row in the ListView (that may, for example, contain UI elements
 * via which the user may add entries to the ListView).
 * 
 * @param <T> The type of entity displayed by the ListView.
 * @param <A> The {@link EntityAdapter} in which the <code>CustomArrayAdapter</code> relies to
 * construct row views and to render the contents of <code>T</code>s into those {@link View}s.
 */
public class CustomArrayAdapter<T, A extends EntityAdapter<T>> extends ArrayAdapter<T> implements INarrowCustomArrayAdapter<T>
{
	private final Activity m_activity;
	private final List<T> m_objects;
	private final Class<A> m_adapterClass;
	private final View m_extraRowView;
	private int m_selectedPosition = -1;
	private final Handler mHandler = new Handler();

	/**
	 * Create a <code>CustomArrayAdapter</code>.
	 * @param activity The {@link Activity} from which concrete <code>A</code>s may obtain an
	 * inflater to help them to create and supply {@link View}s to the <code>CustomArrayAdapter</code>.
	 * @param objects A {@link List} of entity objects rendered into {@link View}s by <code>A</code>s.
	 * @param adapterClass The class of the concrete {@link EntityAdapter} that the 
	 * <code>CustomArrayAdapter</code> will instantiate when needed.
	 * @param extraRowView <code>null</code> or a {@link View} that the <code>CustomArrayAdapter</code>
	 * should supply to the ListView for display as an additional last row.
	 */
	public CustomArrayAdapter(Activity activity, List<T> objects, Class<A> adapterClass, View extraRowView) {
		super(activity, 0, objects);

		m_activity = activity;
		m_objects = objects;
		m_adapterClass = adapterClass;
		m_extraRowView = extraRowView;
	}

	public List<T> getObjects() {
		return m_objects;
	}
	
	/**
	 * Get the number of rows to be displayed by the ListView (including the extra row
	 * if one was specified).
	 * @return The number of rows to be displayed by the ListView.
	 */
	@Override
    public int getCount()
	{
		if (m_extraRowView == null) return m_objects.size();
		return m_objects.size() + 1;
    }
    
	/**
	 * Add a new <code>T</code> to the end of the array.
	 * @param object The new instance of <code>T</code> to add.
	 * @param selectNewItem Flag indicating whether or not to select the
	 * newly added <code>T</code>.
	 */
	@Override
    public void add(T object, boolean selectNewItem) {
		if (selectNewItem) m_selectedPosition = m_objects.size();

		super.add(object);
    }

	@Override
	public void addFromAnyThread(final T object, final boolean selectNewItem) {
		// Enqueue work on mHandler to change the data on the main thread.
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				add(object, selectNewItem);
			}
		});
	}

	/**
	 * Add a new <code>T</code> at the specified position in the array.
	 * @param newItem The new <code>T</code> to add.
	 * @param position The 0-indexed position of the <code>T</code> before which to
	 * add the new <code>T</code>. 
	 * @param selectNewItem Flag indicating whether or not to select the
	 * newly added <code>T</code>.
	 */
	@Override
	public void add(T newItem, int position, boolean selectNewItem) {
		if (selectNewItem) {
			m_selectedPosition = position;
		} else {
			if (m_selectedPosition >= position) m_selectedPosition++;
		}

		super.insert(newItem, position);
	}
	
	/**
	 * Remove a <code>T</code>.  If removing the selected one, then select the next
	 * (remaining) <code>T</code>.  If removing the last <code>T</code> and it is 
	 * selected, then select the previous (remaining) <code>T</code>.
	 */
	@Override
    public void remove(T object) {
		int removeIndex = m_objects.indexOf(object);
		
		// If removing the selected object, and if it is the last one, then select the previous object.
		if (m_selectedPosition == removeIndex) {
			if (removeIndex == m_objects.size() - 1) {
				m_selectedPosition--;
			}
		}
		
		if (Looper.myLooper() != Looper.getMainLooper()) {
			m_objects.remove(object);
		} else {
			super.remove(object);
		}
    }

    @Override
    public void removeFromAnyThread(final T object) {
        // Enqueue work on mHandler to change the data on the main thread.
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                remove(object);
            }
        });
    }

	@Override
	public void clear() {
		m_selectedPosition = -1;
		super.clear();
	}
	
	@Override
	public T get(int location) {
		if (location < 0 || location >= getCount()) return null;
		return m_objects.get(location);
	}
	
	/**
	 * Sort the contained items and retain the selection.
     * @param comparator The comparator used to sort the objects contained
     *        in this <code>INarrowCustomArrayAdapter</code>.
	 */
	public void sort(Comparator<? super T> comparator)
	{
		T selectedItem = getSelection();
		if (Looper.myLooper() == Looper.getMainLooper()) {
			super.sort(comparator);
		}
		if (selectedItem != null) setSelection(selectedItem);
	}
	public void sortFromAnyThread(final Comparator<? super T> comparator)
	{
		// Enqueue work on mHandler to change the data on the main thread.
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				sort(comparator);
			}
		});
	}

    public int getPosition(T item) {
        return super.getPosition(item);
    }

    /**
	 * Select the specified <code>T</code>.
	 * @param selectedObject The <code>T</code> to select.
	 */
	public void setSelection(T selectedObject)
	{
		int pos = m_objects.indexOf(selectedObject);
		setSelection(pos);
	}

	/**
	 * Select the <code>T</code> at the specified 0-indexed position.
	 * @param pos The index of the selected row.  A negative value indicates no selection. A
	 * value >= the number of <code>T</code>s sets the selection index to <code>-1</code> (i.e. no
	 * selection).
	 */
	public T setSelection(int pos)
	{
		if (pos >= m_objects.size()) pos = -1;

		if (pos == m_selectedPosition)
			return getSelection();
		
		m_selectedPosition = pos;
		this.notifyDataSetChanged();
		
		return getSelection();
	}
	
	/**
	 * Get the currently selected <code>T</code>.
	 * @return The currently selected <code>T</code>.
	 */
	public T getSelection() 
	{
		if (m_selectedPosition < 0) return null;
		return m_objects.get(m_selectedPosition);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// If this is the last row, then return the extra View.
		if (m_extraRowView != null && position == getCount() - 1) {
			return m_extraRowView;
		}
		
		View rowView = convertView;
		
		// Create a new EntityAdapter and View if the convertView is null or if it
		// is not of an appropriate type.  Note that we implicitly determine the
		// appropriateness of the View by inspecting the type of <A> attached to the
		// View's tag.
		if (rowView == null || !m_adapterClass.isInstance(rowView.getTag()))
		{
			// Construct a new concrete EntityAdapter and get a View from it.
			A entityAdapter;
			try {
				entityAdapter = m_adapterClass.newInstance();
			} catch (IllegalAccessException | InstantiationException e) {
				e.printStackTrace();
				return(null);
			}
			rowView = entityAdapter.getNewView(m_activity);
			rowView.setTag(entityAdapter);
		}

		// Tell the EntityAdapter to render the corresponding <T> into the View.
		{
			T obj = m_objects.get(position);
			A entityAdapter = (A) rowView.getTag();
			entityAdapter.renderAdaptee(obj, position == m_selectedPosition);
		}		

		return rowView;
	}
}
