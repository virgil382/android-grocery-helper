// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui.SelectLocation;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.homecortex.groceryShoppingList.Location.ILocationListener;
import com.homecortex.groceryShoppingList.Location.LocationRequestor;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.SelectLocationPm;
import com.homecortex.groceryShoppingList.R;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class SelectLocationUi extends FragmentActivity implements OnMapClickListener, ILocationListener, OnMapReadyCallback
{
	private static final String TAG = "SelectLocationUi";
	
	private SelectLocationPm pm;
	LocationRequestor locationRequestor;

	private SeekBar seekBarRadius;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
        setContentView(R.layout.selectlocation);

        pm = ApplicationPm.getSelectLocationPm();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.onCreate(savedInstanceState);
        mapFragment.getMapAsync(this);

        // The LocationManager is used by the LocationRequestor to request the current coordinates
        // in case the user wants to set a store's coordinates which does not have coordinates.  This
        // allows the map view to start at the user's current location (rather than somewhere in
        // the Atlantic Ocean.)
    	LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationRequestor = new LocationRequestor(locationManager);

        seekBarRadius = (SeekBar) findViewById(R.id.seekBarRadius);
        seekBarRadius.setProgress((int) pm.getRadius());
        seekBarRadius.setOnSeekBarChangeListener( new OnSeekBarChangeListener()
        {
        	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        	{
        		pm.setRadius(progress);
        	}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) { }

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) { }
        });
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
        googleMap.setOnMapClickListener(this);
		pm.setMapAdapter(new GoogleMapAdapter(googleMap));
	}

    protected void onResume() {
    	super.onResume();
        Log.d(TAG, "onResume()");
    	locationRequestor.start(this);
    }

    /// Called after the user presses the power button to turn off the screen in order to "log off".
    protected void onPause() {
    	super.onPause();
        Log.d(TAG, "onPause()");
        locationRequestor.stop(120000);
    }
    
    protected void onStop() {
    	super.onStop();
        Log.d(TAG, "onStop()");
        locationRequestor.stop(0);
    }

    @Override
    public void onMapClick(LatLng point) {
		Log.d(TAG, "onMapClick: point=" + point);
		pm.onMapClick(point.latitude, point.longitude, seekBarRadius.getProgress());
    }

	@Override
	public void onLocationAvailable(Location location) {
    	pm.setInitialCoordinates(location.getLatitude(), location.getLongitude());
	}

    public void onButtonClickOk(View v) {
        ApplicationPm.m_currentActivity = this;
        pm.onButtonClickOk();
    }
}
