// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui;

public interface IDataModelUpdateListener {
	void onDataModelChange();
}
