// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui.EditShoppingList;

import java.sql.SQLException;
import java.util.ArrayList;

import com.homecortex.groceryShoppingList.DataModel.Entities.Item;
import com.homecortex.groceryShoppingList.DataModel.Entities.ItemInstance;
import com.homecortex.groceryShoppingList.Location.ILocationListener;
import com.homecortex.groceryShoppingList.Location.LocationRequestor;
import com.homecortex.groceryShoppingList.PresentationModel.ApplicationPm;
import com.homecortex.groceryShoppingList.PresentationModel.EditShoppingListPm;
import com.homecortex.groceryShoppingList.PresentationModel.UiPm;
import com.homecortex.groceryShoppingList.Ui.CustomArrayAdapter;
import com.homecortex.groceryShoppingList.Ui.DataModelUpdateNotifier;
import com.homecortex.groceryShoppingList.Ui.IDataModelUpdateListener;
import com.homecortex.groceryShoppingList.UiAdapters.Registry.UiPmRegistry;
import com.homecortex.groceryShoppingList.Util.LicenseManager;
import com.homecortex.groceryShoppingList.dslv.DragSortListView;
import com.homecortex.groceryShoppingList.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.ToggleButton;

public class EditShoppingListUi extends Activity implements ILocationListener, OnItemClickListener, OnItemLongClickListener, IDataModelUpdateListener
{
	private static final String TAG = "EditShoppingListUi";

	private EditShoppingListPm m_pm;
	private DragSortListView m_listViewItemInstances;
	private TextView m_textViewSelectedStoreName;
	private ToggleButton m_toggleButtonAutoSelectStore;
	private AutoCompleteTextView m_editTextAddItemInstance;
	private TextView m_textViewShoppingListTotal;
	private TextView m_textViewShoppingListTotalPickedUp;
	final EditShoppingListUi m_thisActivity = this;
	LocationRequestor m_locationRequestor;
	CustomArrayAdapter<ItemInstance, ItemInstanceAdapter> m_customArrayAdapter;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editshoppinglist);
        
    	m_textViewShoppingListTotal = (TextView) findViewById(R.id.textViewShoppingListTotal);
    	m_textViewShoppingListTotalPickedUp = (TextView) findViewById(R.id.textViewShoppingListTotalPickedUp);
    	m_textViewShoppingListTotalPickedUp.setTextColor(0xFF00FF00);  // green

    	m_pm = ApplicationPm.getEditShoppingListPm();

    	LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
    	m_locationRequestor = new LocationRequestor(locationManager);

    	// Use a CustomAdapter along with the ItemInstanceAdapter to bind the ListView
        // to a new ArrayList containing ItemInstances.  Tell it to include the extra row View.
		m_customArrayAdapter = new CustomArrayAdapter<>(
				this, new ArrayList<ItemInstance>(), ItemInstanceAdapter.class, null);
		try {
			// Set it into the PM so that it may update the contents of the customArrayAdapter
			// and of the ListView as appropriate.
			m_pm.setOrderedItemInstances(m_customArrayAdapter);
		} catch (SQLException e) {
			e.printStackTrace();
		}
        
        // Bind the ListView to an item click handler and to the Adapter.
		m_listViewItemInstances = (DragSortListView) findViewById(R.id.listViewItemInstances);
		//listViewItemInstances.setClickListener(this);
		m_listViewItemInstances.setOnItemClickListener(this);
		
		m_listViewItemInstances.setAdapter(m_customArrayAdapter);
		m_listViewItemInstances.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        m_toggleButtonAutoSelectStore = (ToggleButton) findViewById(R.id.toggleButtonAutoSelectStore);
        m_toggleButtonAutoSelectStore.setChecked(m_pm.isAutoSelectStore());

        m_textViewSelectedStoreName = (TextView) findViewById(R.id.textViewSelectedStoreName);
        m_textViewSelectedStoreName.setTextColor(0xFFFFFFFF);  // white

		refreshControls();
        DataModelUpdateNotifier.instance().addListener(this);

        m_editTextAddItemInstance = (AutoCompleteTextView) findViewById(R.id.editTextAddItemInstance);
    	setAutoCompleteItemNames();
        m_editTextAddItemInstance.setThreshold(2);
        m_editTextAddItemInstance.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE ||
                	event != null && event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)
                {
                	addItemInstance();
                }
				return false;
			}
		});

        m_listViewItemInstances.requestFocus();

        // Create a DropListener whose onDrop() method tells the PM to move an ItemInstance. 
    	DragSortListView.DropListener dropListener = new DragSortListView.DropListener()
    	{
    	    @Override
    	    public void drop(int from, int to) {
    	        if (from != to)
					try {
						m_pm.onMoveItemInstance(from, to);
					} catch (SQLException e) {
						e.printStackTrace();
					}
    	    }
    	};
    	m_listViewItemInstances.setDropListener(dropListener);

        // Create a RemoveListener whose remove() method tells the PM to remove an ItemInstance. 
    	DragSortListView.RemoveListener removeListener = new DragSortListView.RemoveListener()
    	{
			@Override
			public void remove(int which) {
				try {
					m_pm.onRemoveItemInstance(which);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
    	};
    	m_listViewItemInstances.setRemoveListener(removeListener);
    	m_listViewItemInstances.setOnItemLongClickListener(this);
    	
    	// Re-check the license and cache the response if successful.
        LicenseManager.instance().initiateCheckLicensePurchase(this);
    }

    /// Called after the user presses the power button to turn on the screen and optionally "logs on".
    protected void onResume() {
    	super.onResume();
        Log.d(TAG, "onResume()");
        
        if (m_pm.isAutoSelectStore()) {
			// Determine if the GPS is disabled because the user may want to enable it.
			if (m_locationRequestor.isGpsAvailableAndDisabled()) {
				askEnableGps();
				// The update mechanism will start but the GPS will be disabled.
			}

			m_locationRequestor.start(this);
        }
    }

    /// Called after the user presses the power button to turn off the screen in order to "log off".
    protected void onPause() {
    	super.onPause();
        Log.d(TAG, "onPause()");
        
        m_locationRequestor.stop(120000);
    }
    
    protected void onStop() {
    	super.onStop();
        Log.d(TAG, "onStop()");
        m_locationRequestor.stop(0);
    }

    // Handle the event announcing that the user pressed the "+" button by the editText for the ItemInstance name.
	public void onButtonClickAddItemInstance(View v) {
		// If the editText has some text, then add an ItemInstance by that name and return.
		String itemName = m_editTextAddItemInstance.getText().toString();
		if (itemName.compareTo("") != 0) {
	        addItemInstance();
	        return;
		}
		
		// Otherwise, show the soft keyboard so that the user may enter the ItemInstance name.
		m_editTextAddItemInstance.requestFocus();
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.showSoftInput(m_editTextAddItemInstance, InputMethodManager.SHOW_IMPLICIT);
	}
	
	/**
     * Configure the EditText into which the user types the names of Items to add (as new ItemInstances).
     * This method sets the auto-complete dictionary.  
     */
    private void setAutoCompleteItemNames() {
    	ArrayAdapter<Item> adapter = new ArrayAdapter<>(this,
    			android.R.layout.simple_dropdown_item_1line, new ArrayList<Item>());
		ItemNameAutoCompleteListManager itemNameAutoCompletionManager = new ItemNameAutoCompleteListManager(adapter);
		itemNameAutoCompletionManager.open();
    	m_editTextAddItemInstance.setAdapter(adapter);
    }

    private void addItemInstance() {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(m_editTextAddItemInstance.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        
        // Get the Item name from the AutoCompleteTextView and clear it.
		String itemName = m_editTextAddItemInstance.getText().toString();
		m_editTextAddItemInstance.setText("");
		
		// Add a new ItemInstance instantiated from an Item with that name.
		ApplicationPm.m_currentActivity = m_thisActivity;
		try {
			m_pm.addItemInstance(itemName);
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    // The user clicked on the store name.
	public void onClickTextViewSelectedStoreName(View v) {
		if (!m_toggleButtonAutoSelectStore.isChecked()) {
			// Manually select the store.
			ApplicationPm.m_currentActivity = this;
			try {
				m_pm.onButtonClickSelectStore();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	// The user clicked on the toggle button or auto/manual selecting the store.
	public void onClickToggleButtonAutoSelectStore(View v) {
		if (m_toggleButtonAutoSelectStore.isChecked()) {
			// The user enabled store auto-select.  Check a few things before starting Location updates.

			// Determine if location updates can be received.
			if (!m_locationRequestor.areLocationProvidersAvailable()) {
				// They cannot, so let the user know that Location updates are not possible and set 
				// the Auto Select Store button to OFF again.
				CharSequence text = getString(R.string.no_location_providers);
			    Toast.makeText(this, text, Toast.LENGTH_LONG).show();
				m_toggleButtonAutoSelectStore.setChecked(false);
				return;
			}

			// Determine if the GPS is disabled because the user may want to enable it.
			if (m_locationRequestor.isGpsAvailableAndDisabled()) {
				askEnableGps();
				// The update mechanism will start but the GPS will be disabled.
			}

			try {
				m_pm.setAutoSelectStore(true);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	m_locationRequestor.start(this);
		} else {
			// The user disabled store auto-select.
        	disbaleLocationUpdates();
		}
		
		refreshControls();
	}

	/**
	 *  Ask the user if he wants to enable the GPS in order to receive location updates.  If yes,
	 *  then take the user to the GPS settings screen.  Else, disable updates.
	 */
	private void askEnableGps() {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        switch (which){
		        case DialogInterface.BUTTON_POSITIVE:
		            //Yes button clicked
		        	m_locationRequestor.takeUserToGpsSettings(m_thisActivity);
		            break;

		        case DialogInterface.BUTTON_NEGATIVE:
		            //No button clicked
		        	disbaleLocationUpdates();
		            break;
		        }
		    }
		};
		
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(m_thisActivity);
		dialogBuilder
			.setMessage(m_thisActivity.getString(R.string.please_enable_gps))
			.setPositiveButton(m_thisActivity.getString(R.string.yes), dialogClickListener)
			.setNegativeButton(m_thisActivity.getString(R.string.no), dialogClickListener)
			.setCancelable(true);

		// Set the AlertDialog to be cancelable if the user clicks outside.
		AlertDialog dialog = dialogBuilder.create();

		// Cancel listener or back button listener.
		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {         
		    @Override
		    public void onCancel(DialogInterface dialog) {
	        	disbaleLocationUpdates();
		    }
		});
		
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}
	
	private void disbaleLocationUpdates() {
    	m_toggleButtonAutoSelectStore.setChecked(false);
		try {
			m_pm.setAutoSelectStore(false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        m_locationRequestor.stop(0);  // stop immediately
        refreshControls();
	}
	
	// The user clicked on an itemInstance.  Display the screen to modify its attributes.
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		ApplicationPm.m_currentActivity = this;
		m_pm.onClickItemInstance(position);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		ApplicationPm.m_currentActivity = this;
		m_pm.onLongClickItemInstance(position);
		return true;
	}

	// The user clicked the button to save the items order.
	public void onButtonClickSaveItemsOrder(View v) {
		ApplicationPm.m_currentActivity = this;
		try {
			m_pm.onButtonClickSaveItemsOrder();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// A child activity returned.
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// Special-case the result of the purchase activity.  Deliver it to the LicenseManager.
		if (requestCode == LicenseManager.PURCHASE_ACTIVITY_REQUEST_CODE) {
			LicenseManager.instance().onLicensePurchaseResult(resultCode, data);
			super.onActivityResult(requestCode, resultCode, data);
			return;
		}
		
		UiPm requestedActivityPm = UiPmRegistry.getActivityPm(requestCode);
		if(requestedActivityPm == null) return;
		requestedActivityPm.dispatchResult(m_pm, resultCode == Activity.RESULT_CANCELED);
		
		refreshControls();
	}

	@Override
	public void onDataModelChange() {
		m_pm.sortOrderedItemInstancesIfNecessary();
		refreshControls();
	}

	// Refresh (some of) the controls with (new) data from the PM. 
	private void refreshControls() {
		// Refresh the store name.
		String storeName = m_pm.getSelectedStoreName();
    	if (storeName == null)
    		storeName = ApplicationPm.m_currentActivity.getResources().getString(R.string.none);
		m_textViewSelectedStoreName.setText(storeName);

		// Refresh the totals.
		m_textViewShoppingListTotalPickedUp.setText(m_pm.getTotalPickedUp());
		m_textViewShoppingListTotal.setText(m_pm.getTotal());

		m_customArrayAdapter.notifyDataSetChanged();
	}

    // Called when a Location update arrives.
	@Override
	public void onLocationAvailable(Location location) {
    	m_pm.onLocationAvailable(location);
    	refreshControls();
	}
}
