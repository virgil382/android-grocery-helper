// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui;

import android.app.Activity;
import android.view.View;

/**
 * <p>An <code>EntityAdapter</code> declares the operations used to adapt an entity
 * class (i.e. an adaptee) to a {@link CustomArrayAdapter} (i.e. the client).  A
 * concrete <code>EntityAdapter</code> can create a new {@link View} (corresponding to
 * a row) to be displayed by a ListView, and can copy/set values from the
 * adaptees to the {@link View}' UI elements (e.g. Buttons, TextViews).
 * 
 * <p>Also see <a href="http://www.lepus.org.uk/ref/companion/Flyweight.xml">Flyweight
 * Design Pattern</a>:  The <code>EntityAdapter</code> plays the role of 
 * <code>Flyweight</code> while the <code>CustomArrayAdapter</code> plays the roles of
 * <code>Flyweight Factory</code> and <code>Client</code>.
 */
public abstract class EntityAdapter<T> 
{
	public EntityAdapter() {}
	
	/**
	 * Create a new View used by a ListView to render a row.  A concrete EntityAdapter
	 * may obtain a new View by inflating a specific layout and by storing in its member
	 * variables references to the View's UI elements (i.e. TextViews, CheckBoxes,
	 * Buttons) so that at a later time renderAdaptee() may copy values from the adaptee
	 * to them.
	 * 
	 * @param activity The Activity via which this method may obtain a layout inflater.
	 * @return A new View to be displayed by a ListView.
	 */
	public abstract View getNewView(Activity activity);
	
	/**
	 * Copy values from an entity object (i.e. the adaptee) into the View's UI elements
	 * (i.e. TextViews, CheckBoxes, Buttons) to which the concrete EntityAdapter holds
	 * references.
	 *  
	 * @param obj The entity object (i.e. the adaptee).
	 * @param isSelected Flag indicating whether or not the adaptee should be rendered
	 * as selected. 
	 */
	public abstract void renderAdaptee(T obj, boolean isSelected);
}
