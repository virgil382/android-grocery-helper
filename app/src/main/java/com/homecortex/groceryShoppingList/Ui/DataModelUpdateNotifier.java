// Copyright Virgil Mocanu
package com.homecortex.groceryShoppingList.Ui;

import java.util.LinkedList;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/**
 * DataModelUpdateNotifier is a singleton that calls the onDataModelChange() method of IDataModelUpdateListeners in the context
 * of the UI thread whenever the data model changes.  The data model is expected to notify the DataModelUpdateNotifier of changes
 * by calling its onDataModelChange() method from any thread.
 */
public class DataModelUpdateNotifier {
	private static int WHAT_VALUE_DATA_MODEL_CHANGE = 1;
	
	private static DataModelUpdateNotifier m_instance;
	public static DataModelUpdateNotifier instance() {
		if (m_instance == null) m_instance = new DataModelUpdateNotifier();
		return m_instance;
	}

	private Handler m_handler; 
	
	private DataModelUpdateNotifier() {
		m_handler = new Handler(Looper.getMainLooper()) {
			// This method is animated by the UI thread.
			@Override
	        public void handleMessage(Message inputMessage) {
				for(IDataModelUpdateListener listener : m_listeners) {
					listener.onDataModelChange();
				}
			}
		};
	}

	LinkedList<IDataModelUpdateListener> m_listeners = new LinkedList<>();

	// This method is probably animated by the UI thread because it is typically called when the UI sets up.
	public void addListener(IDataModelUpdateListener listener) {
		m_listeners.add(listener);
	}

	// This method is probably animated by the UI thread because it probably gets called when the UI is destroyed.
	public void dropListener(IDataModelUpdateListener listener) {
		m_listeners.remove(listener);
	}
	
	// This method is probably called from a worker thread (e.g. that receives data model updates).
	public void onDataModelChange() {
		m_handler.removeMessages(WHAT_VALUE_DATA_MODEL_CHANGE);
		m_handler.sendEmptyMessage(WHAT_VALUE_DATA_MODEL_CHANGE);
	}
}
